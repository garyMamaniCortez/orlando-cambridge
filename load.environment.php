<?php

/**
 * This file is included very early. See autoload.files in composer.json and
 * https://getcomposer.org/doc/04-schema.md#files
 */

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

/**
 * Load the site/project related .env file.
 */
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();
