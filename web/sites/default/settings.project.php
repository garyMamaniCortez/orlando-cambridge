<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Project settings override for drupal 8.
 */

$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'port' => getenv('DB_PORT'),
  'driver' => 'mysql',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

$databases['basex_default']['default'] = [
  'prefix' => '',
  'host' => getenv('BASEX_HOST'),
  'port' => getenv('BASEX_PORT'),
  'namespace' => 'Drupal\\orlando_interface_search\\Driver\\Database\\basex',
  'driver' => 'basex',
  'database' => getenv('BASEX_DATABASE'),
  'mapping_db' => getenv('BASEX_MAPPING_DATABASE'),
  'username' => getenv('BASEX_USER'),
  'password' => getenv('BASEX_PASSWORD'),
];

$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = getenv('FILES_DIR') . '/private';
$settings['file_temporary_path'] = '/tmp';

$settings['config_sync_directory'] = '../config/sync';
// Exclude cambridge core api configs from being exported or overwritten on
// import.
$settings['config_exclude_modules'] = ['cambridge_core_api'];

if (empty($settings['hash_salt'])) {
  $settings['hash_salt'] = getenv('DRUPAL_HASH_SALT', true) ?: getenv('DRUPAL_HASH_SALT');
}
