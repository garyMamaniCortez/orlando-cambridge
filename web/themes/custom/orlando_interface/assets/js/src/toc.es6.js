(() => {
  'use strict';

  function isExpanded(wrapper) {
    return wrapper.getAttribute('aria-expanded') === 'true';
  }

  function toggleDiv(button) {
    const children = button.nextSibling.nextSibling;
    const state = !isExpanded(children) ? 'true' : 'false';
    const svg = button.querySelector('svg');

    children.setAttribute('aria-expanded', state);
    children.classList.toggle('tw-hidden');

    if (state === 'true') {
      svg.classList.remove('tw-rotate-180');
      svg.classList.add('tw-rotate-0');
    }
    else {
      svg.classList.remove('tw-rotate-0');
      svg.classList.add('tw-rotate-180');
    }
  }

  document.querySelectorAll('.oii-toc--button').forEach(button => {
    button.addEventListener('click', function (e) {
      e.preventDefault();
      toggleDiv(this);
    });
  });
})();
