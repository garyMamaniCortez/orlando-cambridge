(function() {
  const helpToggleButtons = document.querySelectorAll('.help-region-toggle-btn');
  const helpRegionWrapper = document.getElementById('help--wrapper');

  function toggleHelpRegion(state) {
    const value = state ? 'true' : 'false';
    helpRegionWrapper.setAttribute('data-region-open', value);
    helpRegionWrapper.classList.toggle('tw-invisible');

    helpToggleButtons.forEach(btn => {
      btn.setAttribute('aria-expanded', value);
      if (btn.classList.contains('tw-border-white')) {
        btn.classList.toggle('tw-bg-white');
        btn.classList.toggle('tw-text-red-200');
      }
    });
  }

  function isOpen() {
    return helpRegionWrapper.getAttribute('data-region-open') === 'true';
  }

  helpToggleButtons.forEach(btn => {
    btn.addEventListener('click', () => {
      toggleHelpRegion(isOpen());
    });
  });
})();
