(function(Popper, Drupal) {

  const addCloseButton = function (content, popperInstance) {
    let btn = content.querySelector('button[data-popperjs-close]');
    if (btn) { return; }

    btn = document.createElement('button');
    btn.setAttribute('data-popperjs-close', 'true');
    btn.classList.add(...[
      'tw-h-4',
      'tw-w-4',
      'tw-rounded-full',
      'tw-border',
      'tw-border-gray-500',
      'tw-grid',
      'tw-place-content-center',
      'tw-absolute',
      'tw-top-1',
      'tw-right-1',
    ]);
    btn.addEventListener('click', function() {
      toggle(popperInstance);
    });

    const btnText = document.createElement('span');
    btnText.classList.add('tw-sr-only');
    btnText.textContent = Drupal.t('Close');
    btn.appendChild(btnText);

    const svgMarkup = `<svg class="tw-w-2 tw-h-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
</svg>`;
    btn.appendChild(document.createRange().createContextualFragment(svgMarkup));

    content.insertAdjacentElement('afterbegin', btn);
  }

  const toggle = function (popperInstance) {
    const content = popperInstance.state.elements.popper;
    content.classList.toggle('tw-hidden');

    // Adding a close button.
    addCloseButton(content, popperInstance);

    if (content.classList.contains('tw-hidden')) {
      // Disable the event listeners.
      popperInstance.setOptions({
        modifiers: [{ name: 'eventListeners', enabled: false }],
      });
      return;
    }
    // Enable the event listeners.
    popperInstance.setOptions({
      modifiers: [{ name: 'eventListeners', enabled: true }],
    });
    // Update its position.
    popperInstance.update();
  }

  Drupal.behaviors.oiSchorlarnoteReferencePopperJs = {
    attach: function (context, settings) {
      const buttons = context.querySelectorAll('button[data-popperjs-enabled="true"]');
      buttons.forEach(referenceElement => {
        if (referenceElement.classList.contains('js-popper-processed')) { return; }

        // Changing the setup of elements in the milestones to ensure that the
        // id is unique.
        if (referenceElement.closest('div.view.view-display-id-embedded_milestones')) {
          const popperElementId = `${referenceElement.dataset.popperjsTooltipId}--milestones`;
          referenceElement.setAttribute('id', `${popperElementId}--button`);
          referenceElement.dataset.popperjsTooltipId = popperElementId;
          referenceElement.setAttribute('aria-describedby', popperElementId);
          referenceElement.nextSibling.setAttribute('id', popperElementId);
        }

        const popperElement = document.getElementById(referenceElement.dataset.popperjsTooltipId);
        if (popperElement) {
          // Deal with anonymous user case where the popup is empty.
          popperElement.querySelectorAll('[data-popper-inner-content="true"]').forEach(elt => {
            if (elt.innerHTML) { return; }

            elt.innerHTML = Drupal.t('<p>You don\'t have access to view the content  of this popup. Please <a href="/user/login">login</a>.</p>');
          });
          const popperInstance = Popper.createPopper(referenceElement, popperElement, {
            placement: 'bottom',
            modifiers: [{
              name: 'offset',
              options: {
                offset: [0, 40],
              },
            }],
          });
          referenceElement.addEventListener('click', function (){
            toggle(popperInstance);
          });
        }
        referenceElement.classList.add('js-popper-processed');
      });
    }
  };
})(Popper, Drupal);
