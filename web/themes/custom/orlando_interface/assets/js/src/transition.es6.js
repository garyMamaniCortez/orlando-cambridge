/**
 * @file
 * Transition utility functions used by the theme.
 */

(function (Drupal) {
  Drupal.OrlandoInterface = Drupal.OrlandoInterface || {};
  Drupal.OrlandoInterface.transition = {};
  function nextFrame() {
    return new Promise((resolve) => {
      requestAnimationFrame(() => {
        requestAnimationFrame(resolve);
      });
    });
  }

  function afterTransition(element) {
    return new Promise((resolve) => {
      const duration = Number(
        getComputedStyle(element).transitionDuration.replace('s', '')
      ) * 1000;

      setTimeout(() => {
        resolve();
      }, duration);
    });
  }

  Drupal.OrlandoInterface.transition.enter = async function(element, transition) {
    element.classList.remove('hidden');
    element.classList.add(`${transition}-enter`);
    element.classList.add(`${transition}-enter-active`);

    await nextFrame();

    element.classList.remove(`${transition}-enter`);
    await afterTransition(element);

    element.classList.remove(`${transition}-enter-active`);
  };

  Drupal.OrlandoInterface.transition.leave = async function(element, transition) {
    element.classList.remove('hidden');

    element.classList.add(`${transition}-leave`);
    element.classList.add(`${transition}-leave-active`);

    await nextFrame();

    element.classList.remove(`${transition}-leave`);

    await afterTransition(element);

    element.classList.remove(`${transition}-leave-active`);
    element.classList.add('hidden');
  };
})(Drupal);
