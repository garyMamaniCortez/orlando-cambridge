(function() {
  const form = document.getElementById('views-exposed-form-events-page-timeline');
  const toggleBtn = form.querySelector('.filter-inputs--toggle');
  const filterWrapper = form.querySelector('.filter-inputs--wrapper');

  function toggleArea(state) {
    const value = state ? 'true' : 'false';
    filterWrapper.setAttribute('data-is-open', value);
    filterWrapper.classList.toggle('tw-invisible');
    filterWrapper.classList.toggle('is-open');

    if (state) {
      filterWrapper.classList.remove('tw-z-10');
      filterWrapper.classList.remove('tw-opacity-100');
      filterWrapper.classList.add('tw-z-0');
      filterWrapper.classList.add('tw-opacity-0');
    }
    else {
      filterWrapper.classList.remove('tw-z-0');
      filterWrapper.classList.remove('tw-opacity-0');
      filterWrapper.classList.add('tw-z-10');
      filterWrapper.classList.add('tw-opacity-100');
    }
  }

  function isOpen(element) {
    return element.getAttribute('data-is-open') === 'true';
  }

  toggleBtn.addEventListener('click', (e) => {
    e.preventDefault();
    toggleArea(isOpen(filterWrapper));
  });
})();
