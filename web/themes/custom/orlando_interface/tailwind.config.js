const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  prefix: 'tw-',
  theme: {
    colors: {
      transparent: 'transparent',

      white: '#fff',
      black: '#000',

      gray: {
        100: '#f6f6f6',
        200: '#cbcbca',
        300: '#939392',
        400: '#696868',
        450: '#42403e',
        500: '#1d1d1d',
      },
      red: {
        100: '#981915',
        200: '#6d0a18',
      },
      blue: {
        100: '#00649b',
        200: '#034970',
      },
    },
    extend: {
      borderRadius: {
        xs: '0.125rem',
        sm: '0.1875rem',
      },
      borderWidth: {
        '3': '3px',
        '6': '6px',
      },
      fontFamily: {
        sans: ['"NotoSans"', ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        '4xl': '2.125rem',
        '2xl': '1.375rem',
      },
      maxWidth: {
        md: '30rem',
        '285': '285px',
      },
      minWidth: {
        '6': '1.5rem',
        '285': '285px',
      },
      minHeight: {
        '285': '285px',
        '291': '291px',
      },
      spacing: {
        '7.5': '1.875rem',
        '14.5': '3.125rem',
        '44': '11.25rem',
      },
      inset: {
        '-7.5': '-1.875rem',
      },
      opacity: {
        '10': '.1',
        '30': '.3',
        '80': '.8',
      },
      backgroundImage: theme => ({
        'event-type': "url('../../images/bibliographies.png')",
      }),
      typography: (theme) => ({
        default: {
          css: {
            color: theme('colors.gray.500'),
            lineHeight: 1.5,
            maxWidth: 'initial',
          },
        },
      })
    },
    customForms: theme => ({
      default: {
        'input, select, textarea, multiselect': {
          borderRadius: theme('borderRadius.sm'),
        },
        'checkbox, radio': {
          color: theme('colors.white'),
          iconColor: theme('colors.blue.100'),
          borderColor: theme('colors.gray.300'),
          '&:focus': {
            borderColor: theme('colors.blue.100'),
          },
        },
        'checkbox': {
          iconColor: theme('colors.white'),
        },
        'select': {
          'icon': '<svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 24"><path d="M11 17l-5 5-5-5M1 7l5-5 5 5" stroke="#42403E" stroke-width="2" stroke-linecap="round"/></svg>',
          iconColor: theme('colors.gray.450'),
          '&:hover': {
            iconColor: theme('colors.blue.100'),
          },
        },
      },
    }),
  },
  variants: {
    cursor: ['responsive', 'hover'],
    display: ['responsive', 'hover', 'group-hover'],
    backgroundColor: ['responsive', 'hover', 'focus', 'group-hover'],
    backgroundOpacity: ['responsive', 'hover', 'focus', 'group-hover'],
    padding: ['responsive', 'first'],
  },
  plugins: [
    require('@tailwindcss/ui'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/custom-forms'),
  ],
}
