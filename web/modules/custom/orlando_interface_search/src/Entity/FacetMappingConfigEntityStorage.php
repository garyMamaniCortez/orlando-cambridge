<?php

namespace Drupal\orlando_interface_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

class FacetMappingConfigEntityStorage extends ConfigEntityStorage {

  /**
   * Loads enabled facet mappings.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface[]
   *   An array of facet mapping configuration keyed by their machine names.
   */
  public function loadEnabledMappings() {
    $ids = $this->getQuery()
      ->condition('status', 1)
      ->sort('weight')
      ->execute();
    $mappings = [];
    foreach ($this->loadMultiple($ids) as $mapping) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $mapping */
      if ($mapping->isEnabled()) {
        $mappings[$mapping->id()] = $mapping;
      }
    }
    return $mappings;
  }
}
