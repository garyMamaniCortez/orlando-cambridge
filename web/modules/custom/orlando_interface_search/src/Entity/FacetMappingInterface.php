<?php

namespace Drupal\orlando_interface_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\file\FileInterface;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface;

interface FacetMappingInterface extends ConfigEntityInterface {

  public function isEnabled(): bool;

  public function getFacetWidgetId(): string;

  public function setFacetWidgetId(string $facet_widget_id);

  public function getFacetWidget(): BaseXFacetWidgetPluginInterface;

  public function getFacetWidgetConfig(): array;

  public function setFacetWidgetConfig(array $facet_widget_config);

  public function hasValidFacetWidget(): bool;

  public function getFileUri(): string;

  public function getMappings(): array;

}
