<?php

namespace Drupal\orlando_interface_search\Plugin\Block;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\orlando_interface_search\Form\MainSearchForm;
use Drupal\orlando_interface_search\Form\SearchTermsForm;
use Drupal\orlando_interface_search\Form\TagSearchForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Search form' block for basex backend.
 *
 * @Block(
 *   id = "oi_search_form_block",
 *   admin_label = @Translation("Orlando Interface Search form"),
 *   category = @Translation("Forms"),
 * )
 */
class SearchBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new SearchLocalTask.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface  $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'search_type' => SearchTermsForm::class,
      'introduction_text' => '',
      'keys_placeholder' => 'search',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['search_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Search type'),
      '#options' => [
        SearchTermsForm::class => $this->t('Search terms'),
        MainSearchForm::class => $this->t('Full search'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['search_type'],
    ];
    $form['introduction_text'] = array(
      '#title' => $this->t('Introduction text'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['introduction_text'],
      '#cols' => '80',
      '#rows' => '3',
      '#states' => [
        'visible' => [
          ':input[name="search_type"]' => ['value' => 'keys'],
        ],
      ],
    );
    $form['keys_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search term placeholder'),
      '#default_value' => $this->configuration['keys_placeholder'],
      '#states' => [
        'visible' => [
          ':input[name="search_type"]' => ['!value' => 'sort_reset_search'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $search_type = $form_state->getValue('search_type');
    $is_keys = $search_type === 'keys';
    $this->configuration['search_type'] = $search_type;
    $this->configuration['introduction_text'] = $is_keys ? Xss::filter($form_state->getValue('introduction_text'), ['<br/>', '<br>']) : '';
    $this->configuration['keys_placeholder'] = Xss::filter($form_state->getValue('keys_placeholder'));
  }

  public function build() {
    $form_options = [
      'introduction_text' => $this->configuration['introduction_text'],
      'keys_placeholder' => $this->configuration['keys_placeholder'],
    ];
    return $this->formBuilder->getForm($this->configuration['search_type'], $form_options);
  }

}
