<?php

namespace Drupal\orlando_interface_search\BaseXFacetWidget;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;

interface BaseXFacetWidgetPluginInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the label for use on the administration pages.
   *
   * @return string
   *   The administration label.
   */
  public function label();

  /**
   * Gets the facet mapping for this widget.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface
   *   The facet mapping entity.
   */
  public function getFacetMapping(): FacetMappingInterface;

  /**
   * Sets the facet mapping for this widget.
   *
   * @param \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping
   *   The facet mapping entity.
   *
   * @return $this;
   */
  public function setFacetMapping(FacetMappingInterface $facet_mapping);

  public function formElement(array $form = [], FormStateInterface $form_state = NULL, array $results = []): array;

  public function extractSelectedValues($data);

  public function generateSelectedValuesString($values): string;

  public function buildXQueryString($values = NULL): string;

  public function validQueryParameters(array $values, array $allowed_values = []): bool;

}
