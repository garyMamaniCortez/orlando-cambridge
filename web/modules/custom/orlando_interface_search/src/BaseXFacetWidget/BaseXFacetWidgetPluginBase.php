<?php

namespace Drupal\orlando_interface_search\BaseXFacetWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseXFacetWidgetPluginBase
 *
 * @see plugin_api
 */
abstract class BaseXFacetWidgetPluginBase extends PluginBase implements BaseXFacetWidgetPluginInterface {

  /**
   * The facet mapping for this widget.
   *
   * @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface
   */
  protected $facetMapping;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    if (($configuration['#facet_mapping'] ?? NULL) instanceof FacetMappingInterface) {
      $this->setFacetMapping($configuration['#facet_mapping']);
      unset($configuration['#facet_mapping']);
    }
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'is_collapsible' => TRUE,
      'is_bordered' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFacetMapping(): FacetMappingInterface {
    return $this->facetMapping;
  }

  /**
   * {@inheritdoc}
   */
  public function setFacetMapping(FacetMappingInterface $facet_mapping) {
    $this->facetMapping = $facet_mapping;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['is_collapsible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is the widget collapsible?'),
      '#default_value' => !empty($this->configuration['is_collapsible']),
    ];
    $form['is_bordered'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is the widget bordered?'),
      '#default_value' => !empty($this->configuration['is_bordered']),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array $form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    return $this->formElementInit();
  }

  abstract public function processedMappings();

  private function formElementInit() {
    $title = $this->t('@title', [
      '@title' => $this->facetMapping->label(),
    ]);
    $element = ['#tree' => TRUE];
    if (!empty($this->configuration['is_collapsible'])) {
      $element['#type'] = 'fieldset';
      $element['#title'] = $title;
      $element['#collapsible'] = TRUE;
      $element['value']['#title_display'] = 'invisible';
    }
    else {
      $element['#type'] = 'container';
    }

    $element['value']['#title'] = $title;
    $element['value']['#bordered'] = !empty($this->configuration['is_bordered']);
    return $element;
  }

}
