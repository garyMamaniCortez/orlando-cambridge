<?php

namespace Drupal\orlando_interface_search\Query;

use Drupal\orlando_interface_search\Item\Document;
use Drupal\orlando_interface_search\Item\Facet;
use Drupal\orlando_interface_search\Item\FacetWidget;
use Drupal\orlando_interface_search\Utility\QueryHelper;

class ResultSet implements \IteratorAggregate {

  protected $documentsCount = 0;

  protected $documents = [];

  protected $rawDocuments = [];

  protected $rawFacets = [];

  /**
   * Facets widgets array keyed by their types.
   *
   * @var \Drupal\orlando_interface_search\Item\FacetWidget[]
   */
  protected $facets = [];

  protected $query;

  public function __construct(QueryHelper $query) {
    $this->query = $query;
  }

  public function getQuery() {
    return $this->query;
  }

  public function setQuery(QueryHelper $query) {
    $this->query = $query;
    return $query;
  }

  public function getDocumentItems() {
    if (!$this->documents && $this->rawDocuments) {
      $offset = $this->query->viewOffset;
      $num_per_page = $this->query->viewPagerNumPerPage;
      $results = array_slice($this->rawDocuments, $offset, $num_per_page);
      foreach ($results as $value) {
        $attributes = $value['@attributes'];
        $hits = $value['hits']['hit'];
        $document_item = new Document($attributes, $hits);
        $this->addDocumentItem($document_item);
      }
    }
    return $this->documents;
  }

  public function addDocumentItem(Document $item) {
    $this->documents[$item->getId()] = $item;
    return $this;
  }

  public function setDocumentItems(array $items) {
    $this->documents = $items;
    return $this;
  }

  /**
   * @return int
   */
  public function getDocumentItemsCount(): int {
    if ($this->documentsCount === 0 && !empty($this->rawDocuments)) {
      $this->documentsCount = count($this->rawDocuments);
    }
    return $this->documentsCount;
  }

  /**
   * @param int $count
   */
  public function setDocumentItemsCount(int $count): void {
    $this->documentsCount = $count;
  }

  /**
   * @return array
   */
  public function getFacets(): array {
    if (!$this->facets && $this->rawFacets) {
      foreach ($this->rawFacets as $value) {
        $type = is_string($value['type']) ? $value['type'] : '';
        $name = is_string($value['name']) && !empty($value['name']) ? $value['name'] : '';
        $count = is_numeric($value['count']) ? $value['count'] : 0;
        if ($type && $name && $count) {
          $facet_item = new Facet($type, $name, $count);
          $this->addFacetItem($facet_item);
        }
      }
    }
    return $this->facets;
  }

  public function addFacetItem(Facet $facet_item) {
    $type = $facet_item->getType();
    if (!isset($this->facets[$type])) {
      $this->facets[$type]['id'] = $type;
      $this->facets[$type]['items'] = [];
    }
    $this->facets[$type]['items'][] = $facet_item;
    return $this;
  }

  /**
   * @param array $facets
   */
  public function setFacets(array $facets): void {
    $this->facets = $facets;
  }

  public function setRawDocuments(array $items) {
    // A result with one result item will have the type directly under the items
    // array.
    $this->rawDocuments = !isset($items['hits']) ? $items : [$items];
    $this->setDocumentItemsCount(count($this->rawDocuments));
    return $this;
  }

  public function getRawDocuments() {
    return $this->rawDocuments;
  }

  public function setRawFacets(array $items) {
    // A result with one facet item will have the type directly under the items
    // array.
    $this->rawFacets = !isset($items['type']) ? $items : [$items];
    return $this;
  }

  public function getRawFacets() {
    return $this->rawFacets;
  }

  public function getIterator() {
    return new \ArrayIterator($this->documents);
  }

}
