<?php

namespace Drupal\orlando_interface_search\Utility;

use Drupal\orlando_interface_ingestion\TypedRepositories\BibciteReferenceRepository;
use Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository;
use Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_search\Cache\BasexResultsCache;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Drupal\orlando_interface_search\Item\Document;
use Drupal\orlando_interface_search\Query\ResultSet;
use Symfony\Component\HttpFoundation\RequestStack;

class QueryHelper {

  protected $requestStack;

  protected $resultsCacheStorage;

  protected $rawResults;

  public $viewExecuted = FALSE;

  public $viewResult = [];

  public $viewExecuteTime = NULL;

  public $viewTotalRows = NULL;

  public $viewPagerTotalItems = 0;

  public $viewPagerCurrentPage = NULL;

  public $viewPagerNumPerPage = NULL;

  protected $viewCurrentPage = NULL;

  protected $viewItemsPerPage = NULL;

  public $viewOffset = NULL;

  /**
   * NULL value to use as a key for the results storage.
   *
   * @var object
   */
  protected $null;

  /**
   * The search ID set for this query.
   *
   * @var string
   */
  protected $searchId;

  protected $searchParameters;

  protected $basexDatabase;

  public function __construct(RequestStack $requestStack, Connection $connection) {
    $this->requestStack = $requestStack;
    $this->resultsCacheStorage = new \SplObjectStorage();
    $this->basexDatabase = $connection;
    $this->null = (object) [];
    $this->rawResults = new ResultSet($this);
  }

  public function init($facet_only = FALSE) {
    if ($facet_only) {
      $this->searchId = 'orlando_interface_search:search:facets_block';
    }
    else {
      $this->searchId = 'orlando_interface_search:search:results_page';
    }
  }

  public function execute($options = []) {
    if (!empty($this->viewExecuted)) {
      return TRUE;
    }
    $this->viewExecuted = TRUE;
    $cache = new BasexResultsCache();
    $cache->init($this);

    // Try getting data from cache without worrying about the page query.
    // This will probably work for result of the page which have been cached
    // before.
    $start = microtime(TRUE);
    if ($cache->cacheGet()) {
      $this->viewPagerTotalItems = $this->viewTotalRows;
    }
    else {
      // Trying to check the cache of the raw results if available. If the page
      // wasn't cached the get the cached raw results.
      if ($cache->cacheGet(TRUE)) {
        $results = $this->getResultSets();
        // Resetting the documents loaded to trigger the new calculation.
        $results->setDocumentItems([]);
        $is_raw_results = FALSE;
      }
      else {
        // Execute query.
        $results = $this->basexDatabase->queryInit($this)->searchExecute();
        $is_raw_results = TRUE;
      }
      $this->viewPagerTotalItems = $results->getDocumentItemsCount();
      $this->viewTotalRows = $this->viewPagerTotalItems;
      if (!empty($this->viewOffset)) {
        $this->viewPagerTotalItems -= $this->viewOffset;
      }
      $this->viewResult = [];
      $this->addViewResults($results);

      $this->viewExecuteTime = microtime(TRUE) - $start;
      $this->viewResult = array_values($this->viewResult);
      $cache->cacheSet($is_raw_results);
    }
    return $this->viewExecuted;
  }

  public function getSearchId($generate = TRUE) {
    if ($generate && !isset($this->searchId)) {
      static $num = 0;
      $this->searchId = 'search_' . ++$num;
    }
    return $this->searchId;
  }

  protected function addViewResults(ResultSet $result_set) {
    $results = $result_set->getDocumentItems();
    // In this place we could instantiate the paragraphs.
    $count = 0;

    // @Todo check for anonymous user and reduce the results.
    foreach ($results as $item_id => $item) {
      $values = [];
      $values['_item'] = $item;
      $values['_wrapped_entity'] = $this->getDocumentWrappedEntity($item);
      $values['index'] = $count++;
      $this->viewResult[] = $values;
    }
    $this->viewPagerTotalItems = $count;
  }

  public function postExecuteAddResults(ResultSet $results, $is_raw_results = FALSE) {
    // Same implementation as in \Drupal\search_api\Utility\QueryHelper::86
    $search_id = $this->getSearchId();
    $request = $this->getCurrentRequest();
    if ($is_raw_results) {
      // Removing page query.
      $request->query->remove('page');
    }
    if (!isset($this->resultsCacheStorage[$request])) {
      $this->resultsCacheStorage[$request] = [
        $search_id => $results,
      ];
    }
    else {
      $cache = $this->resultsCacheStorage[$request];
      $cache[$search_id] = $results;
      $this->resultsCacheStorage[$request] = $cache;
    }

    $results->setQuery($this);
    $this->rawResults = $results;
  }

  public function getCurrentPage() {
    if ($this->viewPagerCurrentPage) {
      return $this->viewPagerCurrentPage;
    }
    if ($this->viewCurrentPage) {
      return $this->viewCurrentPage;
    }
    return 0;
  }

  public function getResultFromCacheBySearchId($search_id) {
    return $this->resultsCacheStorage[$this->getCurrentRequest()][$search_id] ?? NULL;
  }

  public function getResultSets() {
    return $this->rawResults;
  }

  public function initViewPager($current_page = NULL) {
    if (!is_numeric($current_page) || $current_page < 0) {
      $current_page = 0;
    }
    $this->viewPagerCurrentPage = $current_page;
  }

  public function populateViewPropertiesWithCachedData($cache) {
    $current_page = $cache->data['current_page'] ?? $this->viewPagerCurrentPage;
    $this->viewResult = $cache->data['view_results'] ?? [];
    $this->viewTotalRows = $cache->data['total_rows'];
    $this->viewExecuteTime = 0;
    $this->viewCurrentPage = $current_page;
    $this->viewPagerCurrentPage = $current_page;
  }

  public function getSearchParameters() {
    return $this->searchParameters;
  }

  public function setSearchParameters(array $parameters, $offset, $current_page, $num_per_page) {
    $this->searchParameters = $parameters;
    $this->viewPagerCurrentPage = $current_page;
    $this->viewCurrentPage = $current_page;
    $this->viewOffset = $offset;
    $this->viewPagerNumPerPage = $num_per_page;
    $this->viewItemsPerPage = $num_per_page;
    return $this;
  }

  /**
   * Retrieves the current request.
   *
   * If there is no current request, instead of returning NULL this will instead
   * return a unique object to be used in lieu of a NULL key.
   *
   * @return \Symfony\Component\HttpFoundation\Request|object
   *   The current request, if present; or this object's representation of the
   *   NULL key.
   */
  protected function getCurrentRequest() {
    return $this->requestStack->getCurrentRequest() ?: $this->null;
  }

  /**
   * @param \Drupal\orlando_interface_search\Item\Document $document
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity|null
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  protected function getDocumentWrappedEntity(Document $document) {
    $document_type = $document->getType();
    $id = $document->getId();
    $wrapped_entity = NULL;
    if ($document_type == 'author') {
      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\AuthorProfileRepository $repository */
      $repository = $this->getWrappedEntityRepository('node', 'author_profile');
      $wrapped_entity = $repository->findByCWRCId($id);
    }
    elseif ($document_type == 'person' || $document_type == 'organization') {
      $repository = $this->getWrappedEntityRepository('taxonomy_term', $document_type);
      assert($repository instanceof TaxonomyTermRepository);
      $wrapped_entity = $repository->findByCWRCId($id);
    }
    elseif ($document_type == 'event') {
      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository $repository */
      $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
      assert($repository instanceof ProfileEventRepository);
      $wrapped_entity = $repository->findById($id);
    }
    elseif ($document_type === 'title' && $id) {
      $wrapped_entity = BibciteReferenceRepository::getWrappedEntityByCWRCId($id);
    }
    return $wrapped_entity;
  }

  private function getWrappedEntityRepository(string $entity_type_id, string $bundle) {
    return \Drupal::service('Drupal\typed_entity\RepositoryManager')->repository($entity_type_id, $bundle);
  }

}
