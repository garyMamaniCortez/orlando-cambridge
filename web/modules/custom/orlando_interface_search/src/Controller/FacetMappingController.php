<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;

class FacetMappingController extends ControllerBase {

  public function page(FacetMappingInterface $basex_facet_mapping) {
    $render = [
      'metadata' => [
        '#theme' => 'basex_facet_mapping',
        '#facet_mapping' => $basex_facet_mapping,
      ],
    ];
    return $render;
  }

  /**
   * Returns the page title for a facet mapping's "View" tab.
   *
   * @param \Drupal\orlando_interface_search\Entity\FacetMappingInterface $basex_facet_mapping
   *   The facet mapping that is displayed.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(FacetMappingInterface $basex_facet_mapping) {
    return new FormattableMarkup('@title', ['@title' => $basex_facet_mapping->label()]);
  }

}
