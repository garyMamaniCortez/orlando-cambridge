<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

class FacetMappingsListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_search_facet_mapping_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Title');
    $header['id'] = $this->t('Machine name');
    $header['facet_widget'] = $this->t('Widget');
    $header['status'] = $this->t('Enabled');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $entity */
    $row['label'] = $entity->label();
    $row['id']['#markup'] = $entity->id();
    $row['facet_widget']['#markup'] = $entity->getFacetWidget()->label();
    $row['status']['#markup'] = $entity->status() ? $this->t('Yes') : $this->t('No');
    return $row + parent::buildRow($entity);
  }

}
