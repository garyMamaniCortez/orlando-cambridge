<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\bibcite_entity\Entity\ReferenceInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;
use Drupal\orlando_interface_search\Form\TagSearchForm;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Drupal\orlando_interface_support\Plugin\Field\FieldFormatter\IconDefaultFormatter;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ResultsController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  protected $queryHelper;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paragraphStorage;

  /**
   * @var \Drupal\paragraphs\ParagraphViewBuilder
   */
  protected $paragraphViewBuilder;

  protected $renderer;

  protected $searchKeys = [];

  public function __construct(RequestStack $request_stack, QueryHelper $query_helper, RendererInterface $renderer, FormBuilderInterface  $form_builder) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->queryHelper = $query_helper;
    $this->renderer = $renderer;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('orlando_interface_search.query'),
      $container->get('renderer'),
      $container->get('form_builder')
    );
  }

  public function page() {
    // Perform the query, using the requested offset from
    // PagerManagerInterface::findPage(). This comes from a URL parameter, so
    // here we are assuming that the URL parameter corresponds to an actual
    // page of results that will exist within the set.
    /** @var \Drupal\Core\Pager\PagerParametersInterface $pager_parameters */
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage();
    $num_per_page = 25;
    $offset = $num_per_page * $page;

    // Perform the query to get the data.
    // $result = mymodule_remote_search($keywords, $offset, $num_per_page);
    $parameters = $this->currentRequest->query->all();
    $this->searchKeys = $parameters['keys'] ?? [];
    $this->queryHelper
      ->setSearchParameters($parameters, $offset, $page, $num_per_page)
      ->execute();

    // Now that we have the total number of results, initialize the pager.
    /** @var \Drupal\Core\Pager\PagerManagerInterface $pager_manager */
    $pager_manager = \Drupal::service('pager.manager');
    $total_rows = $this->queryHelper->viewTotalRows;
    $pager_manager->createPager($total_rows, $num_per_page);

    // Content top.
    $keys = !empty($parameters['keys']) ? implode(' ', $parameters['keys']) : '';
    $build['content_top'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['tw-pb-6'],
      ],
      'top' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'tw-flex',
            'tw-flex-col',
            'tw-space-y-4',
            'tw-items-baseline',
            'sm:tw-items-center',
            'sm:tw-place-content-between',
            'sm:tw-flex-row',
            'sm:tw-space-y-0',
          ]
        ],
        'result_summary' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $keys ? $this->t('<span class="tw-font-bold">@count</span> results for <span class="tw-font-bold">@keys</span>', [
            '@count' => $total_rows ?: 0,
            '@keys' => $keys,
          ]) : $this->t('<span class="tw-font-bold">@count</span> results', ['@count' => $total_rows ?: 0]),
        ],
        'sort' => $this->formBuilder()->getForm(TagSearchForm::class),
      ],
    ];

    if ($selected_items = $this->buildFilterItemsClear($parameters)) {
      $build['content_top']['bottom'] = [
        '#theme' => 'item_list',
        '#items' => $selected_items,
        '#attributes' => [
          'class' => [
            'tw-flex',
            'tw-flex-wrap',
            'tw-items-center',
            'tw--m-1.5',
            'tw-text-sm',
          ],
        ],
      ];
    }


    // Create a render array with the search results.
    $build['content_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'tw-pt-8',
          'tw-border-t',
          'tw-border-gray-200',
          'tw-divide-gray-200',
          'tw-space-y-5',
          'tw-divide-y',
        ],
      ],
    ];
    $build['content_wrapper']['results'] = [
      '#theme' => 'orlando_interface_search__results',
      '#items' => $this->getItems(),
    ];
    $build['pager'] = ['#type' => 'pager'];

    $build += [
      '#cache' => array(
        'contexts' => array(
          'url.query_args',
        ),
      ),
    ];
    return $build;
  }

  private function getItems() {
    $results = $this->queryHelper->viewResult;

    $list = [];
    $total_items = count($results);
    $linked_titles = [
      'node',
      'taxonomy_term',
    ];
    foreach ($results as $value) {
      /** @var \Drupal\orlando_interface_search\Item\Document $item */
      $item = $value['_item'];
      $wrapped_entity = $value['_wrapped_entity'];
      $url = '';
      $entity_type_id = '';
      if ($wrapped_entity && $wrapped_entity instanceof WrappedEntity) {
        $title = $wrapped_entity->label();
        $entity = $wrapped_entity->getEntity();
        $entity_type_id = $entity->getEntityTypeId();
        if (in_array($entity_type_id, $linked_titles)) {
          $url = $entity->toUrl()->toString();
        }
      }
      else {
        $title = $this->t('@name', [
          '@name' => $item->getName(),
        ]);
      }
      $list[] = [
        '#theme' => 'orlando_interface_search__results__item',
        '#wrapped_entity' => $wrapped_entity,
        '#item' => $item,
        '#entity_type_id' => $entity_type_id,
        '#title' => $title,
        '#url' => $url,
        '#icon' => $this->getIconTheme($item->getType()),
        '#is_first' => $value['index'] == 0,
        '#is_last' => ($value['index'] + 1) == $total_items,
        '#hits' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'tw-mt-3',
              'tw-space-y-3',
              'tw-divide-dashed',
              'tw-divide-gray-200',
              'tw-divide-y',
            ]
          ],
          '#cache' => ['contexts' => ['user.roles']],
          'content' => $this->buildDocumentHitItems($value)
        ],
      ];
    }
    return $list;
  }

  private function getIconTheme(string $document_type) {
    return [
      '#theme' => IconDefaultFormatter::getIconThemeName($document_type),
      '#attributes' => new Attribute([
        'class' => [
          'tw-w-6',
          'tw-h-6',
          'tw-flex',
          'tw-items-center',
          'tw-justify-center',
          'tw-rounded-full',
          'tw-bg-blue-100',
          'tw-text-white',
        ],
        'aria-hidden' => 'true',
      ]),
      '#svg_attributes' => new Attribute([
        'fill' => 'currentColor',
        'width' => '14px',
        'height' => '14px',
        'aria-hidden' => 'true',
      ]),
      '#color_primary' => 'currentColor',
      '#color_secondary' => '#000000',
      '#color_stroke' => 'transparent',
    ];
  }

  private function buildDocumentHitItems(array $value) {
    $hit_paragraphs = [];
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity $wrapped_entity */
    $wrapped_entity = $value['_wrapped_entity'];
    $entity = $wrapped_entity ? $wrapped_entity->getEntity() : NULL;
    $entity_type_id = $entity ? $entity->getEntityTypeId() : '';
    /** @var \Drupal\orlando_interface_search\Item\Document $item */
    $item = $value['_item'];
    if ($entity_type_id == 'node' || $entity_type_id == 'profile_event') {
      $count = 1;
      foreach ($item->getHits() as $hit) {
        $hit_id = $hit->getId();
        if ($hit_id && !($count > 1 && $this->isAnonymousUser())) {
          $paragraphs = $this->paragraphStorage()->loadByProperties([
            'field_id' => $hit_id,
          ]);
          if ($paragraphs) {
            $count++;
            $paragraph = reset($paragraphs);
            $hit_paragraphs[] = $this->highlightBuildView(
              $this->paragraphViewBuilder()
                ->view($paragraph, 'search_result')
            );
          }
        }
      }
    }
    elseif ($entity instanceof TermInterface) {
      $field_items = $entity->get('field_name_variants');
      $hit_names = $item->getHitValues();
      if ($hit_names) {
        $hit_values = [];
        foreach ($field_items as $field_item_index => $field_item) {
          /** @var \Drupal\double_field\Plugin\Field\FieldType\DoubleField $field_item */
          $name = $field_item->getValue()['first'];
          if (in_array($name, $hit_names)) {
            $hit_values[] = $field_item->getValue();
          }
        }
        if ($hit_values) {
          // Override field values
          $field_items->setValue($hit_values, FALSE);
          $hit_paragraphs[] = $this->highlightBuildView($field_items->view('explore'));
        }
      }
    }
    elseif ($entity instanceof ReferenceInterface) {
      $hit_paragraphs[] = $this->highlightBuildView(
        $this->bibciteReferenceViewBuilder()
          ->view($entity, 'citation')
      );
    }

    return $hit_paragraphs;
  }

  private function paragraphStorage() {
    if (!isset($this->paragraphStorage)) {
      $this->paragraphStorage = $this->entityTypeManager()
        ->getStorage('paragraph');
    }
    return $this->paragraphStorage;
  }

  private function paragraphViewBuilder() {
    if (!isset($this->paragraphViewBuilder)) {
      $this->paragraphViewBuilder = $this->entityTypeManager()
        ->getViewBuilder('paragraph');
    }
    return $this->paragraphViewBuilder;
  }

  private function bibciteReferenceViewBuilder() {
    if (!isset($this->bibciteReferenceViewBuilder)) {
      $this->bibciteReferenceViewBuilder = $this->entityTypeManager()
        ->getViewBuilder('bibcite_reference');
    }
    return $this->bibciteReferenceViewBuilder;
  }

  private function isAnonymousUser() {
    return $this->currentUser()->isAnonymous();
  }

  private function highlightBuildView($build) {
    if ($this->searchKeys && is_array($build)) {
      $markup = $this->renderer->render($build);
      return [
        '#type' => 'processed_text',
        '#text' => orlando_interface_search_highlight((string) $markup, $this->searchKeys),
        '#format' => 'full_html',
      ];
    }
    return $build;
  }

  private function buildFilterItemsClear($parameters) {
    $facet_results = $this->queryHelper->getResultSets()->getFacets();
    $items = [];
    if ($facet_results) {
      $applied_filters = [];
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
      $storage = \Drupal::entityTypeManager()->getStorage('basex_facet_mapping');
      $facet_mappings = $storage->loadEnabledMappings();
      foreach ($facet_mappings as $type => $mapping) {
        if (!isset($parameters[$type])) {
          continue;
        }

        // Keeping track of applied filters so that the clear all filters link
        // knows which ones to remove.
        $applied_filters[] = $type;
        $link_query = $parameters;
        // Removing the current mapping from the query so that the cancel link
        // doesn't contain it.
        unset($link_query[$type]);
        $label = $mapping->label();
        $items[] = [
          '#type' => 'html_tag',
          '#tag' => 'a',
          '#attributes' => [
            'href' => Url::fromRoute('orlando_interface_search.page', [], ['query' => $link_query])->toString(),
            'title' => $this->t('Clear @label filter(s)', ['@label' => $label]),
            'class' => [
              'tw-m-1.5',
              'tw-flex',
              'tw-py-0.5',
              'tw-px-1',
              'tw-items-center',
              'tw-bg-gray-100',
              'hover:tw-bg-gray-200',
              'tw-rounded-full',
              'tw-no-underline',
            ],
          ],
          'label' => [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $this->t('@label: ', ['@label' => $label]),
          ],
          'selected' => [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#attributes' => [
              'class' => [
                'tw-font-bold',
              ],
            ],
            '#value' => $this->t('@selected X', ['@selected' => $mapping->getFacetWidget()->generateSelectedValuesString($parameters[$type])]),
          ],
        ];
      }
      if ($applied_filters) {
        $query = array_diff_key($parameters, array_flip($applied_filters));
        $items[] = [
          '#type' => 'link',
          '#title' => $this->t('Clear all filters'),
          '#url' => Url::fromRoute('orlando_interface_search.page', [], ['query' => $query]),
          '#attributes' => [
            'class' => [
              'tw-m-1.5',
              'tw-flex',
              'tw-underline',
              'hover:tw-no-underline',
              'focus:tw-no-underline',
            ],
          ],
        ];
      }
    }
    return $items;
  }

}
