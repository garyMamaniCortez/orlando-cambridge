<?php

namespace Drupal\orlando_interface_search\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\Context\RequestStackCacheContextBase;

/**
 * Defines the UrlCacheContext service with the ability to skip the page query
 * arg.
 *
 * Cache context ID: 'oi_url'.
 * Calculated cache context ID: 'url.oi_url:remove_page_param'.
 * (Will make it remove the page parameter).
 */
class UrlCacheContext extends RequestStackCacheContextBase implements CalculatedCacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Orlando Interface Url');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($flag = NULL) {
    $request = $this->requestStack->getCurrentRequest();
    if ($flag === NULL || $flag !== 'remove_page_param') {
      return $request->getUri();
    }

    $query = $request->query;
    $query->remove('page');
    $request->server->set('QUERY_STRING', http_build_query($query->all()));
    return $request->getUri();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($flag = NULL) {
    return new CacheableMetadata();
  }

}
