<?php

namespace Drupal\orlando_interface_search\Cache;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\Cache;
use Drupal\orlando_interface_search\Utility\QueryHelper;

class BasexResultsCache {

  /**
   * Contains all data that should be written/read from cache.
   */
  public $storage = [];

  /**
   * Which cache bin to store query results in.
   *
   * @var string
   */
  protected $resultsBin = 'data';

  /**
   * Stores the cache ID used for the results cache.
   *
   * The cache ID is stored in generateResultsKey() got executed.
   *
   * @var string
   */
  protected $resultsKey;

  protected $rawResultsKey;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|null
   */
  protected $cacheBackend;

  /**
   * The cache contexts manager.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager|null
   */
  protected $cacheContextsManager;

  /**
   * @var \Drupal\orlando_interface_search\Utility\QueryHelper
   */
  protected $query;

  public function init(QueryHelper $query) {
    $this->query = $query;
  }

  /**
   * Retrieves the cache backend.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache backend.
   */
  public function getCacheBackend() {
    return $this->cacheBackend ?: \Drupal::cache($this->resultsBin);
  }

  /**
   * Retrieves the cache contexts manager.
   *
   * @return \Drupal\Core\Cache\Context\CacheContextsManager
   *   The cache contexts manager.
   */
  public function getCacheContextsManager() {
    return $this->cacheContextsManager ?: \Drupal::service('cache_contexts_manager');
  }

  /**
   * Save data to the cache.
   */
  public function cacheSet($is_raw_results = FALSE) {
    $query = $this->query;
    $data = [
      'total_rows' => $query->getResultSets()->getDocumentItemsCount(),
      'results' => $query->getResultSets(),
    ];
    if (!$is_raw_results) {
      $data['view_results'] = $query->viewResult;
      $data['current_page'] = $query->viewPagerCurrentPage;
    }
    $results_key = $this->generateResultsKey($is_raw_results);
    $expire = Cache::PERMANENT;
    $this->getCacheBackend()
      ->set($results_key, $data, $expire, $this->getCacheTags());
  }

  public function cacheGet($is_raw_results = FALSE) {
    $result_key = $this->generateResultsKey($is_raw_results);
    if ($cache = $this->getCacheBackend()->get($result_key)) {
      $this->query->populateViewPropertiesWithCachedData($cache);
      $this->query->postExecuteAddResults($cache->data['results'], $is_raw_results);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Calculates and sets a cache ID used for the result cache.
   *
   * @return string
   *   The generated cache ID.
   */
  public function generateResultsKey($is_raw_results = FALSE) {
    if (!isset($this->resultsKey) || !isset($this->rawResultsKey)) {
      $query = $this->query;
      $prefix = 'orlando_interface_search:page:results:';
      if (!$is_raw_results) {
        $key_data = [
          'pager' => [
            'page' => $query->viewPagerCurrentPage,
            'items_per_page' => $query->viewPagerNumPerPage,
            'offset' => $query->viewOffset,
          ],
        ];
        $cache_contexts = ['url', 'url.query_args'];
      }
      else {
        $key_data = [];
        $cache_contexts = ['oi_url:remove_page_param'];
      }
      $cache_contexts_manager = $this->getCacheContextsManager();
      $key_data += $cache_contexts_manager->convertTokensToKeys($cache_contexts)
        ->getKeys();
      $result_key = $prefix . Crypt::hashBase64(serialize($key_data));
      if (!$is_raw_results) {
        $this->resultsKey = $result_key;
      }
      else {
        $this->rawResultsKey =  $result_key;
      }
    }
    return $is_raw_results ? $this->rawResultsKey : $this->resultsKey;
  }

  /**
   * Gets an array of cache tags for the current list
   *
   * @return string[]
   *   An array of cache tags.
   */
  public function getCacheTags() {
    return [
      'orlando_interface_search_list:basex_index',
      'profile_event_list',
      'node_list:author_profile',
      // @todo check to see if we can make this more granular.
      'taxonomy_term_list',
    ];
  }

  /**
   * Gets the max age for the current view.
   *
   * @return int
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

}
