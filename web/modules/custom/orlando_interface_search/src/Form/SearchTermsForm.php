<?php


namespace Drupal\orlando_interface_search\Form;


use Drupal\Core\Form\FormStateInterface;

class SearchTermsForm extends BaseSearchForm {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return static::FORM_ID_PREFIX . 'terms_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('display_keys_input', TRUE);
    $form = parent::buildForm($form, $form_state);
    $form['keys']['#required'] = TRUE;
    return $form;
  }

  protected function getQueries(array $form, FormStateInterface $form_state): array {
    return [
      'keys' => explode(' ', $form_state->getValue('keys')),
    ];
  }

}
