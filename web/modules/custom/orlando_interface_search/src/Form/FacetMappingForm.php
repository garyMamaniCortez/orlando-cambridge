<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Utility\Error;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for creating and editing facet mappings.
 */
class FacetMappingForm extends EntityForm {

  protected $facetWidgetPluginManager;

  public function __construct(BaseXFacetWidgetPluginManager $facet_widget_manager, MessengerInterface $messenger) {
    $this->facetWidgetPluginManager = $facet_widget_manager;
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.orlando_interface_search.basex_facet_widget'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    if ($form_state->isRebuilding()) {
      $this->entity = $this->buildEntity($form, $form_state);
    }

    $form = parent::form($form, $form_state);

    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping */
    $facet_mapping = $this->getEntity();
    // Set the page title according to whether we are creating or editing the
    // server.
    if ($facet_mapping->isNew()) {
      $form['#title'] = $this->t('Add basex facet mapping');
    }
    else {
      $form['#title'] = $this->t('Edit basex facet mapping %label', ['%label' => $facet_mapping->label()]);
    }

    $form['#facet_widget_config_form_id'] = 'orlando-interface-search--facet-mapping--widget-config-form';
    $this->buildEntityForm($form, $form_state, $facet_mapping);

    if ($form) {
      $this->buildFacetWidgetConfigForm($form, $form_state, $facet_mapping);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    if ($form === []) {
      return [];
    }

    return parent::actions($form, $form_state);
  }

  public function buildEntityForm(array &$form, FormStateInterface $form_state, FacetMappingInterface $facet_mapping) {
    $id = $facet_mapping->isNew() ? NULL : $facet_mapping->id();
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Enter the displayed title for the mapping.'),
      '#default_value' => $facet_mapping->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $id,
      '#maxlength' => 50,
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => '\Drupal\orlando_interface_search\Entity\FacetMapping::load',
        'source' => ['title'],
      ],
      '#disabled' => !$facet_mapping->isNew(),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Only enabled facet mappings can be displayed in the facet form.'),
      '#default_value' => $facet_mapping->status(),
    ];

    if ($id) {
      $file_exists = is_file($facet_mapping->getFileUri());
      $form['mapping_file_upload'] = [
        '#type' => 'file',
        '#title' => $this->t('Facet mapping file'),
        '#multiple' => FALSE,
        '#description' => $file_exists ? $this->t('Replace the existing mapping file.') : $this->t('Upload the mapping file.'),
        '#upload_validators' => [
          'file_validate_extensions' => ['xml'],
        ],
      ];
    }

    $facet_widgets = $this->facetWidgetPluginManager->getDefinitions();
    $facet_widget_options = [];
    foreach ($facet_widgets as $facet_widget_id => $definition) {
      $config = $facet_widget_id === $facet_mapping->getFacetWidgetId() ? $facet_mapping->getFacetWidgetConfig() : [];
      $config['#facet_mapping'] = $facet_mapping;
      try {
        $facet_widget = $this->facetWidgetPluginManager
          ->createInstance($facet_widget_id, $config);
      }
      catch (PluginException $e) {
        continue;
      }
      $facet_widget_options[$facet_widget_id] = Markup::create(Html::escape($facet_widget->label()));
    }
    asort($facet_widget_options, SORT_NATURAL | SORT_FLAG_CASE);
    if ($facet_widget_options) {
      if (count($facet_widget_options) == 1) {
        $facet_mapping->set('facet_widget', array_key_first($facet_widget_options));
      }
      $form['facet_widget'] = [
        '#type' => 'radios',
        '#title' => $this->t('Facet widget'),
        '#description' => $this->t('Choose a facet widget to use for this facet mapping.'),
        '#options' => $facet_widget_options,
        '#default_value' => $facet_mapping->getFacetWidgetId(),
        '#required' => TRUE,
        '#disabled' => !$facet_mapping->isNew(),
        '#ajax' => [
          'callback' => [get_class($this), 'buildAjaxFacetWidgetConfigForm'],
          'wrapper' => $form['#facet_widget_config_form_id'],
          'method' => 'replace',
          'effect' => 'fade',
        ],
      ];
    }
    else {
      $this->messenger()->addError('There are no facet widget plugins available for facets. Please install a module which provides one or implement one within this module to proceed.');
      $form = [];
    }
  }

  public function buildFacetWidgetConfigForm(array &$form, FormStateInterface $form_state, FacetMappingInterface $facet_mapping) {
    $form['facet_widget_config'] = [];
    if ($facet_mapping->hasValidFacetWidget()) {
      $facet_widget = $facet_mapping->getFacetWidget();
      $form_state->set('facet_widget', $facet_widget->getPluginId());

      if ($form_state->isRebuilding()) {
        $this->messenger->addWarning($this->t('Please configure the selected facet widget.'));
      }
      // Attach the facet widget plugin configuration form.
      $facet_widget_form_state = SubformState::createForSubform($form['facet_widget_config'], $form, $form_state);
      $form['facet_widget_config'] = $facet_widget->buildConfigurationForm($form['facet_widget_config'], $facet_widget_form_state);

      // Modify the facet widget plugin configuration container element.
      $form['facet_widget_config']['#type'] = 'details';
      $form['facet_widget_config']['#title'] = $this->t('Configure %plugin facet widget', ['%plugin' => $facet_widget->label()]);
      $form['facet_widget_config']['#open'] = TRUE;
    }
    elseif (!$facet_mapping->isNew()) {
      $this->messenger->addError($this->t('The facet widget plugin is missing or invalid.'));
      return;
    }
    $form['facet_widget_config'] += [
      '#type' => 'container',
    ];
    $form['facet_widget_config']['#attributes']['id'] = $form['#facet_widget_config_form_id'];
    $form['facet_widget_config']['#tree'] = TRUE;
  }

  /**
   * Handles switching the selected facet widget plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxFacetWidgetConfigForm(array $form, FormStateInterface $form_state) {
    return $form['facet_widget_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping */
    $facet_mapping = $this->getEntity();

    $facet_widget_id = $facet_mapping->getFacetWidgetId();
    if ($facet_widget_id != $form_state->get('facet_widget')) {
      // Checking if the facet widget plugin changed during initial mapping
      // creation.
      $input = &$form_state->getUserInput();
      $input['facet_widget_config'] = [];
      $form_state->setRebuild();
    }
    elseif ($facet_mapping->hasValidFacetWidget()) {
      $facet_widget = $facet_mapping->getFacetWidget();
      $facet_widget_form_state = SubformState::createForSubform($form['facet_widget_config'], $form, $form_state);
      $facet_widget->validateConfigurationForm($form['facet_widget_config'], $facet_widget_form_state);
    }

    // Deal with the mapping file upload or replacement.
    $file_exists = is_file($facet_mapping->getFileUri());
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['mapping_file_upload'])) {
      /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file_upload */
      $file_upload = $all_files['mapping_file_upload'];
      if ($file_upload->isValid()) {
        $form_state->setValue('mapping_file_upload', $file_upload->getRealPath());
      }
    }
    elseif (!$facet_mapping->isNew() && !$file_exists) {
      $form_state->setError($form['mapping_file_upload'], $this->t('Facet mapping file field is required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping */
    $facet_mapping = $this->getEntity();
    if ($facet_mapping->hasValidFacetWidget()) {
      $facet_widget = $facet_mapping->getFacetWidget();
      $facet_widget_form_state = SubformState::createForSubform($form['facet_widget_config'], $form, $form_state);
      $facet_widget->submitConfigurationForm($form['facet_widget_config'], $facet_widget_form_state);

      $mapping_file_upload = $form_state->getValue('mapping_file_upload', '');
      if ($mapping_file_upload) {
        /** @var \Drupal\Core\File\FileSystemInterface $file_system */
        $file_system = \Drupal::service('file_system');
        $upload_location = 'private://orlando-2-0-c-modelling/entities/';
        $filename = $upload_location . $facet_mapping->id() . '_mapping.xml';
        $destination = $file_system->getDestinationFilename($filename, FileSystemInterface::EXISTS_REPLACE);
        if ($destination) {
          $file_system->moveUploadedFile($mapping_file_upload, $destination);
          $file_system->chmod($destination);
        }
      }
    }

    return $facet_mapping;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if (!$form_state->isRebuilding()) {
      try {
        /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping */
        $facet_mapping = $this->getEntity();
        $facet_mapping->save();
        $this->messenger->addStatus($this->t('The facet mapping was successfully saved.'));
        if (!is_file($facet_mapping->getFileUri())) {
          $this->messenger->addWarning($this->t('The facet mapping file was not found please upload it!'));
          $form_state->setRedirect('entity.basex_facet_mapping.edit_form', ['basex_facet_mapping' => $facet_mapping->id()]);
        }
        else {
          $form_state->setRedirect('entity.basex_facet_mapping.canonical', ['basex_facet_mapping' => $facet_mapping->id()]);
        }
      }
      catch (EntityStorageException $e) {
        $form_state->setRebuild();

        $message = '%type: @message in %function (line %line of %file).';
        $variables = Error::decodeException($e);
        $this->getLogger('orlando_interface_search')->error($message, $variables);

        $this->messenger->addError($this->t('The facet mapping could not be saved.'));
      }
    }
  }

}
