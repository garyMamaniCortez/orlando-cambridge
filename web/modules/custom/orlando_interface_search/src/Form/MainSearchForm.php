<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainSearchForm extends BaseSearchForm {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return static::FORM_ID_PREFIX . 'main_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('display_keys_input', TRUE);
    $parameters = $this->getRequest()->query->all();
    $form = parent::buildForm($form, $form_state);

    $form['keys']['#required'] = FALSE;
    $form['keys']['#default_value'] = !empty($parameters['keys']) ? implode(' ', $parameters['keys']) : '';
    $facet_mappings = $this->getFacetMappings();
    if ($facet_mappings) {
      $facet_results = $this->getQueryHelper()->getResultSets()->getFacets();
      $form['#query_parameters'] = $parameters;
      $form['#facet_results'] = $facet_results;
      $form['#facet_mappings'] = $facet_mappings;
      foreach ($facet_mappings as $type => $facet_mapping) {
        if (!isset($facet_results[$type]['items'])) {
          continue;
        }
        $form[$type] = $facet_mapping->getFacetWidget()->formElement($form, $form_state, $facet_results[$type]['items']);
      }
    }

    return $form;
  }

  protected function getQueries(array $form, FormStateInterface $form_state): array {
    $queries = [
      'keys' => explode(' ', $form_state->getValue('keys')),
    ];
    if (!empty($form['#facet_results'])) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface[] $facet_mappings */
      $facet_mappings = $form['#facet_mappings'];
      foreach ($form['#facet_results'] as $type => $result) {
        if (!isset($facet_mappings[$type])) {
          continue;
        }
        $widget = $facet_mappings[$type]->getFacetWidget();
        $values = $widget->extractSelectedValues($form_state->getValue([$type, 'value']));
        if ($values) {
          $queries[$type] = $values;
        }
      }
    }

    // Let put bak the sort if available.
    if (isset($form['#query_parameters']['sort'])) {
      $queries['sort'] = $form['#query_parameters']['sort'];
    }

    return $queries;
  }

}
