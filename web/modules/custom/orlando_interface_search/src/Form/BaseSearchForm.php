<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseSearchForm extends FormBase {

  const FORM_ID_PREFIX = 'orlando_interface_search__';

  protected $currentRequest;

  protected $queryHelper;

  protected $facetMappings;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $options = $build_info['args'][0] ?? [];
    $placeholder = $this->t('@placeholder', [
      '@placeholder' => $options['keys_placeholder'] ?? 'search',
    ]);
    if (!empty($options['introduction_text'])) {
      $form['intro'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#attributes' => [
          'class' => [
            'tw-mb-4',
            'tw-leading-tight',
            'tw-text-xl',
            'sm:tw-mb-15',
            'sm:tw-text-4xl',
          ],
        ],
        '#value' => nl2br($options['introduction_text']),
        '#weight' => -99,
      ];
    }
    $form['keys'] = [
      '#type' => 'search',
      '#title' => $this->t('Search'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#size' => 30,
      '#placeholder' => $placeholder,
      '#access' => (bool) $form_state->get('display_keys_input'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 99,
    ];
    $form['#cache']['contexts'] = [
      'url.path',
      'url.query_args'
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('orlando_interface_search.page', [], [
      'query' => $this->getQueries($form, $form_state),
    ]);
  }

  abstract protected function getQueries(array $form, FormStateInterface $form_state): array;

  /**
   * Gets the query helper.
   *
   * @return \Drupal\orlando_interface_search\Utility\QueryHelper
   *   The query helper.
   */
  public function getQueryHelper() {
    if (!$this->queryHelper) {
      $this->queryHelper = \Drupal::service('orlando_interface_search.query');
    }
    return $this->queryHelper;
  }

  /**
   * Gets the facet mappings.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface[]
   *   The facet mappings.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFacetMappings() {
    if (!$this->facetMappings) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
      $storage = \Drupal::entityTypeManager()->getStorage('basex_facet_mapping');
      $this->facetMappings = $storage->loadEnabledMappings();
    }
    return $this->facetMappings;
  }

  public function currentRequest() {
    if (!$this->currentRequest) {
      $this->currentRequest = \Drupal::requestStack()->getCurrentRequest();
    }
    return $this->currentRequest;
  }

}
