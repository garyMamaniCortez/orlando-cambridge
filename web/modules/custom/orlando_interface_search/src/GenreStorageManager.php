<?php

namespace Drupal\orlando_interface_search;

use League\Csv\Reader;

class GenreStorageManager {

  const STATIC_DATA_FOLDER = '/assets/static_data';
  const GENRE_CSV_FILE = self::STATIC_DATA_FOLDER . '/genre.csv';
  const ROOT_PARENT = '__root__';

  protected $csv;

  protected $records;

  protected $treeGenres = [];

  protected $treeParents = [];

  /**
   * Array of genre parents keyed parent machine name.
   *
   * @var array
   */
  protected $treeChildren = [];

  protected $trees = [];

  public function __construct() {

  }

  public function load(string $machine_name) {

  }

  public function loadMultiple(array $machine_names) {

  }

  public function getRecords() {
    return $this->getCsv()->getRecords();
  }

  private function getCsv() {
    if (!isset($this->csv)) {
      $name = 'orlando_interface_search';
      $app_root = \Drupal::root();
      $path = drupal_get_path('module', $name);
      $file = $app_root . '/' . $path . static::GENRE_CSV_FILE;
      $this->csv = Reader::createFromPath($file, 'r');
      $this->csv->setHeaderOffset(0);
    }
    return $this->csv;
  }

  public function loadTree() {
    $this->treeChildren = [];
    $this->treeParents = [];
    $this->treeGenres = [];
    $parent = static::ROOT_PARENT;

    $records = $this->getRecords();
    foreach ($records as $genre) {
      $machine_name = $genre['machineName'];
      $parents = explode(',', $genre['parents']);
      foreach ($parents as $parent_machine_name) {
        if (empty($parent_machine_name)) {
          $parent_machine_name = static::ROOT_PARENT;
        }
        $this->treeChildren[$parent_machine_name][] = $machine_name;
      }
      if ($parents[0] === '') {
        $parents[0] = static::ROOT_PARENT;
      }
      $this->treeParents[$machine_name] = $parents;
      $this->treeGenres[$machine_name] = $genre;
    }

    $max_depth = count($this->treeChildren);
    $tree = [];

    $process_parents = [];
    $process_parents[] = $parent;
    while (count($process_parents)) {
      $parent = array_pop($process_parents);
      $depth = count($process_parents);
      if ($max_depth > $depth && !empty($this->treeChildren[$parent])) {
        $has_children = FALSE;
        $child = current($this->treeChildren[$parent]);
        do {
          if (empty($child)) {
            break;
          }
          $genre = (object) $this->treeGenres[$child];
          if (isset($this->treeParents[$genre->machineName])) {
            $genre = clone $genre;
          }
          $genre->level = $depth;
          $machine_name = $genre->machineName;
          $genre->parents = $this->treeParents[$machine_name];
          $tree[$genre->machineName] = $genre;
          if (!empty($this->treeChildren[$machine_name])) {
            $has_children = TRUE;

            $process_parents[] = $parent;
            $process_parents[] = $machine_name;

            reset($this->treeChildren[$machine_name]);
            next($this->treeChildren[$parent]);
            break;
          }
        } while ($child = next($this->treeChildren[$parent]));

        if (!$has_children) {
          reset($this->treeChildren[$parent]);
        }
      }
    }
    $this->tree = $tree;
    return $this->tree;
  }

}
