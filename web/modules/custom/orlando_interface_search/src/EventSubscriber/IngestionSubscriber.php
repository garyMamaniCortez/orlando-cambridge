<?php

namespace Drupal\orlando_interface_search\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_ingestion\Event\IngestionEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\Event\IngestionStartEvent;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IngestionSubscriber implements EventSubscriberInterface {

  protected $database;

  protected $entityTypeManager;

  /**
   * IngestionSubscriber constructor.
   *
   * @param \Drupal\orlando_interface_search\Driver\Database\basex\Connection $connection
   *   The basex database connection.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      IngestionEvents::INGESTION_STARTED => ['onIngestionStart'],
      IngestionEvents::CONNECTION_INGESTION => ['onIngestion'],
      IngestionEvents::ITEM_INGESTION => ['onIngestion'],
      IngestionEvents::ITEM_DELETE => ['onDeletion'],
      IngestionEvents::INGESTION_FINISHED => ['onIngestionFinished'],
    ];
  }

  /**
   * Inserts a document in basex on ingestion.
   *
   * @param \Drupal\orlando_interface_ingestion\Event\IngestionEvent $event
   *   The ingestion event.
   */
  public function onIngestion(IngestionEvent $event) {
    $this->database->insert([
      'name' => $event->getId(),
      'document_type' => $event->getType(),
      'filepath' => $event->getFilepath(),
    ]);
  }

  public function onDeletion(IngestionEvent $event) {
    try {
      $this->database->delete([
        'name' => $event->getId(),
        'document_type' => $event->getType(),
      ]);
    }
    catch (\Exception $e) {}
  }

  public function onIngestionFinished() {
    $this->database->openDatabase();
    $this->database->initSettings();
    $this->database->createIndex();
    $this->database->closeSession();
  }

  public function onIngestionStart(IngestionStartEvent $event) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $facet_mapping_storage */
    $facet_mapping_storage = $this->entityTypeManager->getStorage('basex_facet_mapping');
    $facet_mappings = $facet_mapping_storage->loadEnabledMappings();
    foreach ($facet_mappings as $mapping) {
      $options['is_mapping'] = TRUE;
      $options['name'] = $mapping->id();
      $options['filepath'] = $mapping->getFileUri();
      $this->database->insert($options);
    }
  }

}
