<?php

namespace Drupal\orlando_interface_search\Driver\Database\basex;

use Drupal\Core\Database\Log;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\orlando_interface_search\BaseXClient\Session;
use Drupal\orlando_interface_search\Item\Document;
use Drupal\orlando_interface_search\Item\Facet;
use Drupal\orlando_interface_search\LoggerTrait;
use Drupal\orlando_interface_search\Query\ResultSet;
use Drupal\orlando_interface_search\Utility\QueryHelper;


class Connection {

  use LoggerTrait;

  const ADD_CMD = 9;

  /**
   * The database target this connection is for.
   *
   * We need this information for later auditing and logging.
   *
   * @var string|null
   */
  protected $target = NULL;

  /**
   * The key representing this connection.
   *
   * The key is a unique string which identifies a database connection. A
   * connection can be a single server or a cluster of primary and replicas
   * (use target to pick between primary and replica).
   *
   * @var string|null
   */
  protected $key = NULL;

  /**
   * The current database logging object for this connection.
   *
   * @var \Drupal\Core\Database\Log|null
   */
  protected $logger = NULL;

  /**
   * Index of what driver-specific class to use for various operations.
   *
   * @var array
   */
  protected $driverClasses = [];

  protected $session;

  protected $connectionOptions;

  /**
   * @var \Drupal\orlando_interface_search\Query\ResultSet
   */
  protected $searchResults;

  protected $searchExecuted = FALSE;

  /**
   * @var \Drupal\orlando_interface_search\Utility\QueryHelper
   */
  protected $queryHelper;

  /**
   * Initialize Connection Class.
   *
   * @param \Drupal\orlando_interface_search\BaseXClient\Session $session
   * @param array $connection_options
   */
  public function __construct(Session $session, array $connection_options) {
    $this->session = $session;
    $this->connectionOptions = $connection_options;
  }

  public function queryInit(QueryHelper $query_helper) {
    $this->searchResults = new ResultSet($query_helper);
    $this->queryHelper = $query_helper;
    return $this;
  }

  /**
   * Tells this connection object what its target value is.
   *
   * This is needed for logging and auditing. It's sloppy to do in the
   * constructor because the constructor for child classes has a different
   * signature. We therefore also ensure that this function is only ever
   * called once.
   *
   * @param string $target
   *   (optional) The target this connection is for.
   */
  public function setTarget($target = NULL) {
    if (!isset($this->target)) {
      $this->target = $target;
    }
  }

  /**
   * Returns the target this connection is associated with.
   *
   * @return string|null
   *   The target string of this connection, or NULL if no target is set.
   */
  public function getTarget(): ?string {
    return $this->target;
  }

  /**
   * Tells this connection object what its key is.
   *
   * @param string $key
   *   The key this connection is for.
   */
  public function setKey($key) {
    if (!isset($this->key)) {
      $this->key = $key;
    }
  }

  /**
   * Returns the key this connection is associated with.
   *
   * @return string|null
   *   The key of this connection, or NULL if no key is set.
   */
  public function getKey() {
    return $this->key;
  }

  /**
   * Associates a logging object with this connection.
   *
   * @param \Drupal\Core\Database\Log $logger
   *   The logging object we want to use.
   */
  public function setLogger(Log $logger) {
    $this->logger = $logger;
  }

  /**
   * Gets the current logging object for this connection.
   *
   * @return \Drupal\Core\Database\Log|null
   *   The current logging object for this connection. If there isn't one,
   *   NULL is returned.
   */
  public function getLogger() {
    return $this->logger;
  }

  public function query(string $query, array $args = [], array $options = []) {
    return $this->getResults($query);
  }

  /**
   * @param array $options
   *
   * @return string
   */
  public function insert(array $options) {
    $is_default = empty($options['is_mapping']);
    $this->openDatabase($is_default);
    $class = $this->getDriverClass('Insert');
    $database = $is_default ? $this->connectionOptions['database'] : $this->connectionOptions['mapping_db'];
    $queryString = "";
    $xml = file_get_contents($options['filepath']);

    $uri = $this->getDocumentUriFromOptions($options);
    $this->session->replace($uri, $xml);
    if (!$is_default) {
      return;
    }

    $queryString .= "import module namespace extract = 'org.basex.modules.extract'; ";
    $queryString .= "let \$uri := '/$database$uri' ";
    $queryString .= "return extract:extractFacetsForDatabase(\$uri)";
    /** @var \Drupal\orlando_interface_search\Driver\Database\basex\Insert $insert */
    $insert = new $class($this->session, $queryString);
    try {
      return $insert->execute();
    }
    catch (\Exception $exception) {
      $this->drupalLogException($exception);
    }
    return;
  }

  public function delete(array $options) {
    $this->openDatabase();
    $class = $this->getDriverClass('Delete');
    /** @var \Drupal\orlando_interface_search\Driver\Database\basex\Delete $delete */
    $delete = new $class($this->session, 'DELETE ' . $this->getDocumentUriFromOptions($options));
    return $delete->execute();
  }

  public function search(array $parameters) {
    $xquery = 'import module namespace search = "org.basex.modules.search"; ';
    $xquery .= sprintf('let $database := "%s" ', $this->connectionOptions['database']);

    $search_terms = 'let $searchTerms := (';
    if (!empty($parameters['keys'])) {
      $search_terms .= '"' . implode('","' , $parameters['keys']) . '"';
      unset($parameters['keys']);
    }
    $search_terms .= ') ';

    $xquery .= $search_terms;
    $xquery .= $this->buildFacetsXquery($parameters);
    $xquery .= $this->buildSortingXquery($parameters);
    $xquery .= 'return search:getCompleteSearchResults($database, $searchTerms, $filters, $sorting, $direction)';

    $start = microtime(TRUE);
    $results_xml = '';
    try {
      $results_xml = $this->session->query($xquery)->execute();
    }
    catch (\Exception $exception) {
      $this->drupalLogException($exception);
    }
    $this->session->close();
    $execute_time = microtime(TRUE) - $start;
    $message = 'Xquery: <pre><code>@xquery</code></pre><br/>Parameters: <pre><code>@parameters</code></pre><br/>Execution time(ms): @time';
    $variables = [
      '@xquery' => $xquery,
      '@time' => $execute_time,
      '@parameters' => print_r($parameters, TRUE),
    ];
    $this->getDrupalLogger()->log(RfcLogLevel::INFO, $message, $variables);
    return json_decode(json_encode((array) simplexml_load_string($results_xml)), 1);
  }

  public function buildSortingXquery($parameters) {
    $mapping = static::sortingMapping(FALSE);
    $sorting_key = $parameters['sort'] ?? 'relevance';
    // Ensure that we only allow supported sorting values.
    if (!is_string($sorting_key) || !isset($mapping[$sorting_key])) {
      $sorting_key = 'relevance';
    }
    $sorting = $mapping[$sorting_key]['sorting'];
    $direction = $mapping[$sorting_key]['direction'];
    $xquery = 'let $sorting := "' . $sorting . '" ';
    $xquery .= 'let $direction := "' . $direction . '" ';
    return $xquery;
  }

  public function buildFacetsXquery($parameters) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('basex_facet_mapping');
    $xquery = 'let $filters := map{';
    $mappings = $storage->loadEnabledMappings();
    foreach ($mappings as $type => $mapping) {
      if (empty($parameters[$type])) {
        continue;
      }
      $xquery .= $mapping->getFacetWidget()->buildXQueryString($parameters[$type]) . ', ';
    }
    $xquery = rtrim($xquery, ', ');
    $xquery .= '} ';
    return $xquery;
  }

  public function searchExecute() {
    if ($this->hasSearchExecuted()) {
      return $this->searchResults;
    }

    $this->searchExecuted = TRUE;

    $parameters = $this->queryHelper->getSearchParameters();
    $raw_results = $this->search($parameters);

    $facets = $raw_results['facets']['facet'] ?? [];
    $this->searchResults->setRawFacets($facets);

    $documents = $raw_results['documents']['document'] ?? [];
    $this->searchResults->setRawDocuments($documents);

    // Store the results in the static cache.
    $this->queryHelper->postExecuteAddResults($this->searchResults, TRUE);

    return $this->searchResults;
  }

  public function createIndex() {
    $this->session->execute('CREATE INDEX TEXT');
    $this->session->execute('CREATE INDEX ATTRIBUTE');
    $this->session->execute('CREATE INDEX FULLTEXT');
  }

  public function dropIndex() {
    $this->session->execute('DROP INDEX TEXT');
    $this->session->execute('DROP  INDEX ATTRIBUTE');
    $this->session->execute('DROP INDEX FULLTEXT');
  }

  public function optimize(bool $all = FALSE) {
    $this->openDatabase();
    $cmd = 'OPTIMIZE';
    $cmd = $all ? $cmd . ' ALL' : $cmd;
    $this->session->execute($cmd);
    $this->closeSession();
  }

  public function initSettings() {
    $this->session->execute('SET INTPARSE true');
    $this->session->execute('SET STEMMING true');
    //    $this->session->execute('SET STOPWORDS "/srv/basex/repo/stopwords.txt"');
  }

  /**
   * Gets the driver-specific override class if any for the specified class.
   *
   * @param string $class
   *   The class for which we want the potentially driver-specific class.
   *
   * @return string
   *   The name of the class that should be used for this driver.
   */
  public function getDriverClass($class) {
    if (empty($this->driverClasses[$class])) {
      $driver_class = $this->connectionOptions['namespace'] . '\\' . $class;
      $this->driverClasses[$class] = $driver_class;
    }
    return $this->driverClasses[$class];
  }

  public static function open(array &$connection_options = []) {
    $host = $connection_options['host'];
    $port = $connection_options['port'];
    $username = $connection_options['username'];
    $password = $connection_options['password'];
    return new Session($host, $port, $username, $password);
  }

  /**
   * Close Session
   *
   *  Ends database session
   */
  public function closeSession() {
    $this->session->close();
    return $this;
  }

  /**
   * Open Database
   *
   *  Opens database (uses the database that was initialized)
   */
  public function openDatabase($default = TRUE) {
    if ($default) {
      $database = $this->connectionOptions['database'];
    }
    else {
      // We are opening the mapping database.
      $database = $this->connectionOptions['mapping_db'];
    }
    try {
      $this->session->execute("OPEN $database");
    }
    catch (\Exception $e) {
      watchdog_exception('orlando_interface_ingestion', $e);
      throw new \Exception("Failed to open database $database");
    }
    return $this;
  }

  /**
   * Get Query Results
   *
   * Gets query results as an array
   *
   * @param string $queryString
   * String xquery to be sent to BaseX
   */
  public function getResults(string $queryString): array {
    $query = $this->session->query($queryString);
    // Get results
    try {
      $results = [];
      while ($query->more()) {
        $results[] = $query->next();
      }
    }
    catch (\Exception $e) {
      return ['error' => $e->getMessage()];
    }
    $query->close();
    return $results;
  }

  public function hasSearchExecuted() {
    return $this->searchExecuted;
  }

  public static function sortingMapping($as_options = TRUE) {
    $query_parameter_keys = ['relevance', 'az', 'za', 'old', 'new'];
    if ($as_options) {
      return array_combine($query_parameter_keys, [
        new TranslatableMarkup('Relevance'),
        new TranslatableMarkup('A-Z'),
        new TranslatableMarkup('Z-A'),
        new TranslatableMarkup('Oldest first'),
        new TranslatableMarkup('Newest first'),
      ]);
    }
    return array_combine($query_parameter_keys, [
      ['sorting' => 'relevance', 'direction' => 'descending'],
      ['sorting' => 'alphabetical', 'direction' => 'ascending'],
      ['sorting' => 'alphabetical', 'direction' => 'descending'],
      ['sorting' => 'chronological', 'direction' => 'ascending'],
      ['sorting' => 'chronological', 'direction' => 'descending'],
    ]);
  }

  private function getDocumentUriFromOptions(array $options) {
    if (!empty($options['is_mapping'])) {
      return "/{$options['name']}";
    }
    return "/{$options['document_type']}/{$options['name']}";
  }

}
