<?php

namespace Drupal\orlando_interface_search\Item;

use Drupal\Core\StringTranslation\TranslatableMarkup;

class Facet {

  protected $type;

  protected $name;

  protected $count;

  protected $label;

  /**
   * FacetItem constructor.
   *
   * @param string $type
   * @param string $name
   * @param int $count
   */
  public function __construct(string $type, string $name, int $count) {
    $this->type = $type;
    $this->name = $name;
    $this->count = $count;
//    if ($this->type == 'RESULT') {
//      switch ($this->name) {
//        case 'author':
//          $this->label = new TranslatableMarkup('Author profile');
//          break;
//        case 'person':
//          $this->label = new TranslatableMarkup('Persons');
//          break;
//        case 'organization':
//          $this->label = new TranslatableMarkup('Organizations');
//          break;
//        case 'event':
//          $this->label = new TranslatableMarkup('Events');
//          break;
//        case 'title':
//          $this->label = new TranslatableMarkup('Bibliography');
//          break;
//        default:
//          $name = strtolower($this->name);
//          $this->label = new TranslatableMarkup('@name', [
//            '@name' => ucfirst($name),
//          ]);
//      }
//    }
//    else {
//      $name = strtolower($this->name);
//      $this->label = new TranslatableMarkup('@name', [
//        '@name' => ucfirst($name),
//      ]);
//    }
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return int
   */
  public function getCount() {
    return $this->count;
  }

}
