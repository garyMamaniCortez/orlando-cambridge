<?php

namespace Drupal\orlando_interface_search\Item;

class Genre {

  protected $machineName;
  protected $label;
  protected $parents;
  protected $level;
  protected $children;

  public function getMachineName() {
    return $this->machineName;
  }

  public function setMachineName($machineName) {
    $this->machineName = $machineName;
  }

  public function getLabel() {
    return $this->label;
  }

  public function setLabel($label) {
    $this->label = $label;
  }


  public function getParents() {
    return $this->parents;
  }

  public function setParents($parents) {
    $this->parents = is_string($parents) ? explode(',', $parents) : $parents;
  }

  public function getLevel() {
    return $this->level;
  }

  public function setLevel($level) {
    $this->level = $level;
  }

}
