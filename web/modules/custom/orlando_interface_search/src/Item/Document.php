<?php

namespace Drupal\orlando_interface_search\Item;

class Document implements \IteratorAggregate {

  protected $itemId;

  protected $hitCount;

  protected $name;

  protected $type;

  protected $rawHits;

  protected $hitValues;

  /**
   * @var \Drupal\orlando_interface_search\Item\Hit[]
   */
  protected $hits;

  public function __construct(array $attributes, array $raw_hits, string $item_id = '') {
    $this->itemId = $item_id ?: static::extractItemIdFromUri($attributes['uri']);
    $this->hitCount = $attributes['count'];
    $this->type = $attributes['documentType'];
    $this->name = $attributes['documentName'] ?? '';
    $this->rawHits = $this->hitCount == 1 ? [$raw_hits] : $raw_hits;
  }

  public function getId() {
    return $this->itemId;
  }

  public function getType() {
    return $this->type;
  }

  public function getName() {
    return $this->name;
  }

  public function getHits() {
    if (empty($this->hits) && !empty($this->rawHits)) {
      foreach ($this->rawHits as $hit) {
        $hit_id = $hit['context']['@attributes']['id'] ?? '';
        $has_value = FALSE;
        if (!$hit_id && !empty($hit['context']['@attributes']['value'])) {
          $hit_id = $hit['context']['@attributes']['value'];
          $has_value = TRUE;
        }
        $this->hits[] = new Hit($hit['xpath'], $hit_id, $has_value);
      }
    }
    return $this->hits;
  }

  public function getHitValues() {
    if (!isset($this->hitValues)) {
      $this->hitValues = [];
      foreach ($this->getHits() as $hit) {
        if ($hit->getValue()) {
          if ($hit->getXpath() != 'entity/person/identity/preferredForm/namePart') {
            $this->hitValues[] = $hit->getValue();
          }
        }
      }
    }
    return $this->hitValues;
  }

  public function getHitCount() {
    return (int) $this->hitCount;
  }

  public function getIterator() {
    return new \ArrayIterator($this->getHits());
  }

  public static function extractItemIdFromUri(string $uri) {
    // Get the last element of the uri after splitting it.
    $item_name = substr($uri, strrpos($uri, '/') + 1);
    $excluded_strings = [
      'orlando_',
      '.xml',
      'organizations',
      'persons',
      'bibls',
    ];
    // Clean the $item_name for any prefix and return the result.
    return str_replace($excluded_strings, '', $item_name);
  }

}
