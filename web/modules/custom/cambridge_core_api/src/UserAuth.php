<?php

namespace Drupal\cambridge_core_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;

/**
 * Gets user authentication data based on provided payload.
 */
class UserAuth implements UserAuthInterface {

  /**
   * The api config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a UserAuth.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param ClientInterface $http_client
   *   The HTTP client to get the user api data with.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $this->config = $config_factory->get('cambridge_core_api.settings');
    $this->httpClient = $http_client;
  }

  /**
   * @inheritdoc
   */
  public function authenticate(array $payload){
    $uri = $this->config->get('uri') . UserAuthInterface::ENDPOINT;
    $data = NULL;
    $logger = \Drupal::logger(ExternalAuthRunner::PROVIDER);

    try {
      $instantiated_payload = $this->instantiatePayload($payload);
      $params = Json::encode($instantiated_payload);
      $headers = [
        'Accept' => 'application/json',
        'X-API-KEY' => $this->config->get('api_key'),
        'Content-Type' => 'application/json',
      ];

      $response = new ApiResponse($this->httpClient
        ->request('POST', $uri, ['headers' => $headers, 'body' => $params]));
      $data = $response->getData() ? (object) $response->getData()[0] : NULL;
      if (!$data) {
        // Masking the password for logging purpose.
        if ($payload['authMethod'] === UserAuthInterface::BASIC_METHOD && !empty($instantiated_payload->password)) {
          $instantiated_payload->password = '******';
        }
        $logger->warning('We got an empty response using the following parameters: <pre>@payload</pre>', [
          '@payload' => Json::encode($instantiated_payload),
        ]);
      }
    }
    catch (\Exception $e) {
      $message = '%type: @message in %function (line %line of %file).';
      $variables = Error::decodeException($e);
      $logger->log(RfcLogLevel::ERROR, $message, $variables);
      return NULL;
    }

    return $data;
  }

  /**
   * Instantiates a payload array.
   *
   * @param array $payload
   *   The payload array.
   *
   * @return object
   *   The instantiated payload.
   */
  private function instantiatePayload(array $payload) {
    $payload += [
      'productId' => $this->config->get('product_id'),
    ];
    return (object) $payload;
  }

}
