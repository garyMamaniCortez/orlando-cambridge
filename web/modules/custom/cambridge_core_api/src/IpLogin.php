<?php

namespace Drupal\cambridge_core_api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\Validation;

class IpLogin implements IpLoginInterface {

  protected $currentRequest;

  protected $userAuth;

  protected $externalAuthRunner;

  public function __construct(RequestStack $request_stack, UserAuthInterface $user_auth, ExternalAuthRunnerInterface $external_auth) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->userAuth = $user_auth;
    $this->externalAuthRunner = $external_auth;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(bool $redirect_to_front = FALSE) {
    if (!($ip = static::getIpFromRequest($this->currentRequest))) {
      return;
    }

    $payload = [
      'authMethod' => UserAuthInterface::IP_METHOD,
      'ip' => $ip,
    ];
    if ($user_data = $this->userAuth->authenticate($payload)) {
      $user_data->ipLogin = $ip;
      $this->externalAuthRunner->loginRegister($user_data->identityId, $user_data, $redirect_to_front);
    }
  }

  public static function getIpFromRequest(Request $request) {
    $ip = $request->server->get('HTTP_X_FORWARDED_FOR');
    $validator = Validation::createValidatorBuilder()->getValidator();
    $constraint = new Ip();
    return $validator->validate($ip, [$constraint])->count() == 0 ? $ip : NULL;
  }

}
