<?php

namespace Drupal\cambridge_core_api;

use Symfony\Component\HttpFoundation\Request;

interface IpLogoutInterface {

  public function run(Request $request);

}
