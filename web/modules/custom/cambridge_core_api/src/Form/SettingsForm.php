<?php

namespace Drupal\cambridge_core_api\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a SettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    parent::__construct($config_factory);
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['cambridge_core_api.settings'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'cambridge_core_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cambridge_core_api.settings');

    $form['uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Server'),
      '#required' => TRUE,
      '#default_value' => $config->get('uri'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    $form['product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('product_id'),
    ];

    $form['ip_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable IP Login'),
      '#description' => $this->t('When an anonymous user accesses the login page of the site, the module will attempt to log them in automatically using their ip address.'),
      '#default_value' => $config->get('ip_login'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('cambridge_core_api.settings');
    $form_state->cleanValues();

    $uri = $form_state->getValue('uri');
    $api_key = $form_state->getValue('api_key');
    $product_id = $form_state->getValue('product_id');
    $ip_login = $form_state->getValue('ip_login');

    $old_uri = $config->get('uri');
    $config->set('uri', $uri);
    $old_api_key = $config->get('api_key');
    $config->set('api_key', $api_key);
    $old_product_id = $config->get('product_id');
    $config->set('product_id', $product_id);
    $old_ip_login = $config->get('ip_login');
    $config->set('ip_login', $ip_login);

    if ($uri != $old_uri || $api_key != $old_api_key || $product_id != $old_product_id || $ip_login != $old_ip_login) {
      $this->cacheTagsInvalidator->invalidateTags(['cambridge_core_api_login']);
    }

    $config->save();
  }

}
