<?php

namespace Drupal\cambridge_core_api;

interface IpLoginInterface {

  /**
   * Authenticates the user using their ip address and the cambridge core api.
   *
   * @param bool $redirect_to_front
   */
  public function authenticate(bool $redirect_to_front = FALSE);

}
