<?php

namespace Drupal\cambridge_core_api\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Component\HttpFoundation\Request;

/**
 * A policy evaluating to static::DENY for the user login page.
 */
class UserLoginPageRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    $route_match = RouteMatch::createFromRequest($request);
    if ($route_match && $route_match->getRouteName() === 'user.login') {
      return self::DENY;
    }
  }

}
