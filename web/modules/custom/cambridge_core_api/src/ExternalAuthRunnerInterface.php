<?php

namespace Drupal\cambridge_core_api;

interface ExternalAuthRunnerInterface {

  const PROVIDER = 'cambridge_core_api';
  const AUTH_LOGIN_COOKIE_FLAG = 'cambridge_core_api_auth_login';

  public function loginRegister(string $username, \stdClass $user_data, bool $redirect_to_front = FALSE);

}
