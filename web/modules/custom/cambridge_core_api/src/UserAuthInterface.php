<?php

namespace Drupal\cambridge_core_api;

interface UserAuthInterface {

  const BASIC_METHOD = 'BASIC';
  const IP_METHOD = 'IP';
  const AUTOLOGIN_METHOD = 'AUTOLOGIN';
  const ENDPOINT = '/auth/getWithEntitlement';

  /**
   * Authenticates the provided payload.
   *
   * @param array $payload
   *   The authentication payload.
   *
   * @return object|null
   *   The user information on success, or NULL on failure to authenticate.
   */
  public function authenticate(array $payload);

}
