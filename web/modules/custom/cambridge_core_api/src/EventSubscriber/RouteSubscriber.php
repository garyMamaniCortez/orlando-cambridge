<?php

namespace Drupal\cambridge_core_api\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @inheritDoc
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.login')) {
      $defaults = $route->getDefaults();
      unset($defaults['_form']);

      $defaults['_controller'] = '\Drupal\cambridge_core_api\Controller\IpLoginController::login';
      $route->setDefaults($defaults);
    }
  }

}
