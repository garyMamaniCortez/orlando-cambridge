<?php

namespace Drupal\cambridge_core_api\EventSubscriber;

use Drupal\cambridge_core_api\ExternalAuthRunnerInterface;
use Drupal\cambridge_core_api\IpLogoutInterface;
use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Url;
use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExternalAuthLoginSubscriber implements EventSubscriberInterface {

  const REDIRECT_COOKIE = 'CambridgeCoreApiRedirectToFrontPage';

  protected $currentRequest;

  protected $drupalKernel;

  protected $ipLogout;

  public function __construct(RequestStack $request_stack, DrupalKernelInterface $drupal_kernel, IpLogoutInterface $ip_logout) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->drupalKernel = $drupal_kernel;
    $this->ipLogout = $ip_logout;
  }

  /**
   * Runs on login to redirect the user to the front page if needed.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthLoginEvent $event
   *   The event.
   */
  public function onLogin(ExternalAuthLoginEvent $event) {
    if ($event->getProvider() !== ExternalAuthRunnerInterface::PROVIDER) {
      return;
    }

    $redirect = $this->currentRequest->cookies->get(static::REDIRECT_COOKIE, 0);
    if ($redirect) {
      // Remove the redirect to front flag.
      $this->currentRequest->cookies->remove(static::REDIRECT_COOKIE);

      $url = Url::fromRoute('<front>');
      $response = new RedirectResponse($url->toString());
      // Save the session so things like messages get saved.
      $this->currentRequest->getSession()->save();
      $response->prepare($this->currentRequest);
      // Making sure that the kernel event triggering is happening.
      $this->drupalKernel->terminate($this->currentRequest, $response);
      $response->send();
    }
  }

  /**
   * Runs on kernel request to attempt to logout the user who have used their ip
   * address to login but it has since changed.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function autoLogout(RequestEvent $event) {
    $request = $event->getRequest();
    // Let not try the auto-logout if this request is coming from login.
    $cookies = $request->cookies ?? NULL;
    $is_logging_in = $cookies ? $cookies->get(ExternalAuthRunnerInterface::AUTH_LOGIN_COOKIE_FLAG, FALSE) : FALSE;
    if (!$is_logging_in) {
      $this->ipLogout->run($request);
    }
    elseif ($cookies) {
      $request->cookies->remove(ExternalAuthRunnerInterface::AUTH_LOGIN_COOKIE_FLAG);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ExternalAuthEvents::LOGIN => ['onLogin'],
      KernelEvents::REQUEST => ['autoLogout'],
    ];
  }

}
