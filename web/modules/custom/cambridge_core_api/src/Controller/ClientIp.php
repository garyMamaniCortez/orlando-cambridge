<?php

namespace Drupal\cambridge_core_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\Constraints\IpValidator;
use Symfony\Component\Validator\Validation;

class ClientIp extends ControllerBase {

  public function test() {
    $request = \Drupal::request();
    $client_ip = $request->server->get('HTTP_X_FORWARDED_FOR');
    $validator = Validation::createValidatorBuilder()
      ->getValidator();
    $constraint = new Ip();
    $violations = $validator->validate($client_ip, $constraint);

    $build['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Below is the client IP detected by this module')
    ];
    $build['client_ip'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Client IP: @ip - Valid: @valid', [
        '@ip' => $client_ip,
        '@valid' => $violations->count() > 0 ? 'nope' : 'yep',
      ]),
      '#cache' => [
        'contexts' => ['ip'],
      ],
    ];
    return $build;
  }

}
