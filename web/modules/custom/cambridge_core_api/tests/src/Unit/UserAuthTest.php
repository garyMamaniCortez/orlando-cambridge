<?php

namespace Drupal\Tests\cambridge_core_api\Unit;

use Drupal\cambridge_core_api\UserAuth;
use Drupal\cambridge_core_api\UserAuthInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;

/**
 * Tests the UserAuth service.
 *
 * @coversDefaultClass \Drupal\cambridge_core_api\UserAuth
 *
 * @group cambridge_core_api
 */
class UserAuthTest extends UnitTestCase {

  /**
   * The handler for http client response.
   *
   * @var MockHandler
   */
  protected $mockHandler;

  /**
   * The user auth object under test.
   *
   * @var \Drupal\cambridge_core_api\UserAuthInterface
   */
  protected $userAuth;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->mockHandler = new MockHandler();

    $http_client = new Client(['handler' => $this->mockHandler]);
    $config_factory = $this->getConfigFactoryStub([
      'cambridge_core_api.settings' => [
        'uri' => 'https://example.com/v1',
        'api_key' => 'owi7vmcI5IU1K4RpFAa0paNuJPwaPkKX',
        'product_id' => '8476351GNCSCFLJX91QFCXLVOBNOO1AB',
      ],
    ]);
    $this->userAuth = new UserAuth($config_factory, $http_client);
  }

  /**
   * Tests the authenticate method all the 3 existing authMethod.
   *
   * @param string $fixture
   *   The auth method response fixture.
   * @param array $payload
   *   The payload.
   * @param string $property
   *   The response data object property to use for assert.
   * @param string $expected
   *   The expected property value.
   *
   * @covers ::authenticate
   *
   * @dataProvider providerAuthenticationMethods
   */
  public function testAuthenticate($fixture, $payload, $property, $expected) {
    $this->mockHandler->append(new Response(200, [], $fixture));
    $data = $this->userAuth->authenticate($payload);

    if ($property) {
      $this->assertEquals($expected, $data->$property);
    }
    else {
      $this->assertEmpty($data);
    }
  }

  /**
   * Provides test data for the supported authentication methods.
   *
   * @return array
   *   The data.
   */
  public function providerAuthenticationMethods() {
    $data = [];

    // Basic auth method.
    $fixture = file_get_contents(__DIR__ . '/../../fixtures/authentication/basic.json');
    $payload = [
      'authMethod' => UserAuthInterface::BASIC_METHOD,
      'username' => 'test.orlando@example.com',
      'password' => 'demopassowrd',
    ];
    $property = 'identityEmailAddress';
    $expected = 'test.orlando@example.com';
    $data[] = [$fixture, $payload, $property, $expected];

    // IP auth method.
    $fixture = file_get_contents(__DIR__ . '/../../fixtures/authentication/ip.json');
    $payload = [
      'authMethod' => UserAuthInterface::IP_METHOD,
      'ip' => '129.128.0.5',
    ];
    $property = 'identityDisplayName';
    $expected = 'University of Alberta';
    $data[] = [$fixture, $payload, $property, $expected];

    // AUTOLOGIN auth method.
    $fixture = file_get_contents(__DIR__ . '/../../fixtures/authentication/autologin.json');
    $payload = [
      'authMethod' => UserAuthInterface::AUTOLOGIN_METHOD,
      'referrerURL' => 'https://www.facebook.com/test1/',
      'autologinID' => 'a86pzvhgWaQD0Mao84360V2YKAy45673',
    ];
    $property = 'identityType';
    $expected = 'SOCIETY';
    $data[] = [$fixture, $payload, $property, $expected];

    // Failed call.
    $fixture = file_get_contents(__DIR__ . '/../../fixtures/authentication/failed.json');
    $payload = [];
    $property = '';
    $expected = '';
    $data[] = [$fixture, $payload, $property, $expected];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() {
    unset($this->mockHandler);
    unset($this->userAuth);
  }

}
