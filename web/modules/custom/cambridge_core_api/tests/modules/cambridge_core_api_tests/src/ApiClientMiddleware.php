<?php

namespace Drupal\cambridge_core_api_tests;

use Drupal\cambridge_core_api\UserAuthInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

/**
 * Guzzle middleware for the cambridge core api.
 */
class ApiClientMiddleware {

  /**
   * The api config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a ApiClientMiddleware.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('cambridge_core_api.settings');
  }

  /**
   * Invoked method that returns a promise.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        $uri = $request->getUri();

        // API requests to our test endpoint.
        $endpoint = $this->config->get('uri') . UserAuthInterface::ENDPOINT;
        if ($uri->getScheme() . '://' . $uri->getHost() . $uri->getPath() === $endpoint) {
          return $this->createPromise($request);
        }

        // Otherwise, no intervention. We defer to the handler stack.
        return $handler($request, $options);
      };
    };
  }

  /**
   * Creates a promise for the OMDb request.
   *
   * @param RequestInterface $request
   *
   * @return \GuzzleHttp\Promise\PromiseInterface
   */
  protected function createPromise(RequestInterface $request) {
    $params = Json::decode($request->getBody());
    $auth_method = $params['authMethod'];

    // Basic auth method.
    if ($auth_method === UserAuthInterface::BASIC_METHOD) {
      $fixture = file_get_contents(__DIR__ . '/../../../fixtures/authentication/basic.json');
    }
    elseif ($auth_method === UserAuthInterface::IP_METHOD) {
      // IP auth method.
      $fixture = file_get_contents(__DIR__ . '/../../../fixtures/authentication/ip.json');
    }
    else {
      // AUTOLOGIN auth method.
      $fixture = file_get_contents(__DIR__ . '/../../../fixtures/authentication/autologin.json');
    }

    $response = new Response(200, [], $fixture);
    return new FulfilledPromise($response);
  }

}
