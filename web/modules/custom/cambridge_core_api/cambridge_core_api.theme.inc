<?php

/**
 * @file
 * Template implementations for the Cambridge core api module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements template_preprocess_HOOK() for cambridge_core_api_organization_image.
 */
function template_preprocess_cambridge_core_api_organization_image(&$variables) {
  if (empty($variables['url'])) {
    $path = '/' . drupal_get_path('module', 'cambridge_core_api') . '/images/identity.png';
    $variables['url'] = $path;
    $variables['alt'] = new TranslatableMarkup('Cambridge identity logo');
  }
  if (empty($variables['title'])) {
    $variables['title'] = new TranslatableMarkup('Institution:');
  }
}
