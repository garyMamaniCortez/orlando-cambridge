# Cambridge Core API

A module which integrates drupal login authentication with the Cambridge Core API.

## Dependencies

1. Drupal core ^8.8 || ^9
2. [composer require drupal/externalauth](https://drupal.org/project/externalauth)
3. A valid api server uri endpoint, key and product id

## Installation

1. [Enable the module](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules#s-step-2-enable-the-module)
2. Navigate to the module configuration page at `/admin/config/people/cambridge-core-api` and setup the requested information
3. Enjoy

## Issues

For any issues or feature request go to https://gitlab.com/calincs/cwrc/orlando-cambridge/-/issues
