<?php

namespace Drupal\orlando_interface_support\Element;

use Drupal\element_embed\Annotation\EmbeddableRenderElement;
use Drupal\element_embed\ConfigurableEmbeddableRenderElementInterface;

/**
 * @EmbeddableRenderElement(
 *   id = "oi_scholarnote_citation_trigger",
 *   label = @Translation("Scholarnote/Citation Trigger")
 * )
 */
class ScholarNoteReferenceTrigger extends ScholarNoteReferenceTriggerElement implements ConfigurableEmbeddableRenderElementInterface {

  /**
   * {@inheritdoc}
   */
  public static function getForm(array $form, array $configuration) {
    $form['target'] = [
      '#type' => 'select',
      '#title' => t('Target'),
      '#required' => TRUE,
      '#options' => [
        'references' => t('Citation'),
        'scholarnote' => t('Scholar note'),
      ],
      '#default_value' => $configuration['target'] ?? 'references',
    ];
    $form['popper_content_id'] = [
      '#type' => 'textfield',
      '#title' => t('Content ID'),
      '#required' => TRUE,
      '#default_value' => $configuration['popper_content_id'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function getElement(array $configuration) {
    return [
      '#type' => 'oi_scholarnote_citation_trigger',
      '#target' => $configuration['target'] ?? 'references',
      '#popper_content_id' => $configuration['popper_content_id'] ?? '',
    ];
  }

}
