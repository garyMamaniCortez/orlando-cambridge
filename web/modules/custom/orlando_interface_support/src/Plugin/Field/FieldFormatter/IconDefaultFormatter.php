<?php

namespace Drupal\orlando_interface_support\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Template\Attribute;

/**
 * Plugin implementation of the 'ois_svg_icon_default' formatter.
 *
 * @FieldFormatter(
 *   id = "ois_svg_icon_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "ois_svg_icon",
 *   }
 * )
 */
class IconDefaultFormatter extends FormatterBase {

  /**
   * @inheritDoc
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $settings = $item->settings;
      $elements[$delta] = [
        '#theme' => static::getIconThemeName($item->value),
        '#attributes' => new Attribute([
          'class' => [
            'tw-w-14.5',
            'tw-h-14.5',
            'tw-flex',
            'tw-items-center',
            'tw-justify-center',
            'tw-rounded-full',
            'tw-bg-gray-500',
          ],
        ]),
        '#svg_attributes' => new Attribute([
          'fill' => $settings['main_fill'],
          'width' => $settings['width'],
          'height' => $settings['height'],
        ]),
        '#color_primary' => $settings['color_primary'],
        '#color_secondary' => $settings['color_secondary'],
        '#color_stroke' => $settings['color_stroke'],
      ];
    }

    return $elements;
  }

  public static function getIconThemeName(string $identifier) {
    $identifier = strtolower($identifier);
    switch ($identifier) {
      case 'pen_nib':
      case 'author':
      case 'author_profile':
      case 'entry':
        $name = 'pen_nib';
        break;
      case 'bibliography':
      case 'title':
      case 'bibliographies':
        $name = 'bibliography';
        break;
      case 'places':
        $name = 'place';
        break;
      case 'organization':
      case 'organizations':
        $name = 'organisation';
        break;
      case 'person':
        $name = 'person';
        break;
      case 'event':
      case 'standalone_event':
      case 'standalone event':
        $name = 'timeline';
        break;
      default:
        $name = str_replace([' ', '-'], '_', $identifier);
    }
    return 'ois__svg_icon__' . $name;
  }

}
