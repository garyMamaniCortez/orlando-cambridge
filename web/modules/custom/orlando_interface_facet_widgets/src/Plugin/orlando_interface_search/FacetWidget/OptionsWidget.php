<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXFacetWidget;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginBase;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "options",
 *   label = @Translation("Options widget"),
 * )
 */
class OptionsWidget extends BaseXFacetWidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'key_tag_name' => '',
      'label_tag_name' => '',
      'element_type' => 'checkboxes',
      'select_all_label' => '',
      'is_multiple' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['key_tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key tag name'),
      '#description' => $this->t('The name of the tag containing the key value for the options.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['key_tag_name'],
    ];
    $form['label_tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label tag name'),
      '#description' => $this->t('The name of the tag containing the label for the options.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['label_tag_name'],
    ];
    $form['element_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Element type'),
      '#description' => $this->t('The form element type to use.'),
      '#options' => [
        'checkboxes' => $this->t('Checkboxes'),
        'radios' => $this->t('Radios'),
        'select' => $this->t('Select box/dropdown'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_type'],
    ];
    $form['select_all_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select all label'),
      '#default_value' => $this->configuration['select_all_label'],
      '#states' => [
        'visible' => [
          [':input[name="facet_widget_config[element_type]"]' => ['value' => 'checkboxes']],
          [':input[name="facet_widget_config[is_multiple]"]' => ['checked' => TRUE]],
        ],
        'empty' => [
          [':input[name="facet_widget_config[is_multiple]"]' => ['checked' => FALSE]],
        ],
      ],
    ];
    $form['is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can the user select multiple value?'),
      '#deafult_value' => !empty($this->configuration['is_multiple']),
      '#states' => [
        'checked' => [':input[name="facet_widget_config[element_type]"]' => ['value' => 'checkboxes']],
        'unchecked' => [':input[name="facet_widget_config[element_type]"]' => ['value' => 'radios']],
        'disabled' => [':input[name="facet_widget_config[element_type]"]' => [
          ['value' => 'checkboxes'],
          ['value' => 'radios'],
        ]],
      ],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array $form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $element = parent::formElement($form, $form_state);
    $element['value']['#type'] = $this->configuration['element_type'];
    $all_options = $this->getOptions();
    if ($results) {
      $options = [];
      foreach ($results as $facet_result) {
        $name = $facet_result->getName();
        /** @var \Drupal\orlando_interface_search\Item\Facet $facet_result */
        $options[$name] = $this->t('@name (@count)', [
          '@name' => $all_options[$name],
          '@count' => $facet_result->getCount(),
        ]);
      }
    }
    else {
      $options = $all_options;
    }

    $element['value']['#options'] = $options;

    // Set default value.
    $facet_mapping_id = $this->facetMapping->id();
    $selected_values = $form['#query_parameters'][$facet_mapping_id] ?? [];
    if ($selected_values && $options && $this->validQueryParameters($selected_values, array_keys($options))) {
      $element['value']['#default_value'] = $selected_values;
    }
    return $element;
  }

  public function processedMappings() {
    $mappings = $this->facetMapping->getMappings();
    return $mappings['mapping'] ?? [];
  }

  public function getOptions() {
    $key_tag_name = $this->configuration['key_tag_name'];
    $label_tag_name = $this->configuration['label_tag_name'];
    $options = [];
    foreach ($this->processedMappings() as $mapping) {
      if (isset($mapping[$key_tag_name]) && isset($mapping[$label_tag_name])) {
        $options[$mapping[$key_tag_name]] = $mapping[$label_tag_name];
      }
    }
    return $options;
  }

  public function extractSelectedValues($data) {
    if (!$data) {
      return;
    }
    return array_keys(array_filter($data, function($value){
      return !empty($value);
    }));
  }

  public function generateSelectedValuesString($values): string {
    if (!$values || !is_array($values) || !$this->validQueryParameters($values)) {
      return '';
    }

    $arr = array_intersect_key($this->getOptions(), array_flip($values));
    return ucwords(implode(',', array_values($arr)));
  }

  public function buildXQueryString($values = NULL): string {
    $xquery = '';
    if (!$values || !is_array($values) || !$this->validQueryParameters($values)) {
      return $xquery;
    }

    $id = $this->facetMapping->id();
    $xquery = '"' . $id . '":(';
    $xquery .= '"' . implode('","' , $values) . '"';
    $xquery .= ')';
    return $xquery;
  }

  public function validQueryParameters(array $values, array $allowed_values = []): bool {
    $allowed_values = $allowed_values ?: array_keys($this->getOptions());
    return !empty(array_intersect($values, $allowed_values));
  }

}
