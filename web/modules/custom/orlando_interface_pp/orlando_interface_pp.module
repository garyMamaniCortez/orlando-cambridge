<?php

/**
 * @file
 * Orlando interface paragraph parser module file.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_ENTITY_TYPE_delete() for paragraph.
 */
function orlando_interface_pp_paragraph_delete(EntityInterface $entity) {
  if ($entity->bundle() === 'basic') {
    $connection_storage = \Drupal::entityTypeManager()->getStorage('oi_connection');
    $ids = $connection_storage->getQuery()
      ->condition('paragraph.target_id', $entity->id())
      ->accessCheck(FALSE)
      ->execute();
    if ($ids) {
      foreach ($ids as $id) {
        $connection = $connection_storage->load($id);
        $connection->delete();
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for paragraph.
 */
function orlando_interface_pp_preprocess_paragraph(&$variables) {
  /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
  $paragraph = $variables['paragraph'];
  if ($paragraph->bundle() === 'basic') {
    $variables['#attached']['library'][] = 'orlando_interface_pp/placeholder';
  }
}

/**
 * Implements hook_entity_embed_alter().
 */
function orlando_interface_pp_entity_embed_alter(array &$build, EntityInterface $entity, array &$context) {
  $placeholder = $context['data-oi-element-entity-placeholder'] ?? '';
  if ($placeholder && !empty($build['entity'])) {
    // Adding placeholder as a property here so that the theme can use it for
    // display.
    $build['#oi_content_placeholder'] = $placeholder;
    $build['entity']['#oi_content_placeholder'] = $placeholder;
  }
}
