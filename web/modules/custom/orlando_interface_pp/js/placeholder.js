(function() {
  // Paragraph wrapper.
  document.querySelectorAll('.paragraph-wrapper').forEach(function (wrapper) {
    // Removing line breaks.
    wrapper.innerHTML = wrapper.innerHTML.replace(/\n|\r\n/g, '');
  });
  document.querySelectorAll('[data-embed-button="taxonomy"]').forEach(function (embeddedContainerElement, i) {
    // Removing line breaks and white spaces between before and after the tags.
    embeddedContainerElement.innerHTML = embeddedContainerElement.innerHTML.replace(/^\s+|\s+$/g, '');
    // Applying the same cleaning to the children elements as well.
    embeddedContainerElement.querySelectorAll('span, a').forEach(function (elt) {
      elt.innerHTML = elt.innerHTML.replace(/^\s+|\s+$/g, '');
    });
  });
})();
