<?php

namespace Drupal\orlando_interface_pp;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\dynamic_entity_reference\Plugin\Field\FieldType\DynamicEntityReferenceItem;
use Drupal\entity\EntityViewsData;

/**
 * Provides views integration for the 'oi_connection' custom entity.
 */
class ConnectionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Adding relationships for source and subject fields.
    $dynamic_entity_ref_fields = [];
    $entity_type_id = $this->entityType->id();
    $base_fields = $this->getEntityFieldManager()->getBaseFieldDefinitions($entity_type_id);
    foreach ($base_fields as $base_field) {
      if ($base_field->getType() == 'dynamic_entity_reference' && !$base_field->isComputed()) {
        $dynamic_entity_ref_fields[] = $base_field;
      }
    }

    $entity_type_manager = $this->getEntityTypeManager();
    $table_mapping = $entity_type_manager->getStorage($entity_type_id)->getTableMapping();
    $base_table = $this->entityType->getBaseTable();
    $sql_entity_types = $this->getSqlEntityTypes();
    $entity_type_label = $this->entityType->getLabel();
    foreach ($dynamic_entity_ref_fields as $field) {
      $field_name = $field->getName();
      $columns = $table_mapping->getColumnNames($field_name);
      $column_id = $columns['target_id'];
      $column_type = $columns['target_type'];

      // Unlimited (-1) or > 1 store field data in a dedicated table.
      $table = $field->getCardinality() == 1 ? $base_table : $base_table . '__' . $field_name;
      // Filter out non SQL entity types because \Drupal\views\EntityViewsData
      // class only allows entities with
      // \Drupal\Core\Entity\Sql\SqlEntityStorageInterface.
      $targets = array_intersect(DynamicEntityReferenceItem::getTargetTypes($field->getSettings()), array_keys($sql_entity_types));
      foreach ($targets as $target_entity_type_id) {
        if ($entity_type_manager->hasHandler($target_entity_type_id, 'views_data')) {
          $target_entity_type = $entity_type_manager->getDefinition($target_entity_type_id);
          $target_table = $entity_type_manager->getHandler($target_entity_type_id, 'views_data')
            ->getViewsTableForEntityType($target_entity_type);

          $t_args = [
            '@origin_label' => $entity_type_label,
            '@target_label' => $target_entity_type->getLabel(),
            '@field_name' => $field->getLabel() ?: $field_name,
            '@type' => 'base field',
          ];

          // Relationship (Origin -> Target).
          $pseudo_field = $target_entity_type_id . '__' . $field_name;
          if (isset($data[$table][$pseudo_field]['relationship'])) {
            continue;
          }

          $data[$table][$pseudo_field]['relationship'] = [
            'title' => t('@field_name to @target_label entities', $t_args),
            'label' => t('@field_name: @target_label', $t_args),
            'group' => $entity_type_label,
            'help' => t('References to @target_label entities referenced by @field_name @type on @origin_label entities.', $t_args),
            'id' => 'standard',
            'base' => $target_table,
            'entity type' => $target_entity_type_id,
            'base field' => $target_entity_type->getKey('id'),
            'relationship field' => $column_id,
            'extra' => [
              [
                'left_field' => $column_type,
                'value' => $target_entity_type_id,
              ],
            ],
          ];

          // Reverse Relationship (Target -> Origin).
          $pseudo_field = 'reverse__' . $entity_type_id . '__' . $field_name;
          $data[$target_table][$pseudo_field]['relationship'] = [
            'title' => t('Reverse reference to @field_name @type on @origin_label', $t_args),
            'label' => t('Reverse reference to @field_name @type on @origin_label', $t_args),
            'group' => $target_entity_type->getLabel(),
            'help' => t('Reverse reference from @target_label entities referenced by @field_name @type on @origin_label entities.', $t_args),
          ];
          if ($field->getCardinality() == 1) {
            $data[$target_table][$pseudo_field]['relationship'] += [
              'id' => 'standard',
              'base' => $table,
              'entity type' => $entity_type_id,
              'base field' => $column_id,
              'field' => $target_entity_type->getKey('id'),
              'extra' => [
                [
                  'field' => $column_type,
                  'value' => $target_entity_type_id,
                ],
              ],
            ];
          }
          else {
            $data[$target_table][$pseudo_field]['relationship'] += [
              'id' => 'entity_reverse',
              'base' => $base_table,
              'entity_type' => $entity_type_id,
              'base field' => $this->entityType->getKey('id'),
              'field_name' => $field_name,
              'field table' => $table,
              'field field' => $column_id,
              'join_extra' => [
                [
                  'field' => $column_type,
                  'value' => $target_entity_type_id,
                ],
              ],
            ];
          }
        }
      }
    }

    return $data;
  }

  private function getSqlEntityTypes() {
    $sql_entity_types = [];
    $entity_type_manager = $this->getEntityTypeManager();
    // Ensure origin and target entity types are SQL.
    foreach ($entity_type_manager->getDefinitions() as $entity_type) {
      // \Drupal\views\EntityViewsData class only allows entities with
      // \Drupal\Core\Entity\Sql\SqlEntityStorageInterface.
      if ($entity_type->hasHandlerClass('views_data') && $entity_type_manager->getStorage($entity_type->id()) instanceof SqlEntityStorageInterface) {
        $sql_entity_types[$entity_type->id()] = $entity_type->id();
      }
    }
    return $sql_entity_types;
  }

}
