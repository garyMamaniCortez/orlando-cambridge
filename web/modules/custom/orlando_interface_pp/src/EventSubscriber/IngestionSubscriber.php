<?php

namespace Drupal\orlando_interface_pp\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_ingestion\Commands\IngestItemsCommands;
use Drupal\orlando_interface_ingestion\Event\BasicBodySetEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\TypedRepositories\BibciteReferenceRepository;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\typed_entity\RepositoryManager;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IngestionSubscriber implements EventSubscriberInterface {

  protected $createdConnectionIds = [];

  protected $entityTypeManager;

  protected $repositoryManager;

  protected $termRepo;

  protected $bibciteReferenceRepo;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, RepositoryManager $repository_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->repositoryManager = $repository_manager;
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    return [
      IngestionEvents::BEFORE_BODY_SET => ['parseParagraphBody'],
    ];
  }

  public function parseParagraphBody(BasicBodySetEvent $event) {
    $body = $event->getBody();
    $paragraph = $event->getParagraph();
    $module = 'orlando_interface_pp';
    $body = str_replace('&', '&amp;', $body);
    \Drupal::logger($module)->info('The following paragraph is being parsed: @id - @paragraph', [
      '@id' => $paragraph->get('field_id')->value,
      '@paragraph' => $body,
    ]);
    $doc = new \DOMDocument();
    $doc->loadXML('<BODY>' . $body . '</BODY>');
    $crawler = new Crawler($doc);

    // Embedded entities tags.
    $this->processEmbeddedEntities($crawler, $doc, $event->getSource(), $paragraph);
    // Card display tags.
    $this->processCardTags($crawler, $doc, $paragraph->id());

    $html = str_replace('&amp;', '&', $crawler->html());
    $event->setBody(trim(preg_replace('/\s+/', ' ', $html)));
    $event->setExtractedConnections($this->createdConnectionIds);
    $this->createdConnectionIds = [];
  }

  private function processEmbeddedEntities(Crawler $crawler, \DOMDocument $document, EntityInterface $source, ParagraphInterface $paragraph) {
    $custom_tags = [
      'orlando_person',
      'orlando_organization',
      'orlando_citation',
      'orlando_textscope',
    ];
    foreach ($custom_tags as $custom_tag) {
      $crawler->filter($custom_tag)
        ->each(function(Crawler $tag) use ($document, $source, $paragraph) {
          $node_name = $tag->nodeName();
          $subject_uri = $tag->attr('id');
          $source_author_profile = $tag->attr('parent_profile');
          $context = $tag->attr('context');
          $display = $tag->attr('display');
          if ($context && $subject_uri && $source_author_profile && $display) {
            $values = [
              'source' => $source,
              'paragraph' => $paragraph,
              'subject_type' => str_replace('orlando_', '', $node_name),
              'subject_uri' => $subject_uri,
              'context' => $context,
              'display' => $display,
            ];
            $style = $tag->attr('style');
            $attributes = $this->assignConnection($values);
            foreach ($tag as $node) {
              $text_content = trim($node->textContent);
              $element = $document->createElement('drupal-entity', $text_content);
              $element->setAttribute('data-oi-element-entity-placeholder', $text_content);
              foreach ($attributes as $qualified_name => $value) {
                $element->setAttribute($qualified_name, $value);
              }
              if ($style) {
                $element->setAttribute('data-oi-element-style', $style);
              }
              $node->parentNode->replaceChild($element, $node);
            }
          }
        });
    }
  }

  private function processInlineTags(Crawler $crawler, \DOMDocument $document) {
    $custom_tags = [
      'orlando_socalled',
      'orlando_quote',
      'orlando_topic',
      'orlando_title',
      'orlando_place',
    ];
    foreach ($custom_tags as $custom_tag) {
      $crawler->filter($custom_tag)
        ->each(function(Crawler $tag) use ($document) {
          $style = $tag->attr('style');
          $node_name = $tag->nodeName();
          foreach ($tag as $node) {
            $element = $document->createElement('span', trim($node->textContent));
            $element->setAttribute('data-oi-element-node-name', $node_name);
            $element->setAttribute('data-oi-element-display', 'inline');
            if ($style) {
              $element->setAttribute('data-oi-element-style', $style);
            }
            $node->parentNode->replaceChild($element, $node);
          }
        });
    }
  }

  private function processCardTags(Crawler $crawler, \DOMDocument $document, $paragraph_id) {
    $custom_tags = [
      'orlando_references',
      'orlando_scholarnote',
    ];
    foreach ($custom_tags as $custom_tag) {
      $crawler->filter($custom_tag)
        ->each(function(Crawler $tag, $index) use ($document, $paragraph_id) {
          $node_name = $tag->nodeName();
          $target = str_replace('orlando_', '', $node_name);
          $id_prefix = 'oi-paragraph-' . $paragraph_id . '--card-' . $node_name . '-' . $index;
          foreach ($tag as $i => $node) {
            $id = Html::cleanCssIdentifier($id_prefix . '--' . $i);
            $outer_content = $document->createElement('div');
            $outer_content->setAttribute('data-popper-content', 'true');
            $outer_content->setAttribute('class', 'tw-hidden');
            $outer_content->setAttribute('id', $id);

            $inner_content = $document->createElement('div');
            $inner_content->setAttribute('data-popper-inner-content', 'true');
            // Extracting the children node elements.
            $children = [];
            for ($position = 0; $position < $node->childNodes->length; $position++) {
              $children[] = $node->childNodes->item($position);
            }
            // Appending the element in the inner wrapper.
            foreach ($children as $child) {
              $inner_content->appendChild($child);
            }
            $outer_content->appendChild($inner_content);

            $trigger = $document->createElement('drupal-element');
            $trigger->setAttribute('data-element-type', 'oi_scholarnote_citation_trigger');
            $trigger->setAttribute('data-embed-button', 'popup_trigger');
            $trigger->setAttribute('data-entity-label', 'popup trigger');
            $settings = [
              'target' => $target,
              'popper_content_id' => $id,
            ];
            $trigger->setAttribute('data-element-settings', json_encode($settings));

            $arrow = $document->createElement('span');
            $arrow->setAttribute('id', $id . '--arrow');
            $arrow->setAttribute('data-popper-arrow', 'true');
            $arrow->setAttribute('aria-hidden', 'true');
            $outer_content->appendChild($arrow);

            $wrapper = $document->createElement('div');
            $wrapper->appendChild($trigger);
            $wrapper->appendChild($outer_content);
            $wrapper->setAttribute('id', $id . '--wrapper');
            $wrapper->setAttribute('data-oi-element-node-name', $node_name);
            $wrapper->setAttribute('data-oi-element-display', 'card');
            $node->parentNode->replaceChild($wrapper, $node);
          }
        });
    }
  }

  private function assignConnection(array $attributes) {
    $embed_entity_attributes = [];
    /** @var \Drupal\Core\Entity\EntityInterface $source */
    $source = $attributes['source'];
    $paragraph = $attributes['paragraph'];
    $context = $attributes['context'];
    $subject_uri = $attributes['subject_uri'];
    $subject_type = $attributes['subject_type'] === 'citation' ? 'bibcite_reference' : $attributes['subject_type'];

    $ext_uri_prefixes = [
      'https://commons.cwrc.ca/',
      'https://dev-02.cwrc.ca/islandora/object/',
    ];
    $subject_id = str_replace($ext_uri_prefixes, '', $subject_uri);

    // Get subject.
    $subject = NULL;
    $embed_button = '';
    if ($subject_type === 'person' || $subject_type === 'organization') {
      $term = $this->getTermRepo($subject_type)->findByCWRCId($subject_id);
      if ($term) {
        $subject = $term->getEntity();
        $embed_button = 'taxonomy';
      }
    }
    else {
      $connections_dir = IngestItemsCommands::FILES_ENTITIES_DIR . '/bibls';
      $filename_id = str_replace(':', IngestItemsCommands::FILENAME_ID_SEPARATOR, $subject_id);
      $filename = "$connections_dir/$filename_id.xml";
      $bundle = BibciteReferenceRepository::extractTypeFromXml($filename);
      $attributes['display'] = 'citation';
      if ($bundle) {
        $reference = $this->getBibciteRepo($bundle)->findByCWRCId($subject_id);
        if ($reference) {
          $subject = $reference->getEntity();
          $embed_button = 'bibcite_reference';
        }
      }
    }

    // Create connection.
    if ($subject) {
      $connection_storage = $this->entityTypeManager->getStorage('oi_connection');
      $source_id = $source->id();
      $source_entity_type_id = $source->getEntityTypeId();
      $subject_id = $subject->id();
      $subject_entity_type_id = $subject->getEntityTypeId();
      $paragraph_id = $paragraph->id();
      $ids = $connection_storage->getQuery()
        ->condition('source.target_id', $source_id)
        ->condition('source.target_type', $source_entity_type_id)
        ->condition('subject.target_id', $subject_id)
        ->condition('subject.target_type', $subject_entity_type_id)
        ->condition('paragraph.target_id', $paragraph_id)
        ->accessCheck(FALSE)
        ->range(0, 1)
        ->execute();
      if (!$ids) {
        $connection = $connection_storage->create([
          'type' => 'default',
          'source' => [
            'target_id' => $source_id,
            'target_type' => $source_entity_type_id,
          ],
          'subject' => [
            'target_id' => $subject_id,
            'target_type' => $subject_entity_type_id,
          ],
          'context' => $context,
          'paragraph' => [
            'target_id' => $paragraph_id,
          ],
          'paragraph_ext_id' => $paragraph->get('field_id')->value,
        ]);
        $connection->save();

        // Populate the connection id to be added to the paragraph entity where
        // this connection originated from.
        $this->createdConnectionIds[] = $connection->id();
      }

      $embed_entity_attributes = [
        'data-embed-button' => $embed_button,
        'data-entity-embed-display' => "view_mode:$subject_entity_type_id.${attributes['display']}",
        'data-entity-type' => $subject_entity_type_id,
        'data-entity-uuid' => $subject->uuid(),
        'data-langcode' => 'en',
      ];
    }

    return $embed_entity_attributes;
  }

  /**
   * Gets the person term repository.
   *
   * @return \Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository
   *   The term repository.
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   */
  private function getTermRepo(string $vid) {
    if (!isset($this->personRepo[$vid])) {
      $this->termRepo[$vid] = $this->repositoryManager->repository('taxonomy_term', $vid);
    }

    return $this->termRepo[$vid];
  }

  /**
   * Gets the organization term repository.
   *
   * @return \Drupal\orlando_interface_ingestion\TypedRepositories\BibciteReferenceRepository
   *   The term repository.
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   */
  private function getBibciteRepo(string $bundle) {
    if (!isset($this->bibciteReferenceRepo[$bundle])) {
      $this->bibciteReferenceRepo[$bundle] = $this->repositoryManager->repository('bibcite_reference', $bundle);
    }
    return $this->bibciteReferenceRepo[$bundle];
  }

  private function appendHTML(DOMNode $parent, $source) {
    $tmpDoc = new DOMDocument();
    $tmpDoc->loadHTML($source);
    foreach ($tmpDoc->getElementsByTagName('body')->item(0)->childNodes as $node) {
      $node = $parent->ownerDocument->importNode($node, true);
      $parent->appendChild($node);
    }
  }

}
