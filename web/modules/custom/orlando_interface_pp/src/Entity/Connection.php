<?php

namespace Drupal\orlando_interface_pp\Entity;

use Drupal\Core\Annotation\PluralTranslation;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Defines the connection entity.
 *
 * @ContentEntityType(
 *   id = "oi_connection",
 *   label = @Translation("Connection"),
 *   label_collection = @Translation("Connections"),
 *   label_singular = @Translation("Connection"),
 *   label_plural = @Translation("Connections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count connection",
 *     plural = "@count connections",
 *   ),
 *   bundle_label = @Translation("Connections type"),
 *   handlers = {
 *     "storage" = "Drupal\orlando_interface_pp\ConnectionStorage",
 *     "storage_schema" = "Drupal\orlando_interface_pp\ConnectionStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\orlando_interface_pp\ConnectionAccessControlHandler",
 *     "views_data" = "Drupal\orlando_interface_pp\ConnectionViewsData",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *     }
 *   },
 *   base_table = "oi_connections",
 *   data_table = "oi_connections_field_data",
 *   translatable = FALSE,
 *   revisionable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *   },
 *   bundle_entity_type = "oi_connections_type",
 *   field_ui_base_route = "entity.oi_connections_type.edit_form",
 *   render_cache = FALSE,
 * )
 *
 */
class Connection extends ContentEntityBase implements ConnectionInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function setSource(ContentEntityInterface $source) {
    $this->set('source', [
      'target_type' => $source->getEntityTypeId(),
      'target_id' => $source->id(),
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSource(): ContentEntityInterface {
    return $this->get('source')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setTarget(ContentEntityInterface $subject) {
    $this->set('subject', [
      'target_type' => $subject->getEntityTypeId(),
      'target_id' => $subject->id(),
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget(): ContentEntityInterface {
    return $this->get('subject')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setParagraph(ParagraphInterface $paragraph) {
    $this->set('paragraph', [
      'target_id' => $paragraph->id(),
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraph(): ParagraphInterface {
    return $this->get('paragraph')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(string $context) {
    // TODO: Validate context string maybe...
    $this->set('context', $context);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    return $this->get('context')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParagraphExternalId(string $id) {
    $this->set('paragraph_ext_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphExternalId(): string {
    return $this->get('paragraph_ext_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('id')]->setLabel(t('Connection ID'));
    $fields[$entity_type->getKey('uuid')]->setLabel(t('Connection UUID'));
    $fields[$entity_type->getKey('bundle')]->setLabel(t('Connection Type'));

    // <tag parent_profile="" should be an author profile but storing the person
    $fields['source'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Source'))
      ->setDescription(t('The source entity this connection is happening in.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      // Currently only supporting taxonomy terms for person and the standalone
      // event through the profile_event entity.
      ->setSetting('target_type', ['taxonomy_term', 'profile_event'])
      ->setDisplayOptions('form', [
        'type' => 'dynamic_entity_reference_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // Avoided to use "target" as machine name here so that I don't confuse it
    // with entity reference target id or type internal names.
    // <tag id="". A URI to person, organization and bibliography
    $fields['subject'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Target'))
      ->setDescription(t('The target entity this connection is related to.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      // Currently only supporting taxonomy terms for person or organization.
      // But later we could support the source to be an event or bibliography.
      ->setSetting('target_type', ['taxonomy_term', 'bibcite_reference'])
      ->setDisplayOptions('form', [
        'type' => 'dynamic_entity_reference_default',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // <tag context
    $fields['context'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Connection context'))
      ->setDescription(t('The context in which this connection was mentioned in.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('allowed_values', static::getContextMapping())
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['paragraph'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Paragraph'))
      ->setDescription(t('The paragraph in which this connection was mentioned in.'))
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['paragraph_ext_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Paragraph External ID'))
      ->setDescription(t('The External ID of the paragraph. USed to retrieve the drupal internal paragraph.'))
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', ['region' => 'hidden'])
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  public static function getContextMapping() {
    $mappings = [
      'profile_summary' => 'Author summary',
      'birth' => 'Birth',
      'cultural_formation' => 'Cultural formation',
      'instructor' => 'Instructor',
      'education' => 'Education',
      'family_and_intimate_relationships' => 'Family and Intimate relationships',
      'friends_and_associates' => 'Friends, Associates',
      'health' => 'Health',
      'leisure_and_society' => 'Leisure and Society',
      'employer' => 'Employer',
      'occupation' => 'Occupation',
      'other_life_event' => 'Other Life Event',
      'residence' => 'Residence',
      'travel' => 'Travel',
      'violence' => 'Violence',
      'wealth_and_poverty' => 'Wealth and Poverty',
      'intertextuality_and_influence' => 'Intertextuality and Influence',
      'characters' => 'Characters',
      'literary_setting' => 'Literary Setting',
      'theme_or_topic_treated_in_text' => 'Theme or Topic Treated in Text',
      'textual_features' => 'Textual Features',
      'fictionalization' => 'Fictionalization',
      'literary_responses' => 'Literary responses',
      'reception' => 'Reception',
      'anthologization' => 'Anthologization',
      'dedications' => 'Dedications',
      'material_conditions_of_writing' => 'Material Conditions of Writing',
      'performance_of_text' => 'Performance of text',
      'publishing' => 'Publishing',
      'subscriptions' => 'Subscriptions',
      'textual_production' => 'Textual Production',
      'textscope' => 'Text scope',
      'standalone_event' => 'Standalone event',
    ];
    asort($mappings);
    return $mappings;
  }

}
