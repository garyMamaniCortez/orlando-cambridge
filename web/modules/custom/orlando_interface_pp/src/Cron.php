<?php


namespace Drupal\orlando_interface_pp;

use Drupal\advancedqueue\Job;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default cron implementation.
 */
class Cron {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  public function run() {
    $paragraph_storage = $this->entityTypeManager->getStorage('paragraph');
    $paragraph_ids = $paragraph_storage->getQuery()
      ->condition('type', 'basic')
      ->exists('parent_id')
      ->exists('parent_type')
      ->notExists('field_connections')
      ->execute();

    if (!$paragraph_ids) {
      return;
    }

    $queue_storage = $this->entityTypeManager->getStorage('advancedqueue_queue');
    /** @var \Drupal\advancedqueue\Entity\QueueInterface $connections_queue */
    $connections_queue = $queue_storage->load('oi_connections_queue');
    /** @var \Drupal\paragraphs\ParagraphInterface[] $paragraphs */
    $paragraphs = $paragraph_storage->loadMultiple($paragraph_ids);

    foreach ($paragraphs as $paragraph) {
      if ($paragraph->getParentEntity()) {
        $processor_job = Job::create('orlando_interface_pp_connections_processor', [
          'paragraph_id' => $paragraph->id(),
        ]);
        $connections_queue->enqueueJob($processor_job);
      }
    }
  }

}
