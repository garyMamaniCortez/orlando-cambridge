<?php

namespace Drupal\orlando_interface_ingestion\Event;

use Symfony\Contracts\EventDispatcher\Event;

class IngestionStartEvent extends Event {

  protected $items;

  protected $type;

  public function __construct(array $items, string $type) {
    $this->items = $items;
    $this->type = $type;
  }

  /**
   * @return array
   */
  public function getItems(): array {
    return $this->items;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

}
