<?php

namespace Drupal\orlando_interface_ingestion\Event;

final class IngestionEvents {

  /**
   * The name of the event triggered when a connection to an item is being
   * ingested.
   *
   * @Event
   *
   * @var string
   */
  const CONNECTION_INGESTION = 'oi_connection.ingest';

  /**
   * The name of the event triggered when an item is being ingested.
   *
   * @Event
   *
   * @var string
   */
  const ITEM_INGESTION = 'oi_item.ingest';

  /**
   * The name of the event triggered when an item is being deleted.
   *
   * @Event
   *
   * @var string
   */
  const ITEM_DELETE = 'oi_item.delete';

  const INGESTION_FINISHED = 'oi_items.ingested';

  /**
   * The name of the event triggered when items deletion is finished.
   *
   * @Event
   *
   * @var string
   */
  const DELETION_FINISHED = 'oi_items.deleted';

  const INGESTION_STARTED = 'oi_items.started';

  const DELETION_STARTED = 'oi_items.deletion_started';

  const BEFORE_BODY_SET = 'oi_before_basic_paragraph_body_set';

}
