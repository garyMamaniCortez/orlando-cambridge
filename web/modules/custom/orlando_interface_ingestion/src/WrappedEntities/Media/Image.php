<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Media;

use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\TypedRepositories\MediaRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

final class Image extends WrappedEntity {

  protected $title;

  protected $mod;

  public function setImageFile(string $filename): Image {
    $info = pathinfo($filename);
    $file = $this->createFileEntity($filename);
    $mod_xml = str_replace($info['extension'], 'xml', $filename);
    $this->mod = MediaRepository::transformXmlToArray($mod_xml);
    $values = [
      'target_id' => $file->id(),
      'alt' => substr(Xss::filter(strip_tags($this->mod['abstract'])), 0, 512),
      'title' => substr(Xss::filter(strip_tags($this->mod['titleInfo']['title'])), 0, 1024),
    ];

    parent::setFieldValue('field_media_image', $values);
    // Making sure that we removed the path scheme wrapper.
    parent::setFieldValue('field_path', str_replace('private://', '', $info['dirname']));
    $this->setImageFileModsFile($mod_xml);
    return $this;
  }

  public function setImageFileMods(array $data) {
    return $this->setFieldValue('field_image_mods', $data);
  }

  public function setNameFromXml(string $filename): Image {
    return parent::setFieldValue('name', $this->getImageTitleFromXml($filename));
  }

  private function setImageFileModsFile(string $mod_uri) {
    $mod = $this->createFileEntity($mod_uri);
    return $this->setImageFileMods(['target_id' => $mod->id()]);
  }

  private function getImageTitleFromXml(string $filename): string {
    if (empty($this->title)) {
      if (empty($this->mod)) {
        $this->mod = MediaRepository::transformXmlToArray($filename);
      }
      $this->title = Xss::filter(strip_tags($this->mod['titleInfo']['title']));
    }
    return $this->title;
  }

}
