<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\node\NodeInterface;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

final class Event extends WrappedEntity {

  const YEAR_ONLY_FORMAT = 'Y';
  const YEAR_MONTH_FORMAT = 'Y-m';

  const ITEMS_DIR = 'private://orlando-2-0-c-modelling/entities/events';

  /**
   * The paragraph repository.
   *
   * @var \Drupal\orlando_interface_ingestion\TypedRepositories\ParagraphRepository
   */
  protected $paragraphRepository;

  public function getExternalId(): string {
    return $this->getFieldValue('cwrc_id')->value;
  }

  public function setExternalId(string $id, $is_embedded = FALSE): Event {
    if ($is_embedded) {
      $id = $this->getRepository()->getStringHash($id);;
    }
    return $this->setFieldValue('cwrc_id', $id);
  }

  public function setStatus(string $status): Event {
    return $this->setFieldValue('status', $status);
  }

  public function isMilestone(): bool {
    return (bool) $this->getFieldValue('is_milestone')->value;
  }

  public function isEmbedded(): bool {
    return $this->getEntity()->isEmbedded();
  }

  public function setMilestone(bool $is_milestone): Event {
    return $this->setFieldValue('is_milestone', $is_milestone);
  }

  public function setDate(string $date): Event {
    $format = $this->recoupFormatFromDate($date);
    $this->setFieldValue('date_format', $format);
    return $this->setFieldValue('date_value', $this->extractDateParts($date));
  }

  public function setEndDate(string $date): Event {
    $format = $this->recoupFormatFromDate($date);
    $this->setFieldValue('date_end_format', $format);
    return $this->setFieldValue('date_end_value', $this->extractDateParts($date));
  }

  public function getDateLabel(): string {
    return $this->entity->getDateLabel();
  }

  public function setDateLabel(string $label): Event {
    $label = substr(Xss::filter(strip_tags($label)), 0, 255);
    return $this->setFieldValue('date_label', $label);
  }

  public function setRelevance(string $relevance): Event {
    return $this->setFieldValue('relevance', $relevance);
  }

  public function setType(string $type): Event {
    return $this->setFieldValue('type', $type);
  }

  public function getProfile(): ?NodeInterface {
    $field = $this->getFieldValue('profile');
    if ($field->isEmpty()) {
      return NULL;
    }
    return $field->entity;
  }

  public function setProfile(int $nid): Event {
    $value = ['target_id' => $nid];
    return $this->setFieldValue('profile', $value);
  }

  public function getContent() {
    return $this->getFieldValue('field_content');
  }

  public function setDataFromObject(\stdClass $event): Event {
    $is_embedded = $event->status === 'embedded';
    if ($is_embedded) {
      $external_id = $event->content->chronprose_content;
    }
    else {
      $external_id = $event->id;
    }
    $this->setExternalId($external_id, $is_embedded);
    $this->setDate($event->time->value);
    $this->setTypeAndRelevance($event);
    $paragraphs = $this->getFieldValue('field_content');

    if (!empty($event->bibcits) && is_string($event->bibcits)) {
      $event->content->chronprose_content .= $event->bibcits;
    }
    $content = $this->addToBodyParagraphs($event->content, 'chronprose_id', $paragraphs);
    if (!empty($event->time->label)) {
      $this->setDateLabel($event->time->label);
    }
    if (!empty($event->time->end_value)) {
      $this->setEndDate($event->time->end_value);
    }
    if (!empty($event->prose)) {
      $content = $this->addProseToContent($event->prose, $content);
    }
    if (!empty($event->scholarnotes)) {
      $scholarnotes = new \stdClass();
      $scholarnotes->content = $event->scholarnotes;
      $scholarnotes->id = $this->getRepository()->getStringHash($event->scholarnotes);
      $content = $this->addToBodyParagraphs($scholarnotes, 'id', $content);
    }
    if (!empty($event->citations)) {
      $citations = new \stdClass();
      $citations->content = $event->citations;
      $citations->id = $this->getRepository()->getStringHash($event->citations);
      $content = $this->addToBodyParagraphs($citations, 'id', $content);
    }
    $this->getEntity()->field_content = $content;
    return $this;
  }

  public function setParagraphsParent() {
    if ($this->isEmbedded() && $profile = $this->getProfile()) {
      $source = $profile->get('field_person')->entity;
    }
    else {
      $source = $this->getEntity();
    }
    $this->setParagraphItemsParent($this->getContent(), $source, 'field_content');
  }

  private function addToBodyParagraphs(\stdClass $content, string $id_property, FieldItemListInterface $paragraphs) {
    $paragraph = $this->paragraphRepository()->createWrappedEntityFromObject($content, $id_property);
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph_entity */
    $paragraph_entity = $paragraph->getEntity();
    $paragraphs->appendItem([
      'target_id' => $paragraph_entity->id(),
      'target_revision_id' => $paragraph_entity->getRevisionId(),
    ]);
    return $paragraphs;
  }

  private function extractDateParts(string $date): string {
    [$year, $month, $day] = array_pad(explode('-', $date, 3), 3, '01');
    return "$year-$month-$day";
  }

  private function recoupFormatFromDate(string $date): string {
    $date_parts = explode('-', $date);
    $count = count($date_parts);
    if ($count === 1) {
      return static::YEAR_ONLY_FORMAT;
    }
    elseif ($count === 2) {
      return static::YEAR_MONTH_FORMAT;
    }
    else {
      return static::SCHEMA_DATE_FORMAT;
    }
  }

  private function setTypeAndRelevance(\stdClass $event): Event {
    foreach (['type', 'relevance'] as $property) {
      if (isset($event->{$property}) && $this->propertyIsValid($event, $property)) {
        if ($property === 'type') {
          $this->setType($event->{$property});
        }
        else {
          $this->setRelevance($event->{$property});
        }
      }
    }
    return $this;
  }

  private function propertyIsValid(\stdClass $event, string $property): bool {
    $is_valid = !empty($event->{$property});
    if ($is_valid && $property === 'relevance') {
      $relevance = [
        'SELECTIVE',
        'PERIOD',
        'DECADE',
        'COMPREHENSIVE',
      ];
      $is_valid = in_array($event->{$property}, $relevance);
    }
    elseif ($is_valid && $property === 'type') {
      $types = [
        'WRITINGCLIMATE',
        'NATIONALINTERNATIONAL',
        'SOCIALCLIMATE',
        'BRITISHWOMENWRITERS',
      ];
      $is_valid = in_array($event->{$property}, $types);
    }

    return $is_valid;
  }

  /**
   * Instantiate the paragraph repository.
   *
   * @return \Drupal\orlando_interface_ingestion\TypedRepositories\ParagraphRepository
   *   The paragraph repository.
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   */
  private function paragraphRepository() {
    if (empty($this->paragraphRepository)) {
      $this->paragraphRepository = $this->repositoryManager()->repository('paragraph', 'basic');
    }
    return $this->paragraphRepository;
  }

  private function addProseToContent(array $prose_items, FieldItemListInterface $paragraphs) {
    foreach ($prose_items as $item) {
      if (isset($item->paragraph->content)) {
        if (empty($item->paragraph->id)) {
          $item->paragraph->id = $this->getRepository()->getStringHash($item->paragraph->content);
        }
        $paragraphs = $this->addToBodyParagraphs($item->paragraph, 'id', $paragraphs);
      }
    }
    return $paragraphs;
  }

}
