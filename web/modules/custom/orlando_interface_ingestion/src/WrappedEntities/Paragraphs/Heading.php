<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

final class Heading extends BaseParagraph {

  public function getLevel(): string {
    return $this->getFieldValue('field_level')->value;
  }

  public function setLevel(string $level): Heading {
    return $this->setFieldValue('field_level', $level);
  }

  public function getText(): string {
    return $this->getFieldValue('field_text')->value;
  }

  public function setText(string $text): Heading {
    return $this->setFieldValue('field_text', $text);
  }

  public function setDataFromObject(\stdClass $object): Heading {
    $this->setLevel($object->level);
    $this->setText($object->content);
    return $this;
  }

}
