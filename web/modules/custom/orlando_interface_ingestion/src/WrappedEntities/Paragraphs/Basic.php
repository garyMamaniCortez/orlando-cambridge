<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

final class Basic extends BaseParagraph {

  public function getBody() {
    return $this->getFieldValue('field_body');
  }

  public function setBody(string $body) {
    $data = [
      'value' => $body,
      'format' => 'full_html',
    ];
    return $this->setFieldValue('field_body', $data);
  }

  public function setDataFromObject(\stdClass $object) {
    $body = '';
    if (isset($object->content) && is_string($object->content)) {
      $body = $object->content;
    }
    elseif (isset($object->chronprose_content) && is_string($object->chronprose_content)) {
      $body = $object->chronprose_content;
    }

    if ($body) {
      $this->setBody('<div class="paragraph-wrapper">' . $body . '</div>');
    }
    return $this;
  }

}
