<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

use Drupal\Core\Entity\ContentEntityInterface;

final class Event extends BaseParagraph {

  public function setEvent(int $id): Event {
    return $this->setFieldValue('field_event', ['target_id' => $id]);
  }

  public function setDataFromObject(\stdClass $object): Event {
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository $event_repository */
    $event_repository = $this->repositoryManager()->repository('profile_event', 'profile_event');
    $event_repository->validateEventObject($object);
    $wrapped_event = $event_repository->findById($object->content->chronprose_content, TRUE);
    if ($wrapped_event) {
      $event_id = $wrapped_event->getEntity()->id();
    }
    else {
      $event_id = $event_repository->createEmbeddedEventFromObject($object, TRUE)
        ->getEntity()
        ->id();
    }
    return $this->setEvent($event_id);
  }

  public function assignProfileToEventEntity(ContentEntityInterface $node) {
    if ($node->bundle() === 'author_profile') {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $event */
      $event = $this->getFieldValue('field_event')->entity;
      $event->set('profile', ['target_id' => $node->id()]);
      $event->save();

      // Wrapped profile event.
      /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event $wrapped_profile_event */
      $wrapped_profile_event = $this->repositoryManager()->wrap($event);
      if ($wrapped_profile_event) {
        $wrapped_profile_event->setParagraphsParent();
      }
    }
  }

}
