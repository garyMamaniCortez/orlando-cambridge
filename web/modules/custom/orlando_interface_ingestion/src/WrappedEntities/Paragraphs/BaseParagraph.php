<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

abstract class BaseParagraph extends WrappedEntity {

  /**
   * Gets the external ID.
   *
   * @return string
   *   The external id.
   */
  public function getExternalId(): string {
    return $this->getFieldValue('field_id')->value;
  }

  /**
   * Sets the external ID.
   *
   * @param string $id
   *   The ID.
   *
   * @return $this
   *   This term.
   */
  public function setExternalId(string $id) {
    return $this->setFieldValue('field_id', $id);
  }

  abstract public function setDataFromObject(\stdClass $object);

}
