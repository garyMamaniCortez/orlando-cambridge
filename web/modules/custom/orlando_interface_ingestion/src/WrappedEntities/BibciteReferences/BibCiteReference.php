<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences;

use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\Commands\IngestItemsCommands;
use Drupal\orlando_interface_ingestion\TypedRepositories\BibciteReferenceRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Person;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;
use Symfony\Component\DomCrawler\Crawler;

class BibCiteReference extends WrappedEntity {

  const ITEMS_DIR = 'private://orlando-2-0-c-modelling/entities/bibls';

  protected $namespaceOptions = ['namespace' => 'http://www.loc.gov/mods/v3'];

  /**
   * @var \SimpleXMLElement
   */
  protected $xml;

  public function getExternalId() {
    return $this->getFieldValue('cwrc_id')->value;
  }

  public function getModFileUri() {
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->getFieldValue('mod_file')->entity;
    return $file ? $file->getFileUri() : '';
  }

  public function getSimpleXMLElement(string $filename = '') {
    if ($this->xml === NULL) {
      $filename = $filename ?: $this->getModFileUri();
      $this->xml = BibciteReferenceRepository::getSimpleXMLElement($filename, $this->namespaceOptions);
    }
    return $this->xml;
  }

  public function setFieldsFromXML(string $filename = '') {
    $xml = $this->getSimpleXMLElement($filename);
    $fields_xpath_mapping = $this->getFieldsXpathMapping();
    foreach ($fields_xpath_mapping as $field_name => $query) {
      $cast_to_string = TRUE;
      $data = $xml->xpath($query['xpath']);
      $value = NULL;
      $is_publisher = !empty($query['is_publisher']);
      $target_attribute = $query['attribute'] ?? '';
      if (!$is_publisher && $target_attribute && isset($data[0][$target_attribute])) {
        $value = $data[0][$query['attribute']];
      }
      elseif ($is_publisher && !empty($data[0])) {
        if (isset($data[0][$target_attribute])) {
          $field_name = 'oi_publisher';
          $value = $this->processPublisher((string) $data[0][$target_attribute]);
          $cast_to_string = FALSE;
        }
        else {
          $value = $data[0][0];
        }
      }
      elseif ($query['is_author']) {
        $cast_to_string = FALSE;
        $value = $this->processAuthors($data);
      }
      elseif (!empty($query['is_title']) && !empty($data[0])) {
        $value = $this->processTitle($data[0]);
      }
      elseif (!empty($query['is_date'])) {
        $value = $this->processExtractedIsoDate($data, $query['attribute_name']);
      }
      elseif (!empty($query['children_tags']) && !empty($data[0])) {
        $value = $this->processResultChildrenTags($data[0], $query['children_tags']);
      }
      elseif (isset($data[0][0])) {
        $value = $data[0][0];
      }
      $value = $cast_to_string && $value ? (string) $value : $value;

      if ($value) {
        $this->setFieldValue($field_name, $value);
      }
    }
    return $this;
  }

  /**
   * Sets data from XML filename.
   *
   * @param string $filename
   *   The file name.
   *
   * @return $this
   *   The wrapped entity.
   */
  public function setDataFromXml(string $filename) {
    $this->setFieldsFromXML($filename);
    return $this;
  }

  /**
   * Processes the result with children tags.
   *
   * @param \SimpleXMLElement $result
   *   The result.
   * @param array $tags
   *   The children tag.
   *
   * @return string
   *   The value.
   */
  private function processResultChildrenTags(\SimpleXMLElement $result, array $tags) {
    $children = (array) $result[0]->children();
    $value = '';
    foreach ($tags as $tag) {
      if (isset($children[$tag])) {
        $value = $value ? $value . ' - ' . $children[$tag] : $children[$tag];
      }
    }
    return $value;
  }

  private function processExtractedIsoDate(array $dates, string $attribute_name) {
    $start_date = '';
    $end_date = '';
    foreach ($dates as $date) {
      $type = isset($date[$attribute_name]) ? (string) $date[$attribute_name] : '';
      if ($type === '' || $type === 'start') {
        $start_date = (string) $date[0];
      }
      else {
        $end_date = (string) $date[0];
      }
    }

    $value = '';
    if ($start_date) {
      $value = $start_date;
    }
    if ($end_date) {
      $value .= '-' . $end_date;
    }
    return $value;
  }

  private function processTitle(\SimpleXMLElement $result) {
    $doc = new \DOMDocument();
    $xml = str_replace(['tei:title', 'tei:orgname'], ['tei_title', 'tei_orgname'], $result->asXML());
    $doc->loadXML($xml);
    $crawler = new Crawler($doc);

    $crawler->filter('extension')
      ->each(function(Crawler $extension) use ($doc) {
        $tag_name = '';
        $tei_title_level = '';
        $extension->filter('tei_title')->each(function(Crawler $title) use (&$tag_name, &$tei_title_level) {
          $tag_name = 'tei_title';
          $tei_title_level = $title->attr('level');
        });
        $extension->filter('tei_orgname')->each(function() use (&$tag_name) {
          $tag_name = 'tei_orgname';
        });
        foreach($extension as $node) {
          /** @var \DOMNode $node */
          $span = $doc->createElement('span', trim($node->textContent));
          $span->setAttribute('data-tei-ns-tag', $tag_name);
          if ($tei_title_level) {
            $span->setAttribute('data-tei-title-lvl', $tei_title_level);
          }
          $node->parentNode->replaceChild($span, $node);
        }
      });
    return trim(preg_replace('/\s+/', ' ', $crawler->html()));
  }

  private function processPublisher(string $uri) {
    $wrapped_term = $this->getWrappedTermFromUri($uri);
    $value = [];
    if ($wrapped_term) {
      $this->setFieldValue('bibcite_publisher', $wrapped_term->label());
      $value = ['target_id' => $wrapped_term->getEntity()->id()];
    }
    return $value;
  }

  private function processAuthors($results) {
    // @todo check if we should be getting this value from the drupal stored
    // configurations.
    $expected_configured_roles = [
      'author',
      'editor',
      'illustrator',
      'transcriber',
      'compiler',
      'adaptor',
      'recipient',
      'contributor',
      'translator',
    ];
    $data = [];
    foreach ($results as $index => $result) {
      $wrapped_term = NULL;
      $name_parts = [];
      if (isset($result['valueURI'])) {
        $wrapped_term = $this->getWrappedTermFromUri((string) $result['valueURI'], 'person');
        if (!$wrapped_term) {
          continue;
        }
      }
      $children = (array) $result->children();
      $name_parts = isset($children['namePart']) ? explode(', ', $children['namePart']) : $name_parts;

      if (!$wrapped_term && !$name_parts) {
        continue;
      }

      $name_display_form = $children['displayForm'] ?? '';
      $data[$index] = [
        'target_id' => $this->getContributorTargetId($wrapped_term, $name_display_form, $name_parts),
      ];
      if ($index == 0) {
        $data[$index]['category'] = 'primary';
      }
      elseif ($index == 1) {
        $data[$index]['category'] = 'secondary';
      }
      elseif ($index == 2) {
        $data[$index]['category'] = 'tertiary';
      }
      else {
        $data[$index]['category'] = 'subsidiary';
      }

      // When the role tag is not present then we are dealing with an author.
      if (!isset($children['role'])) {
        $data[$index]['role'] = 'author';
        continue;
      }

      // Get the role tag content.
      $role_tag = (array)$children['role'];
      $role_id = is_string($role_tag['roleTerm']) ? strtolower($role_tag['roleTerm']) : $role_tag['roleTerm'];
      // Making sure that we save recipient instead of rcp.
      if ($role_id === 'rcp' || (is_array($role_id) && in_array('rcp', $role_id))) {
        $role_id = 'recipient';
      }
      // Skipping not configured roles.
      if (in_array($role_id, $expected_configured_roles)) {
        $data[$index]['role'] = $role_id;
      }
    }
    return $data;
  }

  private function getWrappedTermFromUri(string $uri, $vid = '') {
    $prefixes = [
      'https://commons.cwrc.ca/',
      'https://dev-02.cwrc.ca/islandora/object/',
      'orlando:'
    ];
    $id = str_replace($prefixes, '', $uri);
    if ($vid) {
      $publisher_dir = IngestItemsCommands::FILES_ENTITIES_DIR . '/' . $vid . 's';
      $filename = $publisher_dir . '/orlando_' . $id . '.xml';
    }
    else {
      foreach (['persons', 'organizations'] as $folder) {
        $publisher_dir = IngestItemsCommands::FILES_ENTITIES_DIR . '/' . $folder;
        $filename = $publisher_dir . '/orlando_' . $id . '.xml';
        if (is_file($filename)) {
          $vid = rtrim($folder, 's');
          break;
        }
      }
    }

    $wrapped_term = NULL;
    if ($vid) {
      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository $term_repository */
      $term_repository = $this->repositoryManager()->repository('taxonomy_term', $vid);
      $wrapped_term = $term_repository->findByCWRCId($id);
      if (!$wrapped_term) {
        $wrapped_term = $term_repository->createWrappedEntityFromXmlFile($id, $filename);
      }
    }
    return $wrapped_term;
  }

  private function getContributorTargetId(Person $person = NULL, string $name_display_form = '', $name_parts = []) {
    $contributor_storage = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $query = $contributor_storage->getQuery();

    $group = $query->orConditionGroup();
    $id = $person ? $person->getEntity()->id() : 0;
    $group->condition('field_person.target_id', $id);

    $and = $query->andConditionGroup();
    $and->condition('last_name', $name_parts[0] ?? '');
    $and->condition('first_name', $name_parts[1] ?? '');
    $group->condition($and);

    $ids = $query->condition($group)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    if ($ids) {
      return array_key_first($ids);
    }

    if ($person) {
      $name = $person->getPreferredName();
      $values = [
        'field_person' => ['target_id' => $id],
        'first_name' => $name->given,
        'last_name' => $name->family,
      ];
    }
    else {
      $values = [
        'first_name' => $name_parts[1] ?? '',
        'last_name' => $name_parts[0] ?? '',
      ];
    }
    $values['nick'] = Xss::filter($name_display_form);
    $contributor = $contributor_storage->create($values);
    $contributor->save();
    return $contributor->id();
  }

  private function getFieldsXpathMapping() {
    return [
      'title' => [
        'xpath' => '//d:mods/d:titleInfo[@usage="primary"]/d:title',
        'is_title' => TRUE,
      ],
      'author' => [
        'xpath' => '//d:mods/d:name|//d:mods/d:relatedItem[@type="host"]/d:name',
        'is_author' => TRUE,
      ],
      'bibcite_publisher' => [
        'xpath' => '//d:mods/d:originInfo/d:publisher|//d:mods/d:relatedItem[@type="host"]/d:originInfo/d:publisher',
        'attribute' => 'valueURI',
        'is_publisher' => TRUE,
      ],
      'bibcite_alternate_title' => [
        'xpath' => '//d:mods/d:titleInfo[@type="alternative"]/d:title',
        'is_title' => TRUE,
      ],
      'bibcite_secondary_title' => [
        'xpath' => '//d:mods/d:relatedItem[@type="host"]/d:titleInfo[@usage="primary"]/d:title',
        'is_title' => TRUE,
      ],
      'bibcite_tertiary_title' => [
        'xpath' => '//d:mods/d:titleInfo[@type="uniform" and @usage="primary"]/d:title',
        'is_title' => TRUE,
      ],
      'bibcite_custom1' => [
        'xpath' => '//d:mods/d:relatedItem[@type="host"]/d:titleInfo[@type="alternative"]/d:title',
        'is_title' => TRUE,
      ],
      'bibcite_number_of_volumes' => [
        'xpath' => '//mods/physicalDescription/extent',
      ],
      'bibcite_place_published' => [
        'xpath' => '//d:mods/d:originInfo/d:place/d:placeTerm[@type="text"]|//d:mods/d:relatedItem[@type="host"]/d:originInfo/d:place/d:placeTerm[@type="text"]',
      ],
      'bibcite_access_date' => [
        'xpath' => '//d:mods/d:location/d:url[@dateLastAccessed !=""]|//d:mods/d:originInfo/d:dateOther[@type="dateAccessed" and @encoding="iso8601"]',
        'attribute' => 'dateLastAccessed',
      ],
      'bibcite_url' => [
        'xpath' => '//d:mods/d:location/d:url',
      ],
      'bibcite_notes' => [
        'xpath' => '//d:mods/d:note[@type="public_note"]',
      ],
      'bibcite_lang' => [
        'xpath' => '//d:mods/d:language/d:languageTerm[@type="text"]',
      ],
      'bibcite_date' => [
        'xpath' => '//d:mods/d:originInfo/d:dateIssued[@encoding="iso8601"]|//d:mods/d:relatedItem[@type="host"]/d:originInfo/d:dateIssued[@encoding="iso8601"]',
        'is_date' => TRUE,
        'attribute_name' => 'type',
      ],
      'bibcite_volume' => [
        'xpath' => '//d:mods/d:part/d:detail[@type="volume"]/d:number|//d:mods/d:relatedItem[@type="host"]/d:part/d:detail[@type="volume"]/d:number',
      ],
      'bibcite_pages' => [
        'xpath' => '//d:mods/d:part/d:extent[@unit="page"]|//d:mods/d:relatedItem[@type="host"]/d:part/d:extent[@unit="page"]',
        'children_tags' => ['start', 'end'],
      ],
      'bibcite_edition' => [
        'xpath' => '//d:mods/d:relatedItem[@type="host"]/d:originInfo/d:edition',
      ],
      'bibcite_original_publication' => [
        'xpath' => '//d:mods/d:originInfo/d:dateOther[@type="original"]',
        'is_date' => TRUE,
        'attribute_name' => 'point',
      ],
      'bibcite_number' => [
        'xpath' => '//d:mods/d:relatedItem[@type="host"]/d:part/d:detail[@type="issue"]/d:number|//d:mods/d:part/d:detail[@type="issue"]/d:number',
      ],
    ];
  }

}
