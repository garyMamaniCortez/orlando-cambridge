<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository;

final class Organization extends Term {

  const FIELD_PREFERRED_NAME = 'name';

  public function getPreferredName(): string {
    return parent::getFieldValue(static::FIELD_PREFERRED_NAME)->value;
  }

  public function setPreferredNameFromXml(string $filename): Organization {
    $repository = $this->getRepository();
    $xpath = $repository->getPreferredNameXpath();
    $name = TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath . '/namePart')[0];
    $name = Xss::filter(strip_tags($name->asXML()));
    return parent::setFieldValue(static::FIELD_PREFERRED_NAME, $name);
  }

  public function setDataFromXml(string $filename): Organization {
    return $this->setPreferredNameFromXml($filename);
  }

}
