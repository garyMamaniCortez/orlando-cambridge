<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\Component\Utility\Xss;
use Drupal\name\Plugin\Field\FieldType\NameItem;
use Drupal\optional_end_date\Plugin\Field\FieldType\OptionalEndDateDateRangeItem;
use Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository;

/**
 * The wrapped entity for the person taxonomy vocabulary.
 */
final class Person extends Term {

  const FIELD_LIFESPAN = 'field_lifespan';
  const XPATH_LIFESPAN = '/entity/person[1]/description[1]/existDates[1]';

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->getRepository()->getPersonFormattedPrefferedName($this);
  }

  public function getPreferredName(): NameItem {
    return parent::getFieldValue(static::FIELD_PREFERRED_NAME)->first();
  }

  public function getLifespan() {
    $field = parent::getFieldValue(static::FIELD_LIFESPAN);
    return !$field->isEmpty() ? $field : NULL;
  }

  public function setLifespan(array $date_ranges): Person {
    if ($date_ranges) {
      parent::setFieldValue(static::FIELD_LIFESPAN, $date_ranges);
    }
    return $this;
  }

  public function setLifespanFromXml(string $filename): Person {
    $exist_dates = TaxonomyTermRepository::getValueFromXmlFile($filename, static::XPATH_LIFESPAN)[0];
    return $this->setLifespan(static::extractLifespanFromXml($exist_dates));
  }

  public function setPreferredNameFromXml(string $filename): Person {
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository $repository */
    $repository = $this->getRepository();
    $xpath = $repository->getPreferredNameXpath();
    $family = TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath . '/namePart[@partType="family"]')[0];
    $given =  TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath . '/namePart[@partType="given"]')[0];

    if (!$family) {
      $family =  TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath . '/namePart[1]')[0];
    }
    $name_parts = [
      'given' => $given ? Xss::filter(strip_tags($given->asXML())) : '',
      'family' => Xss::filter(strip_tags($family->asXML())),
    ];

    return parent::setFieldValue(static::FIELD_PREFERRED_NAME, $name_parts);
  }

  public function setDataFromXml(string $filename): Person {
    $this->setLifespanFromXml($filename);
    $this->setPreferredNameFromXml($filename);
    $this->setNameVariantsFromXml($filename);

    return $this;
  }

  public static function extractLifespanFromXml($exist_dates) {
    $date_ranges = [];
    foreach ($exist_dates as $date_single) {
      $date = strip_tags($date_single->standardDate->asXML());
      $formatted_date = date('Y-m-d', strtotime($date));
      $type = strip_tags($date_single->dateType->asXML());
      if ($type === 'birth') {
        $date_ranges['value'] = $formatted_date;
      }
      else {
        $date_ranges['end_value'] = $formatted_date;
      }
    }
    return $date_ranges;
  }

}
