<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

/**
 * The wrapped entity for orlando related taxonomy terms.
 */
abstract class Term extends WrappedEntity {

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'field_cwrc_id';
  /**
   * The field that contains the external changed date of the entity.
   */
  const FIELD_CHANGED = 'field_cwrc_record_changed_date';
  /**
   * The field that store the name variants.
   */
  const FIELD_NAME_VARIANTS = 'field_name_variants';
  const FIELD_PREFERRED_NAME = 'field_preferred_name';

  /**
   * The date format to use when comparing two dates.
   */
  const DATE_FORMAT = 'Y m d';

  /**
   * Gets the external ID.
   *
   * @return string
   *   The external id.
   */
  public function getExternalId(): string {
    return $this->getFieldValue(static::FIELD_ID)->value;
  }

  /**
   * Sets the external ID.
   *
   * @param string $id
   *   The ID.
   *
   * @return $this
   *   This term.
   */
  public function setExternalId(string $id): Term {
    $this->getEntity()->set(static::FIELD_ID, $id);
    return $this;
  }

  public function getChangedDate(): string {
    $time = $this->getEntity()->{static::FIELD_CHANGED}->value;
    return $this->getDateFromTime($time, static::DATE_FORMAT);
  }

  public function setChangedDate(string $formatted_date): Term {
    $time = strtotime($formatted_date);
    $this->getEntity()->set(static::FIELD_CHANGED, $time);
    return $this;
  }

  public function getNameVariants() {
    return $this->getEntity()->get(static::FIELD_NAME_VARIANTS);
  }

  public function setNameVariants(array $variants): Term {
    $this->getEntity()->set(static::FIELD_NAME_VARIANTS, $variants);
    return $this;
  }

  public function setNameVariantsFromXml(string $filename): Term {
    $repository = $this->getRepository();
    $variants = TaxonomyTermRepository::getValueFromXmlFile($filename, $repository->getNameVariantsXpath())[0];
    $types = static::validNameVariantTypes();

    $data = [];
    foreach ($variants as $variant) {
      $name = strip_tags($variant->namePart->asXML());
      $type = strip_tags($variant->variantType->asXML());
      if (!in_array($type, $types)) {
        $type = 'unknown';
      }
      $data[] = ['first' => $name, 'second' => $type];
    }

    return $this->setNameVariants($data);
  }

  abstract public function setDataFromXml(string $filename);

  /**
   * Checks if the stored changed date is different.
   *
   * @param int $time
   *   The date to use for comparison.
   *
   * @return bool
   *   TRUE if the date has changed, FALSE otherwise.
   */
  public function hasChanged(int $time): bool {
    $current_time = $this->getEntity()->{static::FIELD_CHANGED}->value;
    return $this->getDateFromTime($current_time, static::DATE_FORMAT) !== $this->getDateFromTime($time, static::DATE_FORMAT);
  }

  public static function validNameVariantTypes(): array {
    return [
      'birthName',
      'indexedName',
      'marriedName',
      'nickname',
      'orlandoStandardName',
      'pseudonym',
      'religiousName',
      'selfConstructedName',
      'styledName',
      'titledName',
      'usedForm',
      'royalName',
    ];
  }

}
