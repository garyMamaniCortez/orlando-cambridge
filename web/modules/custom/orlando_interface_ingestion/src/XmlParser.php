<?php

namespace Drupal\orlando_interface_ingestion;

use Drupal\Core\File\FileSystemInterface;
use Saxon\SaxonProcessor;

/**
 * Parse xml to transform them to a desired string.
 */
class XmlParser implements XmlParserInterface {

  const ENTRIES_DIR = 'private://orlando-2-0-c-modelling/entities/entries';

  /**
   * The file system helper service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The saxon processor.
   *
   * @var \Saxon\SaxonProcessor
   */
  protected $processor;

  /**
   * The path to the xslt folder.
   *
   * @var string
   */
  protected $xsltDir;

  /**
   * XmlParser constructor.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
    $this->processor = new SaxonProcessor();

    $path = DRUPAL_ROOT;
    $path .= '/' .drupal_get_path('module', 'orlando_interface_ingestion');
    $this->xsltDir = $path . '/xslt';
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorVersion(): string {
    return $this->processor->version();
  }

  /**
   * {@inheritdoc}
   */
  public function transform(string $filename, string $extension): ?string {
    $xsl_file = $this->getXslFile($extension);
    $xslt_processor = $this->processor->newXsltProcessor();
    $xslt_processor->setSourceFromFile($filename);
    $xslt_processor->compileFromFile($xsl_file);
    $output = $xslt_processor->transformToString();

    $xslt_processor->clearParameters();
    $xslt_processor->clearProperties();

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntryJson(string $id): string {
    $json = static::ENTRIES_DIR . '/transformed/' . $id . '.json';
    if (is_file($json)) {
      return file_get_contents($this->fileSystem->realpath($json));
    }

    $name = 'orlando_' . $id . '.xml';
    $uri = static::ENTRIES_DIR . '/' . $name;
    $filename = $this->fileSystem->realpath($uri);
    $content = $this->transform($filename, 'xml');
    $json = '';
    if ($content) {
      $temp_destination = 'temporary://' . $name;
//      $temp_destination = $this->fileSystem->saveData($content, $temp_destination, FileSystemInterface::EXISTS_REPLACE);
      $temp_destination = $this->fileSystem->realpath($temp_destination);
      file_put_contents($temp_destination, $content);
      if ($temp_destination) {
        $json = $this->transform($temp_destination, 'json');
        file_put_contents('/tmp/demo.json', $json);
      }
      $this->fileSystem->delete($temp_destination);
    }

    return $json;
  }

  private function getXslFile(string $extension): string {
    if ($extension === 'xml') {
      return $this->xsltDir . '/add_ids_standalone_events.xsl';
    }
    else {
      return $this->xsltDir . '/author_profile.xsl';
    }
  }

}
