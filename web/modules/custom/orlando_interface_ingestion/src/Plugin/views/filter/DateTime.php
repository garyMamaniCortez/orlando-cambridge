<?php

namespace Drupal\orlando_interface_ingestion\Plugin\views\filter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\datetime\Plugin\views\filter\Date;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Date views filter helper class.
 *
 * @ViewsFilter("oii_datetime")
 */
class DateTime extends Date {

  /**
   * {@inheritdoc}
   */
  protected function getFieldStorageDefinition() {
    if (isset($this->definition['entity field']) && !isset($this->definition['field_name'])) {
      $this->definition['field_name'] = $this->definition['entity field'];
    }
    return parent::getFieldStorageDefinition();
  }

}
