<?php

namespace Drupal\orlando_interface_ingestion\Plugin\search_api\processor;

use Drupal\orlando_interface_ingestion\Entity\ProfileEvent;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Filters out embedded events.
 *
 * @SearchApiProcessor(
 *   id = "oii_profile_event_filter",
 *   label = @Translation("Profile event filter"),
 *   description = @Translation("Filters out embedded events"),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class ProfileEventFilter extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $event = $item->getOriginalObject()->getValue();
      if (!($event instanceof ProfileEvent)) {
        continue;
      }

      // Removing embedded events from being indexed and show up in the explore
      // page.
      if ($event->isEmbedded()) {
        unset($items[$item_id]);
      }
    }
  }

}
