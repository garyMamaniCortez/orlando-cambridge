<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

trait TypedEntityRepositoryTrait {

  public static function getValueFromXmlFile(string $filename, string $query, $register_xpath_ns = []) {
    return static::getSimpleXMLElement($filename, $register_xpath_ns)->xpath($query);
  }

  public static function getSimpleXMLElement(string $filename, $register_xpath_ns = []) {
    $xml = new \SimpleXMLElement($filename, NULL, TRUE);
    if (!empty($register_xpath_ns['namespace'])) {
      $prefix = $register_xpath_ns['prefix'] ?? 'd';
      $xml->registerXPathNamespace($prefix, $register_xpath_ns['namespace']);
    }
    return $xml;
  }

  public static function transformXmlToArray(string $filename): array {
    return json_decode(json_encode((array) simplexml_load_file($filename)), 1);
  }

  public function getStringHash(string $str) {
    $algo = 'sha512';
    return substr(hash($algo, $str), 0, 50);
  }

}
