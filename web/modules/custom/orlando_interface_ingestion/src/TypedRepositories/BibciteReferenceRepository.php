<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

use Drupal\file\Entity\File;
use Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences\BibCiteReference;
use Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryBase;

class BibciteReferenceRepository extends TypedEntityRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * Finds a bibliography reference by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return mixed|null
   *   The wrapped entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findByCWRCId(string $id): ?BibCiteReference {
    $id = str_replace('orlando:', '', $id);
    $query = $this->getQuery();

    $entities = $query->condition('cwrc_id', $id)->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromXmlFile(string $external_id, string $filename, string $type = '') {
    $external_id = str_replace('orlando:', '', $external_id);
    $type = $type ?: static::extractTypeFromXml($filename);
    if (!$type || !is_string($type)) {
      throw new \Exception(sprintf('Couldn\'t determine or extract the bibliography type From %s file', $filename));
    }

    $file_uri = BibCiteReference::ITEMS_DIR . '/orlando_' . $external_id . '.xml';
    $file_storage = $this->entityTypeManager->getStorage('file');
    $files = $file_storage->loadByProperties(['uri' => $file_uri]);
    if ($files) {
      $file = reset($files);
    }
    else {
      $file = File::create([
        'uri' => $file_uri,
        'uid' => ['target_id' => 1],
        'status' => FILE_STATUS_PERMANENT,
      ]);
      $file->save();
    }
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'type' => $type,
        'cwrc_id' => $external_id,
        'uid' => ['target_id' => 1],
        'mod_file' => [
          'target_id' => $file->id(),
        ],
      ]);
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromXml($filename);
    return $wrapped_entity->save();
  }

  public static function getWrappedEntityByCWRCId(string $id): ?BibCiteReference {
    $wrapped_entity = NULL;
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_reference');
    $ids = $storage->getQuery()
      ->condition('cwrc_id', $id)
      ->range(0, 1)
      ->accessCheck(FALSE)
      ->execute();

    if ($ids && ($bibliography_reference = $storage->load(array_key_first($ids)))) {
      $wrapped_entity = \Drupal::service('Drupal\typed_entity\RepositoryManager')
        ->wrap($bibliography_reference);
    }
    return $wrapped_entity;
  }

  public static function extractTypeFromXml(string $filename): string {
    $bundle = '';
    if (!is_file($filename)) {
      return $bundle;
    }

    $xml_obj = new \SimpleXMLElement($filename, NULL, TRUE);
    $xml_obj->registerXPathNamespace('d', 'http://www.loc.gov/mods/v3');
    $xml_arr = self::transformXmlToArray($filename);
    $genre = $xml_arr['mods']['genre'] ?? [];
    $type_of_resource = $xml_arr['mods']['typeOfResource'] ?? '';
    if (is_string($type_of_resource) && $type_of_resource === 'sound recording') {
      return 'audiovisual';
    }
    elseif (is_string($type_of_resource) && $type_of_resource === 'moving image') {
      return 'film';
    }
    $type_xpath_mapping = [
      'book' => '//d:genre[text()="book" and not(following-sibling::d:genre[text()="article"])]',
      'journal_article' => '//d:genre[text()="article" and not(following-sibling::d:genre[text()="book"]) and not(preceding-sibling::d:genre[text()="book"])]',
      'website' => '//d:genre[text()="web site"]',
      'manuscript' => '//d:genre[text()="script"]',
      'correspondence' => '//d:genre[text()="letter"]',
      'book_chapter_split_xpath' => [
        'items' => [
          'article',
          'book',
        ],
        'type' => 'book_chapter',
      ],
      'journal_split_xpath'  => [
        'items' => [
          'journal',
          'periodical',
        ],
        'type' => 'journal',
      ],
    ];
    foreach ($type_xpath_mapping as $type => $selector) {
      if (!is_array($selector)) {
        $xml_elt = $xml_obj->xpath($selector);
        $data = (array) $xml_elt ?? [];
        if (!empty($data[0])) {
          $bundle = $type;
          break;
        }
      }
      else {
        $found_match = FALSE;
        foreach ($selector['items'] as $item) {
          $found_match = in_array($item, $genre);
        }
        if ($found_match) {
          $bundle = $selector['type'];
          break;
        }
      }
    }

    return $bundle;
  }

}
