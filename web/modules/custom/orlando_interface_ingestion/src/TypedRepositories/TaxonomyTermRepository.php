<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Person;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term;
use Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryBase;

/**
 * The repository for terms related CWRC.
 */
class TaxonomyTermRepository extends TypedEntityRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'field_cwrc_id';

  /**
   * The field that contains the external changed date of the entity.
   */
  const FIELD_CHANGED = 'field_cwrc_record_changed_date';

  /**
   * Finds a taxonomy term by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term|null
   *   The wrapped entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findByCWRCId(string $id): ?Term {
    // Performing this cleaning here again just in case this is called from
    // somewhere it was not done yet.
    $id = AuthorProfile::cleanEntryIdentifierEntityUri($id);
    $id = str_replace(['orlando:', 'orlando_'], '', $id);
    $query = $this->getQuery();

    $entities = $query->condition(static::FIELD_ID, $id)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromXmlFile(string $external_id, string $filename) {
    $external_id = str_replace('orlando:', '', $external_id);
    $changed_date = (array) static::getValueFromXmlFile($filename, $this->getChangedDateXpath());
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'vid' => $this->bundle,
        static::FIELD_ID => $external_id,
        static::FIELD_CHANGED => strtotime($changed_date[0]),
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term $term */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromXml($filename);
    return $wrapped_entity->save();
  }

  public function getPersonFormattedPrefferedName(Person $person) {
    $name_components = $person->getPreferredName();
    if (!$name_components) {
      return $person->getEntity()->label();
    }

    $name_components = $name_components->toArray();
    $format = name_get_format_by_machine_name('default');
    $field_name = 'field_preferred_name';
    $view_display_id = 'taxonomy_term.person.author_profile';

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_display */
    $view_display = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load($view_display_id);

    if (!$view_display) {
      return parent::label();
    }

    $settings = $view_display->getRenderer($field_name)->getSettings();
    /** @var \Drupal\name\NameFormatParser $name_format_parser */
    $name_format_parser = \Drupal::service('name.format_parser');
    return $name_format_parser
      ->parse($name_components, $format, $settings);
  }

  public function getAuthorProfile(int $tid) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $nodes = $node_storage->loadByProperties([
      'type' => 'author_profile',
      'field_person.target_id' => $tid,
    ]);
    return $nodes ? reset($nodes) : NULL;
  }

  public function getChangedDateXpath(): string {
    return '/entity/' . $this->bundle . '[1]/recordInfo[1]/originInfo[1]/recordChangeDate[1]';
  }

  public function getPreferredNameXpath(): string {
    return '/entity/' . $this->bundle . '[1]/identity[1]/preferredForm[1]';
  }

  public function getNameVariantsXpath(): string {
    return '/entity/' . $this->bundle . '[1]/identity[1]/variantForms[1]';
  }

}
