<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryBase;

/**
 * The repository for terms related CWRC.
 */
class AuthorProfileRepository extends TypedEntityRepositoryBase {

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'field_cwrc_id';

  /**
   * Finds a taxonomy term by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile|null
   *   The author profile if found.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findByCWRCId(string $id): ?AuthorProfile {
    $query = $this->getQuery();

    $entities = $query->condition(static::FIELD_ID, $id)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromObject(\stdClass $data, int $uid = 1) {
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'type' => $this->bundle,
        'title' => $data->entry->metadata->title,
        'uid' => $uid,
        static::FIELD_ID => $data->entry->entry_identifiers->id,
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromObject($data);
    return $wrapped_entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function wrapMultipleById(array $items): array {
    return parent::wrapMultipleById($items);
  }

}
