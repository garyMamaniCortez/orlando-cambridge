<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

use Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryBase;

class MediaRepository extends TypedEntityRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * Finds a media entity by it's attached filename.
   *
   * @param string $filename
   *   The filename to search for.
   * @param string $media_type
   *   The optional media type.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image|null
   *   The wrapped media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findMediaByFilename(string $filename, string $media_type = 'image'): ?Image {
    $field = $media_type === 'image' ? 'field_media_image.entity.uri' : '';
    if (!$field) {
      return NULL;
    }

    $entities = [];
    $ids = $this->getQuery()
      ->condition($field, $filename)
      ->execute();

    if ($ids) {
      $entities = $this->wrapMultipleById($ids);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createMediaImageFromData(string $file_uri, int $uid = 1) {
    $info = pathinfo($file_uri);
    $mod = str_replace($info['extension'], 'xml', $file_uri);
    $entity = $this->entityTypeManager->getStorage('media')
      ->create([
        'bundle' => 'image',
        'uid' => $uid,
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setImageFile($file_uri);
    $wrapped_entity->setNameFromXml($mod);
    return $wrapped_entity->save();
  }

  public function ingestMediaImagesByEntries(array $entries, bool $re_ingest = FALSE) {
    /** @var \Drupal\typed_entity\RepositoryManager $repository_manager */
    $repository_manager = \Drupal::service(RepositoryManager::class);
    assert($repository_manager instanceof RepositoryManager);
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\AuthorProfileRepository $repository */
    $repository = $repository_manager->repository('node', 'author_profile');
    foreach ($entries as $entry) {
      $wrapped_author_profile = $repository->findByCWRCId($entry);
      $field_images = $wrapped_author_profile ? $wrapped_author_profile->getFieldValue('field_images') : NULL;
      if ($field_images && ($field_images->isEmpty() || $re_ingest)) {
        if (!$field_images->isEmpty()) {
          $wrapped_author_profile->getEntity()->field_images = NULL;
        }
        $wrapped_author_profile->ingestMediaImages();
        $wrapped_author_profile->save();
      }
    }
  }

}
