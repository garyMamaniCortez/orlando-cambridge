<?php

namespace Drupal\orlando_interface_ingestion\TypedRepositories;

use Drupal\Component\Utility\Xss;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryBase;

class ProfileEventRepository extends TypedEntityRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'cwrc_id';

  /**
   * Finds a profile event by id which is it's content.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term|null
   *   The wrapped entity or null.
   * @param false $is_embedded
   *   Either this is an embedded event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findById(string $id, $is_embedded = FALSE) {
    $query = $this->getQuery();

    if ($is_embedded) {
      $id = $this->getStringHash($id);
    }
    $entities = $query->condition(static::FIELD_ID, $id)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createEmbeddedEventFromObject(\stdClass $event, bool $is_validated = FALSE) {
    $title = '';
    $is_milestone = FALSE;
    // We are dealing with a standalone event.
    if (isset($event->standalone_event)) {
      if (isset($event->standalone_event->metadata->title) && is_string($event->standalone_event->metadata->title)) {
        $title = $event->standalone_event->metadata->title;
        $title = substr(Xss::filter(strip_tags($title)), 0, 255);
      }
      $chronevent = $event->standalone_event->chronevent;
      $event->status = $chronevent->event->status;
      $event->content = $chronevent->event->content;
      $event->relevance = $chronevent->event->relevance;
      $event->type = $chronevent->event->type;
      $event->time = $chronevent->event->time;
      $event->bibcits = $chronevent->event->bibcits ?? '';

      if (!empty($chronevent->prose)) {
        $event->prose = $chronevent->prose;
      }
      if (!empty($chronevent->citations)) {
        $event->citations = $chronevent->citations;
      }
      if (!empty($chronevent->scholarnotes)) {
        $event->scholarnotes = $chronevent->scholarnotes;
      }
    }
    else {
      $is_milestone = isset($event->milestone) && $event->milestone === 'yes';
    }

    if (!$is_validated) {
      $this->validateEventObject($event);
    }

    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'bundle' => 'profile_event',
        'status' => $event->status,
        'title' => $title,
        'is_milestone' => $is_milestone,
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromObject($event);

    return $wrapped_entity->save();
  }

  public function validateEventObject(\stdClass $event) {
    $err_msg = '';
    if (!isset($event->status) || !is_string($event->status)) {
      $err_msg = 'We couldn\'t identify the type of the event this object contains!';
    }
    elseif (empty($event->content->chronprose_content) || empty($event->content->chronprose_id)) {
      $err_msg = 'Trying to save an event without a chronprose is not allowed!';
    }
    elseif (empty($event->time->value)) {
      $err_msg = 'An event should always have date!';
    }

    if ($err_msg) {
      throw new \Exception($err_msg);
    }
  }

  public function updateEmbeddedEventFromEntry(\stdClass $entry_object) {
    $entry_id = $entry_object->entry->entry_identifiers->id;
    /** @var \Drupal\typed_entity\RepositoryManager $repository_manager */
    $repository_manager = \Drupal::service(RepositoryManager::class);
    assert($repository_manager instanceof RepositoryManager);
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\AuthorProfileRepository $repository */
    $repository = $repository_manager->repository('node', 'author_profile');
    $wrapped_author_profile = $repository->findByCWRCId($entry_id);
    if (!$wrapped_author_profile) {
      return;
    }

    $items = [];
    $processed_events = [];
    if (isset($entry_object->entry->biography->section_content)) {
      $items[] = $entry_object->entry->biography->section_content;
    }
    if (isset($entry_object->entry->writing->section_content)) {
      $items[] = $entry_object->entry->writing->section_content;
    }
    foreach ($items as $item) {
      $processed_events = $this->processSectionEventItems($item);
    }

    return $processed_events;
  }

  /**
   * {@inheritdoc}
   */
  public function wrapMultipleById(array $items): array {
    return parent::wrapMultipleById($items);
  }

  private function processSectionEventItems($items) {
    $processed_events = [];
    foreach ($items as $item) {
      if (isset($item->content) && is_array($item->content)) {
        $this->processSectionEventItems($item->content);
      }
      elseif (isset($item->event) && isset($item->event->time->value)) {
        $event = $item->event;
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event $wrapped_entity */
        $wrapped_entity = $this->findById($event->content->chronprose_content, TRUE);
        if ($wrapped_entity) {
          $wrapped_entity->setDataFromObject($event);
          $wrapped_entity->save();
        }
        else {
          $wrapped_entity = $this->createEmbeddedEventFromObject($event);
        }
        $updated_events[] = $wrapped_entity;
      }
    }
    return $processed_events;
  }

}
