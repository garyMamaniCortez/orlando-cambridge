<?php

namespace Drupal\orlando_interface_ingestion\Commands;

use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\Event\ItemIngestionEvent;
use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\typed_entity\RepositoryManager;
use Drush\Commands\DrushCommands;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Implements DeleteEntriesCommands commands.
 */
class DeleteEntriesCommands extends DrushCommands {

  /**
   * The typed entity repository manager.
   *
   * @var \Drupal\typed_entity\RepositoryManager
   */
  protected $repositoryManager;

  protected $eventDispatcher;

  /**
   * Construct a new IngestCommands object.
   *
   * @param \Drupal\typed_entity\RepositoryManager $repository_manager
   *   The typed entity repository manager.
   */
  public function __construct(RepositoryManager $repository_manager, EventDispatcherInterface $event_dispatcher) {
    parent::__construct();
    $this->repositoryManager = $repository_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Deletes a list of author profile entries.
   *
   * @command oi_ingest:delete-entries
   *
   * @param array $opts
   *   A list options which currently support entries. Pass "all" to
   *   delete all entries.
   * @param string $entry_type
   *   The entry type/ Currently supporting author_profile and event for
   *   standalone events.
   *
   * @usage drush oi_ingest:delete-entries -e akhman -e atwoma
   *   To delete akhman and atwoma entries.
   * @usage drush oi_ingest:delete-entries --all
   *   To delete all the author profiles entities.
   * @usage drush oi_ingest:delete-entries --all --delete-images
   *   To delete all the author profiles entities and their related image medias.
   *
   * @aliases oii_delete_entries
   */
  public function deleteEntries(string $entry_type, $opts = ['entries|e' => [], 'all|a' => FALSE, 'delete-images|i' => FALSE]) {
    if (!in_array($entry_type, ['author_profile', 'event'])) {
      return;
    }

    if ($entry_type === 'author_profile') {
      $this->processAuthorProfiles($opts);
    }
    else {
      $this->processStandaloneEvents($opts);
    }
    $event = new \Symfony\Contracts\EventDispatcher\Event();
    $this->eventDispatcher->dispatch($event, IngestionEvents::DELETION_FINISHED);
  }

  private function processAuthorProfiles(array $options) {
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\AuthorProfileRepository $repository */
    $repository = $this->getWrappedEntityRepository('node', 'author_profile');
    $counter = 1;
    if ($options['all']) {
      $nids = $repository->getQuery()
        ->execute();
      /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile[] $wrapped_entities */
      $wrapped_entities = [];
      if (is_array($nids) && count($nids) >= 1) {
        $wrapped_entities = $repository->wrapMultipleById($nids);
      }

      $total = count($nids);
      foreach ($wrapped_entities as $wrapped_entity) {
        $id = $wrapped_entity->getExternalId();
        $this->deleteWrappedAuthorProfileEntity($wrapped_entity, FALSE, [
          'counter' => $counter,
          'total' => $total,
        ]);
        $event = new ItemIngestionEvent($id, 'entry', '');
        $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_DELETE);
        $counter++;
      }
    }
    elseif (!empty($options['entries'])) {
      $total = count($options['entries']);
      foreach ($options['entries'] as $entry) {
        $wrapped_author_profile = $repository->findByCWRCId($entry);
        if ($wrapped_author_profile) {
          $id = $wrapped_author_profile->getExternalId();
          $this->deleteWrappedAuthorProfileEntity($wrapped_author_profile, FALSE, [
            'counter' => $counter,
            'total' => $total,
          ]);
          $event = new ItemIngestionEvent($id, 'entry', '');
          $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_DELETE);
        }
        else {
          $this->io()->warning(dt('No author profile with @id was found. Skipping!', [
            '@id' => $entry,
          ]));
        }
        $counter++;
      }
    }
  }

  private function processStandaloneEvents(array $options) {
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository $repository */
    $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
    $counter = 1;

    if ($options['all']) {
      $ids = $repository->getQuery()
        ->condition('status', 'embedded', '<>')
        ->execute();
      $wrapped_entities = [];
      if (is_array($ids) && count($ids) >= 1) {
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event[] $wrapped_entities */
        $wrapped_entities = $repository->wrapMultipleById($ids);
      }
      $total = count($ids);
      foreach ($wrapped_entities as $wrapped_entity) {
        $id = $wrapped_entity->getExternalId();
        $this->deleteWrappedStandaloneEvent($wrapped_entity,[
          'counter' => $counter,
          'total' => $total,
        ]);
        // Since we dispatching a delete event no need to send the correct path
        // uri.
        $event = new ItemIngestionEvent($id, 'standalone_event', '');
        $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_DELETE);
        $counter++;
      }
    }
    elseif (!empty($options['entries'])) {
      $total = count($options['entries']);
      foreach ($options['entries'] as $entry) {
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event $wrapped_entity */
        $wrapped_entity = $repository->findById($entry);
        if ($wrapped_entity && !$wrapped_entity->isEmbedded()) {
          $id = $wrapped_entity->getExternalId();
          $this->deleteWrappedStandaloneEvent($wrapped_entity, [
            'counter' => $counter,
            'total' => $total,
          ]);
          $event = new ItemIngestionEvent($id, 'standalone_event', '');
          $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_DELETE);
        }
        else {
          $this->io()->warning(dt('No event with @id was found. Skipping!', [
            '@id' => $entry,
          ]));
        }
        $counter++;
      }
    }
  }

  /**
   * Gets the typed entity repository.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryInterface
   *   The typed entity repository.
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   */
  private function getWrappedEntityRepository(string $entity_type_id, string $bundle) {
    assert($this->repositoryManager instanceof RepositoryManager);
    return $this->repositoryManager->repository($entity_type_id, $bundle);
  }

  /**
   * Deletes the given wrapped entity author profile.
   *
   * @param \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile $author_profile
   *   The wrapped entity author profile to delete.
   * @param bool $delete_images
   *   Flag to indicate if images should also be deleted.
   * @param array $position
   *   The position counter.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function deleteWrappedAuthorProfileEntity(AuthorProfile $author_profile, bool $delete_images = FALSE, $position = []) {
    $label = $author_profile->label();
    $this->io()->text(dt('@positionDeleting @label entity', [
      '@label' => $label,
      '@position' => $position ? "[${position['counter']}/${position['total']}] " : '',
    ]));

    if ($delete_images) {
      $images = $author_profile->getImages();
      if (!$images->isEmpty()) {
        foreach ($images as $media) {
          $media->entity->delete();
        }
      }
    }

    // Delete summary paragraph.
    $summary = $author_profile->getSummary();
    $summary_entity = !$summary->isEmpty() ? $summary->first()->entity : NULL;
    if ($summary_entity) {
      $summary_entity->delete();
    }
    // Delete the other paragraphs.
    $paragraphs = $author_profile->getParagraphs();
    if (!$paragraphs->isEmpty()) {
      foreach ($paragraphs as $paragraph) {
        /** @var \Drupal\paragraphs\ParagraphInterface $paragraph_entity */
        $paragraph_entity = $paragraph->entity;
        if ($paragraph_entity) {
          $is_event = $paragraph_entity->bundle() === 'event';
          $event = $is_event ? $paragraph_entity->get('field_event') : NULL;
          // Delete the events attached to the event paragraphs.
          if ($event && !$event->isEmpty() && $event->entity) {
            // Delete the paragraphs attached to event entity.
            foreach ($event->entity->get('field_content') as $paragraph_item) {
              if (!empty($paragraph_item->entity)) {
                $paragraph_item->entity->delete();
              }
            }
            $event->entity->delete();
          }
          $paragraph_entity->delete();
        }
      }
    }
    // Delete the entire entity.
    $author_profile->getEntity()->delete();
    $this->io()->success(dt('@position@label entity have been successfully deleted', [
      '@label' => $label,
      '@position' => $position ? "[${position['counter']}/${position['total']}] " : '',
    ]));
  }

  private function deleteWrappedStandaloneEvent(Event $event, $position = []) {
    $label = $event->label();
    $this->io()->text(dt('@positionDeleting @label entity', [
      '@label' => $label,
      '@position' => $position ? "[${position['counter']}/${position['total']}] " : '',
    ]));

    // Delete the other paragraphs.
    $paragraphs = $event->getContent();
    if (!$paragraphs->isEmpty()) {
      foreach ($paragraphs as $paragraph) {
        if ($paragraph->entity) {
          $paragraph->entity->delete();
        }
      }
    }
    $event->getEntity()->delete();
    $this->io()->success(dt('@position@label entity have been successfully deleted', [
      '@label' => $label,
      '@position' => $position ? "[${position['counter']}/${position['total']}] " : '',
    ]));
  }

}
