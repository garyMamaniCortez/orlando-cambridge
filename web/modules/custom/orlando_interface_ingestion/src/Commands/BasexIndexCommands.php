<?php


namespace Drupal\orlando_interface_ingestion\Commands;

use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Drush\Commands\DrushCommands;

class BasexIndexCommands extends DrushCommands {

  protected $database;

  /**
   * BasexDropCreateIndex constructor.
   *
   * @param \Drupal\orlando_interface_search\Driver\Database\basex\Connection $connection
   *   The basex database connection.
   */
  public function __construct(Connection $connection) {
    parent::__construct();
    $this->database = $connection;
  }

  /**
   * Create or Drop the basex database index.
   *
   * @command oi_index:operation
   *
   * @param string $type
   *   The type of the operation. Only support the create and drop operation
   *   type.
   * @param array $opts
   *   The list of option flags.
   *
   * @throws \Exception
   *
   * @usage drush oi_index:operation create
   *   Create the basex index.
   * @usage drush oi_index:operation create --init-settings
   *   Create the basex index and also initialize the needed configurations.
   * @usage drush oi_index:operation drop
   *   Drops the basex index.
   *
   * @aliases oii_index_op
   */
  public function operation(string $type, array $opts = ['init-settings|i' => FALSE]) {
    if ($type !== 'create' && $type !== 'drop') {
      throw new \Exception(dt('Invalid operation type (@type) provided!', [
        '@type' => $type,
      ]));
    }

    $this->database->openDatabase();
    if ($type === 'create') {
      if ($opts['init-settings']) {
        $this->database->initSettings();
      }
      $this->database->createIndex();
    }
    else {
      $this->database->dropIndex();
    }
    $this->database->closeSession();
  }

}
