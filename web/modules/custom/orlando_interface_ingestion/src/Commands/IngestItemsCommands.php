<?php

namespace Drupal\orlando_interface_ingestion\Commands;

use Drupal\Core\File\FileSystemInterface;
use Drupal\orlando_interface_ingestion\Event\ConnectionIngestionEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\Event\IngestionStartEvent;
use Drupal\orlando_interface_ingestion\Event\ItemIngestionEvent;
use Drupal\orlando_interface_ingestion\TypedRepositories\BibciteReferenceRepository;
use Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event;
use Drupal\typed_entity\RepositoryManager;
use Drush\Commands\DrushCommands;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class IngestItemsCommands extends DrushCommands {

  const FILES_ENTRIES_DIR = 'private://orlando-2-0-c-modelling/entities/entries';
  const FILES_ENTITIES_DIR = 'private://orlando-2-0-c-modelling/entities';
  const FILENAME_ID_SEPARATOR = '_';

  /**
   * The file system helper service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The typed entity repository manager.
   *
   * @var \Drupal\typed_entity\RepositoryManager
   */
  protected $repositoryManager;

  protected $eventDispatcher;

  /**
   * Construct a new IngestCommands object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system helper service.
   * @param \Drupal\typed_entity\RepositoryManager $repository_manager
   *   The typed entity repository manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service
   */
  public function __construct(FileSystemInterface $file_system, RepositoryManager $repository_manager, EventDispatcherInterface $event_dispatcher) {
    parent::__construct();

    $this->fileSystem = $file_system;
    $this->repositoryManager = $repository_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Ingests a list of author profiles entries.
   *
   * @command oi_ingest:entries
   *
   * @param string $entry_type
   *   The entry type/ Currently supporting author_profile and event for
   *   standalone events.
   * @param array $opts
   *   A list of options where ids of entries to ingest can be passed using the
   *   entries flag or the all flag to ingest all available entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries author_profile -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries author_profile -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries author_profile --all
   *   Ingest all entries.
   * @usage drush oi_ingest:entries author_profile --all --limit 10
   *   Ingest all entries but limited to 10.
   * @usage drush oi_ingest:entries author_profile --all --offset 400 --limit 10
   *   Ingest entries where the sequence will start at 309 offset in the list
   *   but limited to 10.
   *
   * @aliases oii_entries
   */
  public function ingestItems(string $entry_type, array $opts = ['entries|e' => [], 'all|a' => FALSE, 'limit' => 0, 'offset|o' => 0]) {
    $items = [];
    $is_author_profile = $entry_type === 'author_profile';
    if ($is_author_profile) {
      $entries_dir_realpath = $this->fileSystem->realpath(AuthorProfile::ENTRIES_DIR) . '/';
    }
    elseif ($entry_type === 'event') {
      $entries_dir_realpath = $this->fileSystem->realpath(Event::ITEMS_DIR) . '/';
    }
    else {
      throw new \Exception(dt('Invalid entry type (@type) provided!', [
        '@type' => $entry_type,
      ]));
    }

    if ($opts['all']) {
      $items = $this->getAllItems($entries_dir_realpath);

      if ($opts['offset'] > 0) {
        // Since element 1 is at 0 index we ensure that when offset is 1 we
        // pass 0. This also allow to start at the 400 elements instead of 401.
        $items = array_slice($items, $opts['offset'] - 1);
      }
    }
    elseif (!empty($opts['entries'])) {
      $items = $opts['entries'];
    }

    if (!$items) {
      throw new \Exception(dt('No entries was provided or @dir is empty!', [
        '@dir' => $entries_dir_realpath,
      ]));
    }

    // Let other modules know that we have started the ingestion and they can
    // do stuff.
    $event = new IngestionStartEvent($items, $entry_type);
    $this->eventDispatcher->dispatch($event, IngestionEvents::INGESTION_STARTED);

    $limit = (int) $opts['limit'];
    $counter = 1;
    $total = count($items);
    // In case we have a limit and it's less than the total count of all the
    // elements, then the limit become the total number of items to be
    // processed.
    $total = ($limit > 0 && $limit < $total) ? $limit : $total;
    $json_key = $is_author_profile ? 'entry' : 'standalone_event';
    foreach ($items as $id) {
      if (!($data = $this->getItemData($is_author_profile, $id))) {
        $counter++;
        continue;
      }

      $connections = $data->{$json_key}->connections ?? NULL;
      if (!$connections) {
        $this->io()->warning(dt('We couldn\'t extract any connections from json for @id', [
          '@id' => $id,
        ]));
        $counter++;
        continue;
      }
      else {
        $this->ingestItemConnections($id, $connections, [
          'counter' => $counter,
          'total' => $total,
        ]);
        $this->ingestItem($is_author_profile, $data, $id);
        $event = new ItemIngestionEvent($id, $json_key, $data->xmlFile);
        $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_INGESTION);
      }

      if ($limit > 0) {
        if ($counter === $limit) {
          break;
        }
      }
      $counter++;
    }
    $event = new \Symfony\Contracts\EventDispatcher\Event();
    $this->eventDispatcher->dispatch($event, IngestionEvents::INGESTION_FINISHED);
  }

  /**
   * Ingests a list of author profiles entry images.
   *
   * @command oi_ingest:entries-images
   *
   * @param array $opts
   *   Comma-separated list of author profiles ids to ingest. Pass "all" to
   *   ingest all entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries-images -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries-images -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries-images all
   *   Ingest all entries. Not currently supported.
   *
   * @aliases oii_entries_images
   */
  public function ingestEntriesImages(array $opts = ['entries|e' => [], 'all|a' => FALSE, 'reingest|i' => FALSE]) {
    if ($opts['all']) {
      $entries_dir_realpath = $this->fileSystem->realpath(AuthorProfile::ENTRIES_DIR) . '/';
      $entries = $this->getAllItems($entries_dir_realpath);
    }
    else {
      $entries = $opts['entries'];
    }
    $reingest = $opts['reingest'];
    /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\MediaRepository $repository */
    $repository = $this->getWrappedEntityRepository('media', 'image');
    $repository->ingestMediaImagesByEntries($entries, $reingest);
  }

  /**
   * Re-ingests embedded events based on a list of author profiles entries.
   *
   * @command oi_ingest:entries-events
   *
   * @param array $opts
   *   Comma-separated list of author profiles ids to ingest. Pass "all" to
   *   ingest all entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries-events -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries-events -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries-events all
   *   Ingest all entries. Not currently supported.
   *
   * @aliases oii_entries_events
   */
  public function reingestEntriesEmbeddedEvents($opts = ['entries|e' => []]) {
    $entries = $opts['entries'];
    foreach ($entries as $entry) {
      if (!($data = $this->getItemData(TRUE, $entry))) {
        continue;
      }

      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository $repository */
      $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
      $this->say(dt('Processing @id', ['@id' => $entry]));
      $result = $repository->updateEmbeddedEventFromEntry($data);
      if (!is_array($result)) {
        $this->io()->error(dt('The @id wasn\'t processed', [
          '@id' => $entry,
        ]));
      }
      else {
        $this->io()->success(dt('Successfully re-ingested @count events embedded in @entry', [
          '@count' => count($result),
          '@entry' => $entry,
        ]));
      }
    }
  }

  /**
   * Helper command to test an xpath for a particular connection.
   *
   * @command oi_ingest:test-xpath
   *
   * @param string $connection_id
   * @param string $vid
   * @param string $xpath
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   *
   * @usage drush oi_ingest:test-xpath 'orlando:0f34ea95-cc5f-4ec5-bd0f-e6ab2dbdc69b' person '/entity/person[1]/identity[1]/preferredForm[1]/namePart[@partType='family"]'
   *   Test the provided xpath for a person with provided id.
   *
   * @aliases oii_test_xpath
   */
  public function testXpathSelection(string $connection_id, string $vid, string $xpath) {
    $connection = $vid . 's';
    $connections_dir = static::FILES_ENTITIES_DIR . '/' . $connection;
    // Getting the connection filename.
    $filename_id = str_replace(':', self::FILENAME_ID_SEPARATOR, $connection_id);
    $filename = "$connections_dir/$filename_id.xml";
    if (!is_file($filename)) {
      $this->io()->warning(dt('@filename does\'t exists!', [
        '@filename' => $filename,
      ]));
      return;
    }

    $value = TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath);
    $this->io()->success(dt('The queried values for @id with @xpath is<br> @value', [
      '@id' => $connection_id,
      '@xpath' => $xpath,
      '@value' => json_encode($value),
    ]));
  }

  /**
   * Helper command to  update xml uri attached to the node author profiles.
   *
   * @command oi_ingest:update-xml-uris
   **
   * @usage drush oi_ingest:update-xml-uris
   *   To update the paths.
   *
   * @aliases oii_update_xml_uris
   */
  public function updateXmlEntryFilesUris() {
    /** @var \Drupal\file\FileStorageInterface $file_storage */
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $fids = $file_storage->getQuery()
      ->condition('uri', 'transformed', 'CONTAINS')
      ->accessCheck(FALSE)
      ->execute();

    if ($fids) {
      foreach ($fids as $fid) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $file_storage->load($fid);
        $old_uri = $file->getFileUri();
        $entry = str_replace('.xml', '', $file->getFilename());
        $uri = AuthorProfile::getAuthorProfileXmlUri($entry) . "/$entry.xml";
        $file->setFileUri($uri);
        $file->save();

        if ($old_uri !== $file->getFileUri()) {
          $this->io()->success(dt('The @name uri have been successful updated!', ['@name' => $file->getFilename()]));
        }
        else {
          $this->io()->warning(dt('Failed to update the uri for @name', ['@name' => $file->getFilename()]));
        }
      }
    }
    else {
      $this->io()->warning(dt('No files contains the word transformed!'));
    }
  }

  private function ingestItemConnections(string $entry_id, $connections, $position) {
    $ext_uri_prefixes = [
      'https://commons.cwrc.ca/',
      'https://dev-02.cwrc.ca/islandora/object/',
    ];
    $bundles = [
      'person' => 'taxonomy_term',
      'organization' => 'taxonomy_term',
      'bibl' => 'bibcite_reference',
    ];
    $bibliography_repositories = [];

    foreach ($bundles as $bundle => $entity_type_id) {
      $connection = $bundle . 's';
      $connection_ids = $connections->{$connection} ?? NULL;
      if (is_array($connection_ids) && !empty($connection_ids)) {
        $connection_ids = array_unique($connection_ids);
        $connections_dir = static::FILES_ENTITIES_DIR . '/' . $connection;
        // Getting the current vocabulary wrapped entity repository.
        /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\TaxonomyTermRepository $repository */
        $repository = $this->getWrappedEntityRepository($entity_type_id, $bundle);
        $processed_ids = [];
        $is_bibliography = $bundle === 'bibl';
        foreach ($connection_ids as $connection_ext_uri) {
          if (isset($processed_ids[$connection_ext_uri])) {
            continue;
          }
          $processed_ids[$connection_ext_uri] = $connection_ext_uri;

          // Extracting the connection id from the external uri.
          $connection_id = str_replace($ext_uri_prefixes, '', $connection_ext_uri);
          // Getting the connection filename.
          $filename_id = str_replace(':', self::FILENAME_ID_SEPARATOR, $connection_id);
          $filename = "$connections_dir/$filename_id.xml";
          // Let the user know which connection id we are working with.
          $this->io()->text(dt('Ingestion process for: Entry id: @entry_id (@counter/@total) | File: @filename', [
            '@entry_id' => $entry_id,
            '@counter' => $position['counter'],
            '@total' => $position['total'],
            '@filename' => $filename,
          ]));

          // When dealing with bibliographies we need to get bundles from xml.
          if ($is_bibliography) {
            $bundle = BibciteReferenceRepository::extractTypeFromXml($filename);
            if (!$bundle) {
              $this->io()->warning(dt('We couldn\'t determine the bibliography type for @filename. Skipping!', [
                '@filename' => $filename,
              ]));
              continue;
            }
            if (!isset($bibliography_repositories[$bundle])) {
              $bibliography_repositories[$bundle] = $this->getWrappedEntityRepository($entity_type_id, $bundle);
            }
            $repository = $bibliography_repositories[$bundle];
          }

          if (!is_file($filename)) {
            $this->io()->warning(dt('@filename does\'t exists!', [
              '@filename' => $filename,
            ]));
            continue;
          }

          $connection_id = str_replace('orlando:', '', $connection_id);
          $wrapped_entity = $repository->findByCWRCId($connection_id);
          if (!$wrapped_entity) {
            // Creating the taxonomy term.
            $wrapped_entity = $repository->createWrappedEntityFromXmlFile($connection_id, $filename);
            if ($wrapped_entity) {
              $this->io()->success(dt('The @connection with label: "@label" successfully created.', [
                '@connection' => $bundle,
                '@label' => $wrapped_entity->getEntity()->label(),
              ]));
            }
          }
          else {
            $this->io()->caution(dt('The @connection with "@label" was already saved!', [
              '@connection' => $bundle,
              '@label' => $wrapped_entity->getEntity()->label(),
            ]));
          }

          $event = new ConnectionIngestionEvent($connection_id, $connection, $filename);
          $this->eventDispatcher->dispatch($event, IngestionEvents::CONNECTION_INGESTION);
        }
      }
      else {
        $this->io()->warning(dt('@id entry didn\'t contain any @connection!', [
          '@id' => $entry_id,
          '@connection' => $connection,
        ]));
      }
    }
  }

  private function ingestItem(bool $is_author_profile, \stdClass $object, string $id) {
    if ($is_author_profile) {
      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\AuthorProfileRepository $repository */
      $repository = $this->getWrappedEntityRepository('node', 'author_profile');
      $id = $object->entry->entry_identifiers->id;
      $wrapped_entity = $repository->findByCWRCId($id);
      $type = 'author profile';
    }
    else {
      /** @var \Drupal\orlando_interface_ingestion\TypedRepositories\ProfileEventRepository $repository */
      $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
      $wrapped_entity = $repository->findById($id);
      $object->id = $id;
      $type = 'standalone event';
    }

    if (!$wrapped_entity) {
      $wrapped_entity = $is_author_profile
      ? $repository->createWrappedEntityFromObject($object)
      : $repository->createEmbeddedEventFromObject($object);

      if ($wrapped_entity) {
        $this->io()->success(dt('The @type "@item (@id)" was successfully saved!', [
          '@item' => $wrapped_entity->label(),
          '@id' => $wrapped_entity->getEntity()->id(),
          '@type' => $type,
        ]));
      }
    }
    else {
      $this->io()->caution(dt('The @type "@entry (@id)" was already saved!', [
        '@entry' => $wrapped_entity->label(),
        '@id' => $wrapped_entity->getEntity()->id(),
        '@type' => $type,
      ]));
    }
  }

  private function getItemData(bool $is_author_profile, string $id): ?\stdClass {
    $events_dir = Event::ITEMS_DIR;
    $dir = $is_author_profile ? static::FILES_ENTRIES_DIR : $events_dir;
    $filename = $is_author_profile
      ? AuthorProfile::getAuthorProfileXmlUri($id) . '/' . $id . '.json'
      : $events_dir . '/' . $id . '/' . $id .'.json';
    if (!is_file($filename)) {
      $this->io()->warning(dt('@file file for @id wasn\'t found in @dir!', [
        '@id' => $id,
        '@file' => $filename,
        '@dir' => $dir,
      ]));
      return NULL;
    }

    $json = file_get_contents($this->fileSystem->realpath($filename));
    $data = json_decode($json);
    $data->xmlFile = str_replace('.json', '.xml', $filename);
    return $data;
  }

  /**
   * Gets the typed entity repository.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\typed_entity\TypedRepositories\TypedEntityRepositoryInterface
   *   The typed entity repository.
   *
   * @throws \Drupal\typed_entity\RepositoryNotFoundException
   */
  private function getWrappedEntityRepository(string $entity_type_id, string $bundle) {
    if($bundle === 'bibl') {
      return;
    }
    assert($this->repositoryManager instanceof RepositoryManager);
    return $this->repositoryManager->repository($entity_type_id, $bundle);
  }

  private function getAllItems(string $entries_dir_realpath) {
    $pattern = $entries_dir_realpath . '*';
    $entries_dir = glob($pattern, GLOB_ONLYDIR);
    return array_map(function($dir) use ($entries_dir_realpath) {
      return str_replace($entries_dir_realpath, '', $dir);
    }, $entries_dir);
  }

}
