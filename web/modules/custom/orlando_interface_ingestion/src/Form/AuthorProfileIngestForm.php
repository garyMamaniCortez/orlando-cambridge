<?php

namespace Drupal\orlando_interface_ingestion\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_ingestion\XmlParserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AuthorProfileIngestForm extends FormBase {

  const FILES_ENTRIES_DIR = 'private://orlando-2-0-c-modelling/entities/entries';
  const FILES_PREFIX = 'orlando_';

  /**
   * The file system helper service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The xml parser.
   *
   * @var \Drupal\orlando_interface_ingestion\XmlParserInterface
   */
  protected $xmlParser;

  /**
   * AuthorProfileIngestForm constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system helper service.
   * @param \Drupal\orlando_interface_ingestion\XmlParserInterface $xml_parser
   *   The xml parser.
   */
  public function __construct(FileSystemInterface $file_system, XmlParserInterface $xml_parser) {
    $this->fileSystem = $file_system;
    $this->xmlParser = $xml_parser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('orlando_interface_ingestion.xml_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_ingestion_author_profile_ingest_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $path = drupal_get_path('module', 'orlando_interface_ingestion');
    $form['bingo'] = [
      '#markup' => $this->t('<p>Let start testing - @path</p>', [
        '@path' => DRUPAL_ROOT . '/' . $path,
      ]),
    ];

    $form['author_profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Author profile'),
      '#description' => $this->t('Enter the author profile you wish to ingest'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $author_profile = $form_state->getValue('author_profile');
    if ($author_profile) {
      $mask = '/' . $author_profile . '.json/';
      $files = $this->fileSystem->scanDirectory(static::FILES_ENTRIES_DIR . '/transformed/', $mask, ['recurse' => FALSE]);
      if (!$files) {
        $message = $this->t('No author profile file was found!');
        $form_state->setErrorByName('author_profile', $message);
      }
      else {
        $form_state->set('profile_files', $files);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $files = $form_state->get('profile_files') ?? [];
    foreach ($files as $file) {
      $profile_id = $file->name;
      $json = file_get_contents($this->fileSystem->realpath($file->uri));
      $data = json_decode($json);
      $boom = 'boom';
    }
  }

}
