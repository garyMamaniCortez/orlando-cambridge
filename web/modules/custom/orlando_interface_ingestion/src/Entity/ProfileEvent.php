<?php

namespace Drupal\orlando_interface_ingestion\Entity;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\content_entity_builder\Entity\Content;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ProfileEvent extends Content {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->isEmbedded()) {
      $profile = $this->get('profile');
      return $this->t('@profile: @date_label', [
        '@profile' => !$profile->isEmpty() ? $profile->entity->label() : 'Missing profile',
        '@date_label' => $this->getDateLabel(),
      ]);
    }
    return $this->get('title')->value;
  }

  public function getStatus() {
    return $this->get('status')->value;
  }

  public function isEmbedded() {
    return $this->getStatus() === 'embedded';
  }

  public function isStandAlone() {
    return !$this->isEmbedded();
  }

  public function getDate($which): ?string {
    $field = $which === 'start' ? 'date_value' : 'date_end_value';
    $date = $this->get($field);
    return !$date->isEmpty() ? $this->formatStoredDate($date->value, $this->getDateFormat($which)) : '';
  }

  public function getDateLabel() {
    $label = $this->get('date_label');
    if ($label->isEmpty()) {
      $date_label = $this->getDate('start');
      $end = $this->getDate('end');
      if ($end) {
        $date_label .= ' - ' . $end;
      }
      return $date_label;
    }
    return $label->value;
  }

  public function getDateFormat($which) {
    $field = $which === 'start' ? 'date_format' : 'date_end_format';
    return $this->get($field)->value;
  }

  public function getEventType() {
    return $this->get('type')->value;
  }

  private function formatStoredDate(string $date, string $format): string {
    [$date_parts['year'], $date_parts['month'], $date_parts['day']] = explode('-', $date);
    $date_plus = DateTimePlus::createFromArray($date_parts);
    return $date_plus->format($format);
  }

}
