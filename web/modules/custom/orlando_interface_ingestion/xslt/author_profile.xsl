<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
  <xsl:output encoding="UTF-8" method="text" indent="no"/>

  <!-- strip space -->
  <xsl:strip-space elements="*"/>

  <!-- global variables -->
  <xsl:variable name="id_prefix" select="ENTRY/@ID"/>

  <!-- replace all double quotation marks in text with escaped versions -->
  <xsl:template match="*/descendant::text()">
    <xsl:value-of select="replace(replace(replace(replace(., '&amp;', '&amp;amp;'), '&lt;', '&amp;lt;'), '&gt;', '&amp;gt;'), '&quot;', '\\&quot;')"/>
  </xsl:template>

  <!-- NAMED TEMPLATES -->
  <!-- Context template -->
  <xsl:template name="context">
    <xsl:choose>
      <xsl:when test="ancestor::AUTHORSUMMARY">
        <xsl:text>profile_summary</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::BIOGRAPHY">
        <xsl:choose>
          <xsl:when test="ancestor::BIRTH">
            <xsl:text>birth</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::CULTURALFORMATION">
            <xsl:text>cultural_formation</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::DEATH">
            <xsl:text>death</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::PERSONNAME">
            <xsl:text>names</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::EDUCATION">
            <xsl:choose>
              <xsl:when test="ancestor::INSTRUCTOR">
                <xsl:text>instructor</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>education</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="ancestor::FAMILY | ancestor::INTIMATERELATIONSHIPS">
            <xsl:text>family_and_intimate_relationships</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::FRIENDSASSOCIATES">
            <xsl:text>friends_and_associates</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::HEALTH">
            <xsl:text>health</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::LEISUREANDSOCIETY">
            <xsl:text>leisure_and_society</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::PERSONNAME">
            <xsl:text>profile_summary</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::OCCUPATION">
            <xsl:choose>
              <xsl:when test="ancestor::EMPLOYER">
                <xsl:text>employer</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>occupation</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="ancestor::OTHERLIFEEVENT">
            <xsl:text>other_life_event</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::POLITICS">
            <xsl:text>politics</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::LOCATION[@RELATIONTO = 'LIVED'] | ancestor::LOCATION[@RELATIONTO = 'MOVED'] | ancestor::LOCATION[@RELATIONTO = 'MIGRATED']">
            <xsl:text>residence</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::LOCATION[@RELATIONTO = 'VISITED'] | ancestor::LOCATION[@RELATIONTO = 'TRAVELLED']">
            <xsl:text>travel</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::VIOLENCE">
            <xsl:text>violence</xsl:text>
          </xsl:when>
          <xsl:when test="ancestor::WEALTH">
            <xsl:text>wealth_and_poverty</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="ancestor::WRITING">
        <xsl:choose>
          <xsl:when test="ancestor::TEXTUALFEATURES">
            <xsl:choose>
              <xsl:when test="ancestor::TINTERTEXTUALITY">
                <xsl:text>intertextuality_and_influence</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::TCHARACTERTYPEROLE">
                <xsl:text>characters</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::TSETTINGDATE | ancestor::TSETTINGPLACE">
                <xsl:text>literary_setting</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::TTHEMETOPIC">
                <xsl:text>theme_or_topic_treated_in_text</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>textual_features</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="ancestor::RECEPTION">
            <xsl:choose>
              <xsl:when test="ancestor::RSHEINFLUENCED">
                <xsl:text>intertextuality_and_influence</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::RFICTIONALIZATION">
                <xsl:text>fictionalization</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::RRESPONSES">
                <xsl:text>literary_responses</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>reception</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="ancestor::PRODUCTION">
            <xsl:choose>
              <xsl:when test="ancestor::PINFLUENCESHER">
                <xsl:text>intertextuality_and_influence</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PANTHOLOGIZATION">
                <xsl:text>anthologization</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PDEDICATION">
                <xsl:text>dedications</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PMATERIALCONDITIONS">
                <xsl:text>material_conditions_of_writing</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PPERFORMANCE">
                <xsl:text>performance_of_text</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PRELATIONSWITHPUBLISHER | ancestor::PSUBMISSIONSREJECTIONS | ancestor::PMODEOFPUBLICATION | ancestor::PSERIALIZATION | ancestor::PCIRCULATION | ancestor::PEARNINGS | ancestor::PCOPYRIGHT | ancestor::PCONTRACT | ancestor::PPRICE | ancestor::PEDITIONS | ancestor::PRARITIESFEATURESDECORATIONS | ancestor::PTYPEOFPRESS | ancestor::PPLACEOFPUBLICATION | ancestor::PADVERTISING | ancestor::PPERIODICALPUBLICATION | ancestor::PANTHOLOGIZATION">
                <xsl:text>publishing</xsl:text>
              </xsl:when>
              <xsl:when test="ancestor::PMODEOFPUBLICATION[@PUBLICATIONMODE = 'SUBSCRIPTION']">
                <xsl:text>subscriptions</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>textual_production</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Entities (persons, organizations, titles) mentioned in entry -->
  <xsl:template name="entities">
    <xsl:text>"connections":
        {"persons": [</xsl:text>
    <xsl:for-each select="//NAME">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
            <xsl:text>,
            </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>],
        "organizations": [</xsl:text>
    <xsl:for-each select="//ORGNAME">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
            <xsl:text>,
            </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>],
        "bibls": [</xsl:text>
    <xsl:for-each select="//(BIBCIT | TEXTSCOPE)">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
            <xsl:text>,
            </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]},</xsl:text>
  </xsl:template>

  <!-- Entry identifiers -->
  <xsl:template name="identifiers">
    <xsl:variable name="DoB">
      <xsl:choose>
        <!-- Date of Birth -->
        <xsl:when test="//BIRTH//CHRONSTRUCT">
          <xsl:value-of select="//BIRTH//CHRONSTRUCT/(DATE | DATERANGE | DATESTRUCT)/text()[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//BIRTH//SHORTPROSE/(DATE | DATERANGE | DATESTRUCT)/text()[1]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="DoD">
      <!-- date of Death -->
      <xsl:choose>
        <xsl:when test="//DEATH//CHRONSTRUCT">
          <xsl:value-of select="//DEATH//CHRONSTRUCT/(DATE | DATERANGE | DATESTRUCT)/text()[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//DEATH/SHORTPROSE/(DATE | DATERANGE | DATESTRUCT)/text()[1]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:text>"entry_identifiers":
         {"id": "</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"entity_uri": "</xsl:text>
    <xsl:value-of select="//STANDARD/@REF"/>
    <xsl:text>"</xsl:text>
    <xsl:if test="//BIRTH//(DATE | DATERANGE | DATESTRUCT)">
      <xsl:text>,</xsl:text>
      <xsl:text>"date-of-birth": "</xsl:text>
      <xsl:value-of select="$DoB"/>
      <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:if test="//DEATH//(DATE | DATERANGE | DATESTRUCT)">
      <xsl:text>,</xsl:text>
      <xsl:text>"date-of-death": "</xsl:text>
      <xsl:value-of select="$DoD"/>
      <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:text>},</xsl:text>
  </xsl:template>

  <!-- MASTER TEMPLATE -->
  <xsl:template match="/">
    <xsl:text>{"entry" :
                 {</xsl:text>
    <xsl:call-template name="identifiers"/>
    <xsl:call-template name="entities"/>
    <xsl:apply-templates select="descendant::ORLANDOHEADER | descendant::AUTHORSUMMARY | descendant::BIOGRAPHY | descendant::WRITING"/>
    <xsl:text>}}</xsl:text>
  </xsl:template>

  <!-- Orlando Header handling -->

  <xsl:template match="ORLANDOHEADER"><xsl:text>"metadata":  {"title": "</xsl:text><xsl:copy-of select="//DOCTITLE/text()"/>", "publication_statement": "<xsl:value-of select="//AUTHORITY"/>",
    "source_description": "<xsl:value-of select="//SOURCEDESC/normalize-space()"/>", "document-revisions": {"type": "<xsl:value-of select="//REVISIONDESC/@TYPE"/>", "content": <xsl:apply-templates
      select="//REVISIONDESC"/><xsl:text>}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="REVISIONDESC">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="RESPONSIBILITY">
      <xsl:text>{"responsibility_target": "</xsl:text><xsl:value-of select="@TARGET"/>", "contributor": "<xsl:value-of select="@RESP"/>", "work_status": "<xsl:value-of select="@WORKSTATUS"/>",
      "work_value": "<xsl:value-of select="@WORKVALUE"/>", "date": "<xsl:value-of select="DATE/@VALUE"/>" <!--"item": "<xsl:value-of select="ITEM/text()"/>"-->} <xsl:choose>
        <xsl:when test="position() != last()">
          <xsl:text>,</xsl:text>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>]}</xsl:text>
  </xsl:template>


  <!-- Author summary -->
  <xsl:template match="//AUTHORSUMMARY">
    <xsl:text>"authorsummary":
                    {"id": "</xsl:text>
    <xsl:value-of select="//AUTHORSUMMARY/@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"content": </xsl:text>
    <xsl:choose>
      <xsl:when test="descendant::node()">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]</xsl:text>
      </xsl:when>
      <xsl:otherwise>"null"</xsl:otherwise>
    </xsl:choose>
    <xsl:text>}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="//BIOGRAPHY">
    <xsl:text>"biography":
                    {"section_type": "biography",
        "section_heading": </xsl:text>
    <xsl:choose>
      <xsl:when test="child::HEADING">
        <xsl:apply-templates select="HEADING"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>"null"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>,</xsl:text>
    <xsl:text>
        "section_content": </xsl:text>
    <xsl:choose>
      <xsl:when test="descendant::node()">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates select="//BIOGRAPHY//DIV1"/>
        <xsl:text>]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>"null"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="//WRITING">
    <xsl:text>"writing":
                    {"section_type": "writing",
        "section_heading": </xsl:text>
    <xsl:choose>
      <xsl:when test="child::HEADING">
        <xsl:apply-templates select="HEADING"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>"null"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>,</xsl:text>
    <xsl:text>"section_content": </xsl:text>
    <xsl:choose>
      <xsl:when test="descendant::node()">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates select="//WRITING//DIV1"/>
        <xsl:text>]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>"null"</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>
        }</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="//DIV1">
    <xsl:text>{"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:if test="name(*[1]) = 'HEADING'">
      <xsl:text> "heading": </xsl:text>
      <xsl:apply-templates select="HEADING"/>
      <xsl:text>,</xsl:text>
    </xsl:if>
    <xsl:text>"content": [</xsl:text>
    <xsl:apply-templates
      select="./descendant::DIV2 | ./descendant::CHRONSTRUCT[parent::BIRTH | parent::CULTURALFORMATION | parent::DEATH | parent::EDUCATION | parent::FAMILY | parent::FRIENDSASSOCIATES | parent::HEALTH | parent::INTIMATERELATIONSHIPS | parent::LEISUREANDSOCIETY | parent::LOCATION | parent::OCCUPATION | parent::OTHERLIFEEVENT | parent::POLITICS | parent::RELIGION | parent::WEALTH] | ./descendant::TEXTSCOPE[parent::DIV1]"/>
    <xsl:text>]}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="//DIV2">
    <xsl:text>{"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:if test="name(*[1]) = 'HEADING'">
      <xsl:text> "heading": </xsl:text>
      <xsl:apply-templates select="HEADING"/>
      <xsl:text>,</xsl:text>
    </xsl:if>
    <xsl:text>"content": [</xsl:text>
    <xsl:apply-templates select="./descendant::P[parent::SHORTPROSE | parent::PRODUCTION] | ./descendant::CHRONSTRUCT | ./descendant::DATASTRUCT | ./descendant::TEXTSCOPE"/>
    <xsl:text>]}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="HEADING">
    <xsl:text>{"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"content": "</xsl:text>
    <xsl:value-of select="descendant::text()/translate(., '/&quot;', '//&quot;')"/>
    <xsl:text>"}</xsl:text>
  </xsl:template>


  <xsl:template match="DATASTRUCT">
    <xsl:text>{"list": [</xsl:text>
    <xsl:apply-templates select="DATAITEM"/>
    <xsl:text> ]}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="DATAITEM">
    <xsl:variable name="Name_type">
      <xsl:value-of select="descendant::node()[1]/name()"/>
    </xsl:variable>
    <xsl:variable name="Name_value">
      <xsl:value-of select="descendant::node()[1]/node()"/>
    </xsl:variable>
    <xsl:text> {"item": {"name_type": "</xsl:text>
    <xsl:value-of select="$Name_type"/>
    <xsl:text>", "name_value" :"</xsl:text>
    <xsl:value-of select="$Name_value/text()"/>
    <xsl:text>", "item_content": "</xsl:text>
    <xsl:apply-templates select="node() except P"/>
    <xsl:text>"}}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="GIVEN | SURNAME">
    <xsl:apply-templates select="node()"/>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text> </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="P[parent::SCHOLARNOTE]"/>
  <xsl:template match="P">
    <xsl:text>{"paragraph":
            {"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",
             "content": "</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"
                }}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="CHRONSTRUCT">
    <xsl:text>{"event": </xsl:text>
    <xsl:text>{"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"status": "embedded",
        "relevance": "</xsl:text>
    <xsl:value-of select="@RELEVANCE"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"type": "</xsl:text>
    <xsl:value-of select="@CHRONCOLUMN"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"time": {</xsl:text>
    <xsl:if test="DATE | DATESTRUCT">
      <xsl:text>"value": "</xsl:text>
      <xsl:value-of select="(DATE | DATESTRUCT)/@VALUE"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"label": "</xsl:text>
      <xsl:value-of select="(DATE | DATESTRUCT)/text()"/>
      <xsl:text>"</xsl:text>
      <xsl:text>}</xsl:text>
    </xsl:if>
    <xsl:if test="DATERANGE">
      <xsl:text>"value": "</xsl:text>
      <xsl:value-of select="DATERANGE/@FROM"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"end_value": "</xsl:text>
      <xsl:value-of select="DATERANGE/@TO"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"label": "</xsl:text>
      <xsl:value-of select="(DATERANGE)/text()"/>
      <xsl:text>"</xsl:text>
      <xsl:text>}</xsl:text>
    </xsl:if>
    <xsl:text>, </xsl:text>

    <!-- milestone handling -->
    <xsl:choose>
      <xsl:when
        test="ancestor::BIRTH | ancestor::DEATH | descendant::PFIRSTLITERARYACTIVITY | descendant::RBESTKNOWNWORK | descendant::RLANDMARKTEXT | descendant::PLASTLITERARYACTIVITY | descendant::RWRITINGMILESTONE">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::DIV2[descendant::PFIRSTLITERARYACTIVITY] and not(.//PFIRSTLITERARYACTIVITY) and position() = 1">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::DIV2[descendant::PLASTLITERARYACTIVITY] and not(.//PFIRSTLITERARYACTIVITY)">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::DIV2[descendant::RBESTKNOWNWORK] and not(.//RBESTKNOWNWORK)">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::DIV2[descendant::RLANDMARKTEXT] and not(.//RLANDMARKTEXT)">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
      <xsl:when test="ancestor::DIV2[descendant::RWRITINGMILESTONE] and not(.//RWRITINGMILESTONE)">
        <xsl:text>"milestone": "yes",</xsl:text>
      </xsl:when>
    </xsl:choose>

    <xsl:text>"content": </xsl:text>
    <xsl:apply-templates select="CHRONPROSE"/>
    <xsl:if test="SCHOLARNOTE">
      <xsl:text>,"scholarnote": "</xsl:text>
      <xsl:apply-templates select="SCHOLARNOTE"/>
      <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:if test="BIBCITS">
      <xsl:text>,"bibcits": "</xsl:text>
      <xsl:apply-templates select="BIBCITS"/>
      <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:text>}}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="CHRONPROSE">
    <xsl:text>{"chronprose_id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"chronprose_content": "</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"}</xsl:text>

  </xsl:template>

  <xsl:template match="TEXTSCOPE">
    <xsl:text>{"textscope": "&lt;orlando_textscope display='none' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='textscope'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_textscope&gt;"}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
        <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="SCHOLARNOTE">
    <xsl:text>&lt;orlando_scholarnote display='card'&gt;</xsl:text>
    <xsl:for-each select="P">
      <xsl:text>&lt;div data-scholar-note-paragraph='true'&gt;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>&lt;/div&gt;</xsl:text>
    </xsl:for-each>
    <xsl:text>&lt;/orlando_scholarnote&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="BIBCITS">

    <xsl:text>&lt;orlando_references display='card'&gt;</xsl:text>
    <xsl:for-each select="BIBCIT">
      <xsl:variable name="bibl_context">
        <xsl:call-template name="context"/>
      </xsl:variable>
      <xsl:text>&lt;orlando_citation display='list' parent_profile='</xsl:text>
      <xsl:value-of select="$id_prefix"/>
      <xsl:text>' context='</xsl:text>
      <xsl:value-of select="$bibl_context"/>
      <xsl:text>'</xsl:text>
      <xsl:if test="@REF">
        <xsl:text> id='</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>'</xsl:text>
      </xsl:if>
      <xsl:text>&gt;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>&lt;/orlando_citation&gt;</xsl:text>
    </xsl:for-each>
    <xsl:text>&lt;/orlando_references&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="NAME">
    <xsl:variable name="name_context">
      <xsl:call-template name="context"/>
    </xsl:variable>
    <xsl:text>&lt;orlando_person display='inline' style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='</xsl:text>
    <xsl:value-of select="$name_context"/>
    <xsl:text>'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_person&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="ORGNAME">
    <xsl:variable name="org_context">
      <xsl:call-template name="context"/>
    </xsl:variable>
    <xsl:text>&lt;orlando_organization display='inline' style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='</xsl:text>
    <xsl:value-of select="$org_context"/>
    <xsl:text>'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_organization&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="PLACE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_place' data-oi-element-style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="TITLE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_title'</xsl:text>
    <xsl:choose>
      <xsl:when test="@TITLETYPE = 'MONOGRAPHIC' or @TITLETYPE = 'JOURNAL'">
        <xsl:text> data-oi-element-style='italic'</xsl:text>
      </xsl:when>
      <xsl:when test="@TITLETYPE = 'SERIES'">
        <xsl:text> data-oi-element-style='bold'</xsl:text>
      </xsl:when>
      <xsl:when test="@TITLETYPE = 'ANALYTIC'">
        <xsl:text> data-oi-element-style='quote'</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="TOPIC">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_topic' data-oi-element-style='bold'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="QUOTE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_quote' data-oi-element-style='</xsl:text>
    <xsl:choose>
      <xsl:when test="@DIRECT = 'Y'">
        <xsl:text>quote</xsl:text>
      </xsl:when>
      <xsl:when test="@DIRECT = 'N'">
        <xsl:text>single_quote</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="SOCALLED">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_socalled' data-oi-element-style='single_quote'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <!--remove elements and content-->
  <xsl:template match="STANDARD"/>
  <xsl:template match="WORKSCITED"/>
  <xsl:template match="KEYWORDCLASS"/>
  <xsl:template match="RESEARCHNOTE"/>
  <xsl:template match="BIOGPROSE"/>

</xsl:stylesheet>
