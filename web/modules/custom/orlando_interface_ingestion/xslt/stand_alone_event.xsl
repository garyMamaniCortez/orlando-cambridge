<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
  <xsl:output encoding="UTF-8" method="text" indent="no"/>

  <xsl:strip-space elements="*"/>

  <!-- global variables -->
  <xsl:variable name="id_prefix" select="replace(replace(tokenize(base-uri(), '/')[last()], '\.xml', ''), 'orlando_', '')"/>

  <!-- replace all double quotation marks in text with escaped versions -->
  <xsl:template match="*/descendant::text()">
    <xsl:value-of select="replace(replace(replace(replace(., '&amp;', '&amp;amp;'), '&lt;', '&amp;lt;'), '&gt;', '&amp;gt;'), '&quot;', '\\&quot;')"/>
  </xsl:template>

  <!-- master template -->
  <xsl:template match="/">
        <xsl:text>{"standalone_event" :
                 {</xsl:text>
    <xsl:text>"id": "</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>",</xsl:text>
    <xsl:call-template name="connections"></xsl:call-template>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates select="descendant::ORLANDOHEADER | descendant::CHRONEVENT"/>
    <xsl:text>}}</xsl:text>
  </xsl:template>

  <!-- extract all named entities listed in the stand-alone event -->
  <xsl:template name="connections">
        <xsl:text>"connections":
        {"persons": [</xsl:text>
    <xsl:for-each select="//NAME">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
                        <xsl:text>,
                </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>],
        "organizations": [</xsl:text>
    <xsl:for-each select="//ORGNAME">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
                        <xsl:text>,
                </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>],
        "bibls": [</xsl:text>
    <xsl:for-each select="//BIBCIT">
      <xsl:sort select="@REF" order="ascending" data-type="text"/>
      <xsl:if test="@REF">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="@REF"/>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
                        <xsl:text>,
                </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]}</xsl:text>
  </xsl:template>

  <xsl:template match="ORLANDOHEADER"><xsl:text>"metadata":  {"title": "</xsl:text><xsl:copy-of
    select="replace(//DOCTITLE/text(), '&quot;', '\\&quot;')"/>", "publication_statement": "<xsl:value-of
    select="//AUTHORITY"/>", "source_description": "<xsl:value-of
    select="//SOURCEDESC/normalize-space()"/><xsl:text>",</xsl:text>
    <xsl:text>"document_revisions":
        [ </xsl:text><xsl:apply-templates
      select="//REVISIONDESC"/><xsl:text>]}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
                <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="REVISIONDESC">
    <xsl:for-each select="RESPONSIBILITY">
      <xsl:text>{"contributor": "</xsl:text><xsl:value-of select="@RESP"/>", "work_status":
      "<xsl:value-of select="@WORKSTATUS"/>", "work_value": "<xsl:value-of
      select="@WORKVALUE"/>", "date": "<xsl:value-of select="DATE/@VALUE"/>"
      <!--"item": "<xsl:value-of select="ITEM/text()"/>"-->} <xsl:choose>
      <xsl:when test="position() != last()">
                    <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="//CHRONEVENT">
        <xsl:text>"chronevent": {
                    </xsl:text>
    <xsl:apply-templates select="child::CHRONSTRUCT|child::SHORTPROSE | child::SCHOLARNOTES | child::BIBCITS"/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <!--<xsl:template match="P[parent::SCHOLARNOTE]"> </xsl:template>-->
  <xsl:template match="SHORTPROSE">
    <xsl:text>"prose": [</xsl:text>
    <xsl:apply-templates select="P[parent::SHORTPROSE]"/>
    <xsl:text>]</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
            <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="P[parent::SHORTPROSE]">
        <xsl:text>{"paragraph":
            {"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",
             "content": "</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"
                }}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
                <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="CHRONSTRUCT">
    <xsl:text>"event": </xsl:text>
    <xsl:text>{"id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"status": "standalone",
        "relevance": "</xsl:text>
    <xsl:value-of select="@RELEVANCE"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"type": "</xsl:text>
    <xsl:value-of select="@CHRONCOLUMN"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"time": {</xsl:text>
    <xsl:if test="DATE | DATESTRUCT">
      <xsl:text>"value": "</xsl:text>
      <xsl:value-of select="(DATE | DATESTRUCT)/@VALUE"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"label": "</xsl:text>
      <xsl:value-of select="(DATE | DATESTRUCT)/text()"/>
      <xsl:text>"</xsl:text>
      <xsl:text>}</xsl:text>
    </xsl:if>
    <xsl:if test="DATERANGE">
      <xsl:text>"value": "</xsl:text>
      <xsl:value-of select="DATERANGE/@FROM"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"end_value": "</xsl:text>
      <xsl:value-of select="DATERANGE/@TO"/>
      <xsl:text>",</xsl:text>
      <xsl:text>"label": "</xsl:text>
      <xsl:value-of select="(DATERANGE)/text()"/>
      <xsl:text>"</xsl:text>
      <xsl:text>}</xsl:text>
    </xsl:if>
    <xsl:text>, </xsl:text>
    <xsl:apply-templates select="CHRONPROSE"/>
    <xsl:if test="SCHOLARNOTES">
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="SCHOLARNOTES"/>
    </xsl:if>
    <xsl:text>}</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
                <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="CHRONPROSE">
    <xsl:text>"content": </xsl:text>
    <xsl:text>{"chronprose_id": "</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>",</xsl:text>
    <xsl:text>"chronprose_content": "</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"}</xsl:text>
  </xsl:template>

  <xsl:template match="SCHOLARNOTES">
    <xsl:text>"scholarnotes": "</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
                <xsl:text>,
                </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="SCHOLARNOTE">
    <xsl:text>&lt;orlando_scholarnote display='card'&gt;</xsl:text>
    <xsl:for-each select="P">
      <xsl:text>&lt;div data-scholar-note-paragraph='true'&gt;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>&lt;/div&gt;</xsl:text>
    </xsl:for-each>
    <xsl:text>&lt;/orlando_scholarnote&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="BIBCITS">
    <xsl:choose>
      <xsl:when test="parent::CHRONEVENT">
        <xsl:text>"citations": "</xsl:text>
        <xsl:text>&lt;div data-oi-element-style='list'&gt;</xsl:text>
        <xsl:text>&lt;h2&gt;works cited&lt;/h2&gt;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&lt;/div&gt;</xsl:text>
        <xsl:text>"</xsl:text>
        <xsl:choose>
          <xsl:when test="position() != last()">
                        <xsl:text>,
                </xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="parent::CHRONPROSE|parent::L|parent::P|parent::QUOTE">
        <xsl:text>&lt;orlando_references display='card'&gt;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&lt;/orlando_references&gt;</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="BIBCIT">
    <xsl:text>&lt;orlando_citation display='list' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='standalone_event'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_citation&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="NAME">
    <xsl:text>&lt;orlando_person display='inline' style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='standalone_event'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_person&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="ORGNAME">
    <xsl:text>&lt;orlando_organization display='inline' style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='standalone_event'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/orlando_organization&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="PLACE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_place' data-oi-element-style='underline' parent_profile='</xsl:text>
    <xsl:value-of select="$id_prefix"/>
    <xsl:text>' context='standalone_event'</xsl:text>
    <xsl:if test="@REF">
      <xsl:text> id='</xsl:text>
      <xsl:value-of select="@REF"/>
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="TITLE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_title'</xsl:text>
    <xsl:choose>
      <xsl:when test="@TITLETYPE = 'MONOGRAPHIC' or @TITLETYPE = 'JOURNAL'">
        <xsl:text> data-oi-element-style='italic'</xsl:text>
      </xsl:when>
      <xsl:when test="@TITLETYPE = 'SERIES'">
        <xsl:text> data-oi-element-style='bold'</xsl:text>
      </xsl:when>
      <xsl:when test="@TITLETYPE = 'ANALYTIC'">
        <xsl:text> data-oi-element-style='quote'</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="TOPIC">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_topic' data-oi-element-style='bold'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="QUOTE">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_quote' data-oi-element-style='</xsl:text>
    <xsl:choose>
      <xsl:when test="@DIRECT = 'Y'">
        <xsl:text>quote</xsl:text>
      </xsl:when>
      <xsl:when test="@DIRECT = 'N'">
        <xsl:text>single_quote</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="SOCALLED">
    <xsl:text>&lt;span data-oi-element-node-name='orlando_socalled' data-oi-element-style='single_quote'&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&lt;/span&gt;</xsl:text>
  </xsl:template>

  <!--remove elements and content-->
  <xsl:template match="SOURCES"/>
  <xsl:template match="KEYWORDCLASS"/>
  <xsl:template match="RESEARCHNOTE"/>
  <xsl:template match="BIOGPROSE"/>

</xsl:stylesheet>
