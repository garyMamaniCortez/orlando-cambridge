<?php

use GuzzleHttp\Client;
use Robo\Tasks;
use Saxon\SaxonProcessor;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends Tasks {

  const COMMIT_SHA = '523c18f922ea24a3840ba8b47fe78975883b3851';
  const FIXTURES_DESTINATION = '/mnt/files/private/orlando-2-0-c-modelling';
  const ARCHIVE = '/tmp/archive.tar.gz';

  protected $fixturesCommitSha = '';

  protected $transformOnly = FALSE;

  protected $reprocessedElementConnections = [];

  /**
   * Provides a commands to extract and transform downloaded content from Gitlab.
   *
   * @param string $entity_type The entity type to process. Currently supporting author_profile or event.
   * @param array $opts
   * @option $token GitLab token to use for authentication.
   * @option $reprocess Whether to re-process existing entity items.
   * @option $re-download Force a re-download of the archive from GitLab.
   * @option $use-sample Whether to use the sample list of entity items.
   * @option $limit Limit the number of entity items to be processed.
   * @option $offset Position from which to start the process of entity items.
   * @option $items A custom list of entity items to process.
   * @option $job-count Number of items to process in parallel.
   * @option $commit-sha The commit sha of the archive to download from Gitlab.
   * @option $transform-only Whether to skip the download of the archive, the
   *   documents extraction and only do the transformation based on the
   *   previously extracted documents.
   *
   * @usage ./vendor/bin/robo fixtures author_profile
   *   To download, extract and transform all the author profile entries, and
   *   their related connections (persons, organizations and bibliographies).
   * @usage ./vendor/bin/robo fixtures event
   *   To download, extract and transform all the standalone event entries, and
   *   their related connections (persons, organizations and bibliographies).
   * @usage ./vendor/bin/robo fixtures author_profile --use-sample
   *   To download, extract and transform the sample of author profile entries
   *   and their related connections (persons, organizations and
   *   bibliographies).
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function fixtures(string $entity_type, array $opts = ['token|t' => '', 'reprocess|r' => FALSE, 're-download|d' => FALSE, 'use-sample|s' => FALSE, 'limit|l' => 0, 'offset|o' => 0, 'items|i' => [], 'job-count|j' => 1, 'commit-sha|c' => '', 'transform-only' => FALSE]) {
    $this->init($opts);
    if ($entity_type !== 'author_profile' && $entity_type !== 'event') {
      $this->io->error('Unrecognized entity type!');
      return;
    }

    $this->fixturesCommitSha = !empty($opts['commit-sha']) ? $opts['commit-sha'] : static::COMMIT_SHA;
    $job_count = is_integer($opts['job-count']) && $opts['job-count'] >= 1 ? $opts['job-count'] : 1;
    $start_date_time = date('Y-m-d H:i:s');
    $this->transformOnly = $opts['transform-only'];

    $this->downloadProjectArchive($opts['token'], $opts['re-download']);
    $this->processItems($entity_type, $opts['reprocess'], $opts['use-sample'], (int) $opts['limit'], (int) $opts['offset'], $opts['items'], $job_count);
    $end_date_time = date('Y-m-d H:i:s');

    $this->io->success(sprintf('Started at %s and Finished at %s', $start_date_time, $end_date_time));
  }

  /**
   * Provides a command to reprocess all the bibliography mod files currently
   * extracted to ensure that their connections are also extracted.
   *
   * @param array $opts
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function reprocessBibliographies(array $opts = ['token|t' => '', 'reprocess|r' => FALSE, 're-download|d' => FALSE, 'commit-sha|c' => '']) {
    $this->init($opts);
    $this->downloadProjectArchive($opts['token'], $opts['re-download']);
    $pattern = static::FIXTURES_DESTINATION . '/entities/bibls/*.xml';
    $bibliography_files = glob($pattern);
    foreach ($bibliography_files as $filename) {
      $this->extractBibliographyConnections($filename, $opts['reprocess']);
    }
  }

  protected function init($opts) {
    $this->io = new SymfonyStyle($this->input(), $this->output());
    $this->fixturesCommitSha = !empty($opts['commit-sha']) ? $opts['commit-sha'] : static::COMMIT_SHA;
  }

  /**
   * Downloads the project archive.
   *
   * @param string $token
   *   The token.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function downloadProjectArchive(string $token, bool $re_download) {
    if (is_file(self::ARCHIVE) && !$re_download) {
      $this->io->warning('There is an archive file already downloaded. Please delete it first and make sure that ' . static::FIXTURES_DESTINATION . ' is empty!');
      return;
    }
    elseif ($re_download) {
      $this->taskExec('[[ -f "' . self::ARCHIVE . '" ]] && rm ' . self::ARCHIVE)
        ->run();
    }

    $id = 18989469;
    $resource = fopen(self::ARCHIVE, 'w');

    if (empty($token)) {
      $token = $this->askHidden('Please, provide the Gitlab HTTP token:');
    }

    $client = new Client(['headers' => ['PRIVATE-TOKEN' => $token]]);
    $sha = $this->fixturesCommitSha;
    $uri = "https://gitlab.com/api/v4/projects/$id/repository/archive.tar.gz?sha=" . $sha;
    try {
      $client->request('GET', $uri, ['sink' => $resource]);
    }
    catch (\Exception $e) {
      $this->io->error('Something went wrong! ' . $e->getMessage());
    }
  }

  protected function processItems(string $entity_type, bool $reprocess, bool $use_sample, int $limit, int $offset, array $items, int $job_count) {
    $saxon_proc = new SaxonProcessor(FALSE);
    $proc_version = $saxon_proc->version();
    $this->io->note("Using saxon version $proc_version");
    if (empty($items)) {
      $items = $this->getItemsList($entity_type, $use_sample);
      if ($offset > 0) {
        // Since element 1 is at 0 index we ensure that when offset is 1 we
        // pass 0. This also allow to start at the 400 elements instead of 401.
        $items = array_slice($items, $offset - 1);
      }
    }

    $counter = 0;
    $total = count($items);
    $total = ($limit > 0 && $limit < $total) ? $limit : $total;
    $processed_items = [];
    foreach ($items as $i => $item_id) {
      $item_id = str_replace('.xml', '', $item_id);
      // This allow us to skip tasks which have been already processed.
      if (isset($processed_items[$item_id])) {
        continue;
      }

      $tasks = $this->taskParallelExec();
      // Adding tasks to be processed in parallel.
      for ($j = $i; $j <= ($i + $job_count); $j++) {
        $counter++;
        // Assigning the item id to work with.
        if ($j == $i) {
          $current_item_id = $item_id;
        }
        elseif (isset($items[$j])) {
          // This is the next item.
          $current_item_id =  str_replace('.xml', '', $items[$j]);
        }
        else {
          break;
        }

        // Add this item into a processed var to keep track of items already
        // processed and skip the loop.
        $processed_items[$current_item_id] = $current_item_id;
        $tasks->addCode(function() use ($entity_type, $saxon_proc, $reprocess, $current_item_id, $counter, $total) {
          $this->printStatus($entity_type, $current_item_id, $counter, $total);
          $this->processItem($entity_type, $saxon_proc, $current_item_id, $reprocess);
        }, $i);
        // Ensure that we don't go over the limit.
        if ($limit > 0) {
          if ($counter === $limit) {
            break;
          }
        }
      }

      $tasks->run();
      if ($limit > 0) {
        if ($counter === $limit) {
          break;
        }
      }
    }
  }

  public function printStatus(string $entity_type, string $item_id, int $counter, int $total) {
    $this->io->title(sprintf('Processing "%s" %s %s/%s', $item_id, $entity_type, $counter, $total));
  }

  public function processItem(string $entity_type, SaxonProcessor $saxon_proc, string $item_id, bool $reprocess) {
    $prefix = 'orlando_';
    $items_folder = $entity_type === 'event' ? 'events' : 'entries';
    $items_dir = self::FIXTURES_DESTINATION . '/entities/' . $items_folder;
    $items_repo_dir = $this->getRepositoryFolder() . '/textbase-pubc/' . $items_folder . '-pubc';
    $item_id = str_replace($prefix, '', $item_id);
    $item = $prefix . $item_id;
    $item_file = "$items_repo_dir/$item.xml";
    $fixtures_item_file = "$items_dir/$item.xml";
    $destination = "$items_dir/$item_id";
    // Making sure that the re-process flag is on in case we want to clean the
    // destination folder and redo the xslt transformation.
    $reprocess = !$this->transformOnly ? $reprocess : TRUE;
    if (!is_dir($destination)) {
      $this->taskExec("mkdir -p $destination")
        ->run();
    }
    elseif ($reprocess) {
      $this->taskExec("rm -rf $destination")->run();
      // Only try to delete the original fixture file (orlando_<item_id>.xml) if
      // the transformation only flag isn't true.
      if (is_file($fixtures_item_file) && !$this->transformOnly) {
        $this->taskExec("rm $fixtures_item_file")->run();
      }
      $this->taskExec("mkdir $destination")->run();
    }

    if (!is_file($fixtures_item_file)) {
      $this->extractFileFromArchive($item_file, $destination, $entity_type, $item);
    }
    $this->transformItem($saxon_proc, $item_id, $fixtures_item_file, $items_dir, $reprocess, $entity_type);
    $this->extractItemConnections($entity_type, $item_id, $items_dir, $reprocess);

    // Extract and process author profile images. Skipping this step if only
    // transformation was requested.
    if ($entity_type === 'author_profile') {
      $item_img_dir = $this->getRepositoryFolder() . '/Orlando-images/' . $item_id;
      $this->extractFileFromArchive($item_img_dir, $destination, 'image', $item_id, '', FALSE);
      $this->processExtractedImageDirFiles($item_id, $destination);
    }
  }

  protected function transformItem(SaxonProcessor $processor, string $item, string $item_file, string $items_dir, bool $reprocess, string $entity_type) {
    if (!is_file($item_file)) {
      $this->io->warning(sprintf('%s could\'t be found!', $item_file));
      return;
    }

    $module = '/var/www/app/web/modules/custom/orlando_interface_ingestion';
    $xslt_processor = $processor->newXsltProcessor();
    $xslt_processor->setSourceFromFile($item_file);
    $xslt_processor->compileFromFile($module . '/xslt/add_ids_standalone_events.xsl');
    $result = $xslt_processor->transformToString();

    $xml = "$items_dir/$item/$item.xml";
    $json = "$items_dir/$item/$item.json";
    // When the re-processed flag have been set then we ca re-transform existing
    // files.
    if ($reprocess && (is_file($xml) || is_file($json))) {
      $this->taskExec("rm $json $xml")
        ->run();
    }
    if ($result !== NULL && !is_file($json)) {
      file_put_contents($xml, $result);

      $xslt_processor->clearParameters();
      $xslt_processor->clearProperties();

      $xslt_processor->setSourceFromFile($xml);
      $xslt_file = $entity_type == 'event' ? 'stand_alone_event' : 'author_profile';
      $xslt_processor->compileFromFile("$module/xslt/$xslt_file.xsl");
      $result = $xslt_processor->transformToString();

      if ($result !== NULL) {
        file_put_contents($json, $result);
      }
    }

    $xslt_processor->clearParameters();
    $xslt_processor->clearProperties();
  }

  protected function extractItemConnections(string $entity_type, string $item, string $items_dir, $reprocess = FALSE) {
    if ($this->transformOnly) {
      $this->io->warning('Skipping item connections extraction since we were asked to do transformation only.');
      return;
    }
    $json_file = "$items_dir/$item/$item.json";
    if (!is_file($json_file)) {
      $this->io->warning(sprintf('%s file could\'t be found!', $json_file));
      return;
    }
    $data_key = $entity_type === 'author_profile' ? 'entry' : 'standalone_event';

    $json = file_get_contents($json_file);
    $data = json_decode($json);
    $connections = $data->{$data_key}->connections ?? NULL;
    if ($connections) {
      $supported_connections = ['persons', 'organizations', 'bibls'];
      $search = [
        'https://commons.cwrc.ca/',
        'https://dev-02.cwrc.ca/islandora/object/',
      ];
      foreach ($supported_connections as $connection) {
        // Initialize the reprocessed property for this connection if not done
        // yet.
        if ($reprocess && !isset($this->reprocessedElementConnections[$connection])) {
          $this->reprocessedElementConnections[$connection] = [];
        }
        $connection_ids = $connections->{$connection} ?? NULL;
        $is_bibliography = $connection === 'bibls';
        if (is_array($connection_ids)) {
          $processed_ids = [];
          $connection_ids = array_unique($connection_ids);
          $connections_dir = self::FIXTURES_DESTINATION . '/entities/' . $connection;
          $connections_repo_dir = $this->getRepositoryFolder() . '/textbase-pubc/' . $connection . '-pubc';
          foreach ($connection_ids as $connection_id) {
            if (isset($processed_ids[$connection_id]) || ($reprocess && isset($this->reprocessedElementConnections[$connection][$connection_id]))) {
              continue;
            }
            $this->reprocessedElementConnections[$connection][$connection_id] = TRUE;
            $processed_ids[$connection_id] = $connection_id;
            $connection_id = str_replace($search, '', $connection_id);
            $connection_id = str_replace(':', '_', $connection_id);
            $connection_file = "$connections_repo_dir/$connection_id.xml";
            $destination_file = $connections_dir . '/' . $connection_id . '.xml';
            if (is_file($destination_file)) {
              if ($reprocess) {
                $this->taskExec("rm $destination_file")
                  ->run();
              }
              else {
                continue;
              }
            }

            $this->extractFileFromArchive($connection_file, $connections_dir, $connection, $connection_id);
            if ($is_bibliography) {
              $this->extractBibliographyConnections($destination_file, $reprocess);
            }
          }
        }
      }
    }
  }

  private function processExtractedImageDirFiles(string $entry_id, string $entry_dir) {
    $img_dir = $entry_dir . '/images';
    if (!is_dir($img_dir)) {
      return;
    }
    $image_bags = array_diff(scandir($img_dir), ['..', '.']);
    $image_bags = array_filter($image_bags, function($bag) {
      return strpos($bag, 'Bag-') === 0;
    });
    foreach ($image_bags as $image_bag) {
      $image_bag_dir = $img_dir . '/' . $image_bag;
      if (!is_dir($image_bag_dir)) {
        continue;
      }

      $this->taskExecStack()
        ->stopOnFail()
        ->exec("mv $image_bag_dir/data/* $img_dir")
        ->exec("rm -rf $image_bag_dir")
        ->run();
    }
    // Rename .bin files to xml.
    $pattern = $img_dir . '/*.bin';
    $bin_files = glob($pattern);
    foreach ($bin_files as $bin_file) {
      $xml = str_replace('bin', 'xml', $bin_file);
      $this->taskExec("mv $bin_file $xml")
        ->run();
    }
  }

  private function extractFileFromArchive(string $file, string $destination, string $entity_type, string $file_id, string $extension = 'xml', $stop_on_fail = TRUE) {
    if ($entity_type === 'image') {
      $object_dir = $destination;
      $fixture_file = $destination . '/images';
      if (is_dir($fixture_file)) {
        // When the destination folder for images exists and it's empty we
        // delete it to ensure the extraction process to complete.
        $should_extract = empty(array_diff(scandir($fixture_file), ['..', '.']));
        if ($should_extract) {
          $this->taskExec("rm -rf $fixture_file")
            ->run();
        }
      }
      else {
        // Since the images destination file is empty
        $should_extract = TRUE;
      }
      $create_dir = FALSE;
    }
    else {
      if ($entity_type === 'author_profile' || $entity_type === 'event') {
        $folder = $entity_type === 'event' ? 'events' : 'entries';
      }
      else {
        $folder = $entity_type;
      }

      $object_dir = self::FIXTURES_DESTINATION . '/entities/' . $folder;
      $fixture_file = $object_dir . '/' . $file_id . '.' . $extension;
      $should_extract = !is_file($fixture_file);
      $create_dir = TRUE;
    }

    if ($should_extract) {
      if (!is_dir($object_dir) && $create_dir) {
        $this->taskExec("mkdir -p $object_dir")
          ->run();
      }
      $this->taskExecStack()
        ->stopOnFail($stop_on_fail)
        ->exec('cd ' . $destination)
        ->exec('tar -zxf ' . self::ARCHIVE . ' ' . $file)
        ->exec('mv ' . $file . ' ' . $fixture_file)
        ->exec('rm -rf ' . $this->getRepositoryFolder())
        ->run();
    }
  }

  private function getItemsList(string $entity_type, bool $use_sample = FALSE) {
    $items = [];
    $ext = 'json';
    if ($entity_type === 'author_profile') {
      $file_id = 'entries-list';
      $destination = self::FIXTURES_DESTINATION . '/entities';
      $json_list_file = $this->getRepositoryFolder() . '/textbase-pubc/entries-register.json';
      $data_key = !$use_sample ? 'entries' : 'entries_samples';
    }
    else {
      $file_id = 'events-list';
      $destination = self::FIXTURES_DESTINATION . '/entities';
      $json_list_file = $this->getRepositoryFolder() . '/textbase-pubc/events-register.json';
      $data_key = !$use_sample ? 'stand_alone_events' : 'stand_alone_events_samples';
    }
    $filename = "$destination/$file_id.$ext";
    // Always force an extraction of the manifest file unless it's transformation
    // only.
    if (is_file($filename) && !$this->transformOnly) {
      $this->io->title(sprintf('Deleted the %s existing file', $filename));
      $this->taskExec("rm $filename")
        ->run();
    }
    $this->extractFileFromArchive($json_list_file, $destination, '', $file_id, $ext);
    $json = file_get_contents($filename);
    $data = json_decode($json);
    if (isset($data->{$data_key}) && is_array($data->{$data_key})) {
      $items = $data->{$data_key};
    }
    return $items;
  }

  private function extractBibliographyConnections(string $filename, $reprocess) {
    if (!is_file($filename)) {
      return;
    }
    if ($reprocess && !isset($this->reprocessedElementConnections['bibls'])) {
      $this->reprocessedElementConnections['bibls'] = [];
    }

    // A helper to allow us to skip the rest of the method if the file has been
    // deleted.
    $skip = FALSE;
    $xml = new \SimpleXMLElement($filename, NULL, TRUE);
    $xml->registerXPathNamespace('d', 'http://www.loc.gov/mods/v3');
    $author_xpath = '//d:mods/d:name[@valueURI]/@valueURI|//d:mods/d:relatedItem[@type="host"]/d:name[@valueURI]/@valueURI';
    $results = $xml->xpath($author_xpath);
    $search = [
      'https://commons.cwrc.ca/',
      'https://dev-02.cwrc.ca/islandora/object/',
    ];
    $connections_dir = self::FIXTURES_DESTINATION . '/entities/persons';
    $connections_repo_dir = $this->getRepositoryFolder() . '/textbase-pubc/persons-pubc';
    foreach ($results as $result) {
      $raw_id = (string) $result;
      if (strpos($raw_id, 'dev-02.cwrc.ca') !== FALSE) {
        $this->io->warning(sprintf('Deleting %s since it\'s probably old by containing a valueURI: %s', $filename, $raw_id));
        $this->taskExec("rm $filename")
          ->run();
        $skip = TRUE;
        break;
      }

      // Skipping connection process even when a reprocess have been requested
      // since we have identified that this connection have already been
      // processed during this command cycle.
      if ($reprocess && isset($this->reprocessedElementConnections['persons'][$raw_id]) || isset($this->reprocessedElementConnections['organizations'][$raw_id])) {
        $this->io->text(sprintf('Skipping: %s', $raw_id));
        continue;
      }

      $connection_id = str_replace($search, '', $raw_id);
      $connection_id = str_replace(':', '_', $connection_id);
      $connection_file = "$connections_repo_dir/$connection_id.xml";
      $destination_file = "$connections_dir/$connection_id.xml";

      if (is_file($destination_file)) {
        if ($reprocess) {
          // This ensure that this connection won't be reprocessed again for
          // another document. Putting it in both organizations and persons
          // cause it's hard to know which connection this one is.
          $this->reprocessedElementConnections['organizations'][$raw_id] = TRUE;
          $this->reprocessedElementConnections['persons'][$raw_id] = TRUE;
          $this->taskExec("rm $destination_file")
            ->run();
        }
        else {
          continue;
        }
      }
      $this->extractFileFromArchive($connection_file, $connections_dir, 'persons', $connection_id);
    }

    if ($skip) {
      return;
    }

    $publisher_xpath = '//d:mods/d:originInfo/d:publisher[@valueURI]/@valueURI|//d:mods/d:relatedItem[@type="host"]/d:originInfo/d:publisher[@valueURI]/@valueURI';
    $results = $xml->xpath($publisher_xpath);
    if (!empty($results[0])) {
      foreach (['persons', 'organizations'] as $connection) {
        $raw_id = (string) $results[0];
        if (strpos($raw_id, 'dev-02.cwrc.ca') !== FALSE) {
          $this->io->warning(sprintf('Deleting %s since it\'s probably old by containing a valueURI: %s', $filename, $raw_id));
          $this->taskExec("rm $filename")
            ->run();
          break;
        }

        $connections_dir = self::FIXTURES_DESTINATION . '/entities/' . $connection;
        $connections_repo_dir = $this->getRepositoryFolder() . '/textbase-pubc/' . $connection . '-pubc';
        $connection_id = str_replace($search, '', $raw_id);
        $connection_id = str_replace(':', '_', $connection_id);
        $connection_file = "$connections_repo_dir/$connection_id.xml";
        $destination_file = "$connections_dir/$connection_id.xml";

        if (is_file($destination_file)) {
          if ($reprocess) {
            $this->taskExec("rm $destination_file")
              ->run();
          }
          else {
            break;
          }
        }
        // Using try and catch here cause we want to fail silently since a
        // publisher can be a person or an organization and the archive might
        // fail.
        try {
          $this->extractFileFromArchive($connection_file, $connections_dir, $connection, $connection_id);
          // Let break if the extraction succeed.
          break;
        }
        catch (\Exception $e) {}
      }
    }
  }

  private function getRepositoryFolder() {
    return 'orlando-2.0-c-modelling-' . $this->fixturesCommitSha . '-' . $this->fixturesCommitSha;
  }

}
