#!/usr/bin/env bash

set -eu

declare -a directories=(
  "web/libraries/blazy/example"
  "web/libraries/ev-emitter/sandbox"
  "web/libraries/ev-emitter/test"
  "web/libraries/fizzy-ui-utils/test"
  "web/libraries/get-size/test"
  "web/libraries/jquery/src"
  "web/libraries/outlayer/docs"
  "web/libraries/outlayer/sandbox"
  "web/libraries/outlayer/test"
  "web/libraries/slick-lightbox/.github"
  "web/libraries/slick-lightbox/bin"
  "web/libraries/slick-lightbox/demo"
  "web/libraries/slick-lightbox/docs"
  "web/libraries/slick-lightbox/gh-pages"
  "web/libraries/slick-lightbox/test"
  "web/libraries/slick-lightbox/src"
  "web/libraries/toc/docs"
  "web/libraries/toc/example"
  "web/libraries/toc/grunt"
  "web/libraries/toc/test"
)
counter=0
echo "Deleting unneeded directories inside web/libraries/*"
for directory in "${directories[@]}"
do
  if [ -d $directory ]; then
    echo "Deleting $directory"
    rm -rf $directory
    counter=$((counter+1))
  fi
done
echo "$counter folders were deleted"

declare -a files=(
  "web/libraries/desandro-matches-selector/tests.*"
  "web/libraries/get-size/sandbox.html"
  "web/libraries/jquery/dist/*.map"
  "web/libraries/jquery/external/sizzle/dist/*.map"
  "web/libraries/slick-carousel/slick/*.scss"
  "web/libraries/slick-carousel/slick/*.less"
  "web/libraries/slick-carousel/*.html"
  "web/libraries/slick-lightbox/*.jade"
  "web/libraries/slick-lightbox/*.html"
  "web/libraries/slick-lightbox/*.coffee"
  "web/libraries/toc/*.json"
  "web/libraries/toc/*.js"
)
counter=0
echo "Deleting unneeded files inside web/libraries/*"
for file in "${files[@]}"
do
  if [[ -f $file ]]; then
    echo "Deleting $file"
    rm $file
    counter=$((counter+1))
  fi
done
echo "$counter files were deleted"
