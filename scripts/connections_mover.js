const argv = require('minimist')(process.argv.slice(2), {
  string: ['entity', 'profile'],
  alias: {e: 'entity', p: 'profile'},
  default: {entity: 'persons', profile: 'akhman'},
});

const entity = argv.e;
const profile = argv.p;

const fs = require('fs');
const path = require('path');
const directoryPath = path.join(__dirname, '../web/modules/custom/orlando_interface_ingestion/tests/fixtures/orlando-2-0-c-modelling/entities');
const source = `${directoryPath}/${entity}`;
const tmpDir = `${source}_tmp`;

function getConnections(entity) {
  const rawData = fs.readFileSync(`${directoryPath}/entries/transformed/${profile}.json`);
  const data = JSON.parse(rawData);
  if (data.entry.connections.hasOwnProperty(entity)) {
    return data.entry.connections[entity];
  }
  return [];
}

function processConnections(connections) {
  let processed_connections = [];
  console.log(`Processing ${connections.length} elements`);
  connections.forEach(connection => {
    if (processed_connections.includes(connection)) {
      console.log(`Skipping already procecessed: ${connection}`);
      return;
    }
    processed_connections.push(connection);
    const id = connection.replace('https://commons.cwrc.ca/', '')
      .replace(':', '_');

    moveConnection(id);
  });
}

function moveConnection(id) {
  const filename = `${source}/${id}.xml`;
  if (!fs.existsSync(filename)) {
    console.log(`Attempting to move a file which doesn't exist: ${filename}`);
    return;
  }
  fs.renameSync(filename, `${tmpDir}/${id}.xml`);
}

function rmdirSyncForce(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      const curPath = `${path}/${file}`;
      if(fs.lstatSync(curPath).isDirectory()) {
        rmdirSyncForce(curPath);
      }
      else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

if (!fs.existsSync(tmpDir)) {
  fs.mkdirSync(tmpDir);
}
else {
  rmdirSyncForce(tmpDir);
  fs.mkdirSync(tmpDir);
}

const connections = getConnections(entity);
processConnections(connections);
// Delete the current source.
rmdirSyncForce(source);
// Move the current tmpDir to become source.
fs.renameSync(tmpDir, source);
