# Orlando Interface

## About

provides entries on authors' lives and writing careers, contextual material,
timelines, sets of internal links, and bibliographies. Interacting with these
materials creates a dynamic inquiry from any number of perspectives into
centuries of women's writing.

## Content extraction and transformation

To extract and transform the xml files into the drupal website file
system token access is required. Once the token access is obtained, and the
preliminary site installation has taken place, navigate to the web app root
folder and run:

1. `./vendor/bin/robo fixtures author_profile`: To download, extract and
   transform all the author profile entries, and their related connections
   (persons, organizations and bibliographies).
2. `./vendor/bin/robo fixtures event`: To download, extract and transform all
   the standalone event entries, and their related connections (persons,
   organizations and bibliographies).

### Note

There are other options and flags which can be added to the `fixtures`
robo command in order to change affect its behaviour. Please, Run
`./vendor/bin/robo fixtures --help` to see the full list of options and flags
and their usage. Also, these two commands can take a **VERY LONG TIME** to
finish.

To speed up the extraction and transformation of the content, a deployment of
multiple instances (pods/containers) of the drupal container and combine the
offset and limit options of the vendor/bin/robo fixtures command. This way,
running the extraction and transformation of multiple content at a time can be
achieve. For example in one instance the following command
`./vendor/bin/robo fixtures author_profile --limit 400` can be run while
running `./vendor/bin/robo fixtures author_profile --offset 399 --limit 400`
in the 2nd instance and finally if there is a 3rd instance, running
`./vendor/bin/robo fixtures author_profile --offset 799` without the limit
option to run the rest of the author profiles which are approximately 1300.

### Assumptions

The current implementation of the robo `fixtures` command assumes that the
private file directory is at `/mnt/files/private`, if this is not the case based
on the setup where this website is running please open an issue under this
repository, and see if we can add a command option to specify where this
directory is situated.

## Content ingestion

Once the extraction and transformation of entries/author profiles is complete, the
ingestion into drupal website database can be done by running:

1. `./vendor/bin/drush oi_ingest:entries author_profile --all`: To ingest all
  the extracted and transformed author profiles.
2. `./vendor/bin/drush oi_ingest:entries author_profile --all`: To ingest all
   the extracted and transformed standalone events.

The `./vendor/bin/drush oi_ingest:entries` also have more options and flags
which can change how the ingestion process take place. Please run
`./vendor/bin/drush oi_ingest:entries --help` for more information and usage
examples.
